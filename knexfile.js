if (!process.env.NODE_ENV) {
  throw new Error('please specify a `NODE_ENV`.')
}

require('babel-core/register')
require('dotenv').config()

const { dbConfig } = require('./src/server/db')


module.exports = {
  [process.env.NODE_ENV]: Object.assign({}, dbConfig, {
    client: 'pg',
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: '__knex_migrations',
      directory: `${__dirname}/src/server/db/migrations`,
    },
  }),
}
