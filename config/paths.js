import path from 'path'

const PATH_BASE = path.resolve(__dirname, '../')
const DIR_SRC = 'src'
const DIR_CLIENT = 'client'
const DIR_DLL = 'dll'
const DIR_PUBLIC = 'public'
const DIR_DIST = 'dist'
const DIR_BUILD = 'build'
const DIR_SERVER = 'server'
const DIR_WEBPACK = 'webpack'
const DIR_NODE_MODULES = 'node_modules'
const DIR_PROJECT_CONFIG = 'config'
const DIR_UPLOADS = 'uploads'


export default function paths(dir = 'base') {
  const base = (...args) => path.resolve.apply(
    path.resolve,
    [PATH_BASE, ...args],
  )
  const basePaths = {
    base: base(),
    entryApp: base(DIR_SRC, DIR_CLIENT, 'index.js'),
    entryServer: base(DIR_SRC, DIR_SERVER, 'index.js'),
    src: base(DIR_SRC),
    dll: base(DIR_WEBPACK, DIR_DLL),
    dllManifest: base(DIR_WEBPACK, DIR_DLL, '[name]-manifest.json'),
    dllVendor: base(DIR_WEBPACK, DIR_DLL, 'dll.vendor.js'),
    dist: base(DIR_DIST, DIR_PUBLIC),
    uploads: base(DIR_PUBLIC, DIR_UPLOADS),
    serve: base(DIR_PUBLIC),
    favicon: base(DIR_PUBLIC, 'favicon.ico'),
    indexFile: base(DIR_SRC, DIR_CLIENT, 'template.ejs'),
    staticDir: base(DIR_PUBLIC),
    build: base(DIR_BUILD),
    server: base(DIR_SRC, DIR_SERVER, 'app.js'),
    nodeModules: base(DIR_NODE_MODULES),
    projectConfig: base(DIR_PROJECT_CONFIG),
  }

  return basePaths[dir]
}
