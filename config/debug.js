export default {
  // use a static user instead of jwt authentication
  ENABLE_DEV_USER: true,

  DEV_USER: {
    id: 1,
    entityId: 1,
    username: 'van',
    firstName: 'Van',
    lastName: 'Nguyen',
    email: 'vnguyen94@gmail.com',
    role: 'admin',
  },
}
