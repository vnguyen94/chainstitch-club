import defaults from './defaults'
import environment from './environment'
import featureFlagsClient from './featureFlagsClient'
import featureFlagsServer from './featureFlagsServer'
import limits from './limits'
import webpack from './webpack'
import paths from './paths'
import debug from './debug'
import security from './security'


const config = Object.assign(
  {},
  defaults,
  environment,
  featureFlagsClient,
  featureFlagsServer,
  limits,
  webpack,
  debug,
  security,
)


export { config as default, paths }
