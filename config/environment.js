import ip from 'ip'


export default {
  __DEV__: process.env.NODE_ENV === 'development',
  __PROD__: process.env.NODE_ENV === 'production',

  HOST_NAME: 'localhost',
  API_PATH: '/api',

  // Server Configuration
  SERVER_HOST: process.env.HOST || ip.address(),
  SERVER_PORT: process.env.PORT || 3001,

  ERROR_MONITORING_ENABLED: false,
}
