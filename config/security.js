export default {
  BCRYPT_ROUNDS: 10,

  JWT_CONFIG: {
    algorithm: 'HS256',
    expiresIn: '1y', // TODO: change to 7d after jwt refresh implemented
    issuer: 'chainstitch club',
    audience: 'user',
  },

  ENCRYPTION_ALGORITHM: 'aes-256-ctr',
}
