export default {
  VALID_COMMUNITY_CREATIONS: ['fades', 'fits'],
  VALID_COMMUNITY_TYPES: ['styles', 'brands', 'regions'],
  VALID_COMMUNITY_TYPES_SINGULAR: ['style', 'brand', 'region'],
  UPLOAD_IMAGE_SIZE: 6000000,
}
