export default {
  // toggle between saving images locally or
  // uploading to S3
  UPLOAD_IMAGES_TO_LOCAL: true,
}
