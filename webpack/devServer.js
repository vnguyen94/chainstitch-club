import webpack from 'webpack'
import WebpackDevServer from 'webpack-dev-server'
import _debug from 'debug'

import projectConfig from '../config'

import webpackConfig from './config/development'

const { HOST_NAME, SERVER_PORT, WEBPACK_DEV_SERVER_PORT } = projectConfig
const debug = _debug('app:webpack:dev:server')
const compiler = webpack(webpackConfig)
const serverOptions = {
  noInfo: false,
  quiet: false,
  hot: true,
  historyApiFallback: true,
  proxy: {
    '/api': `http://${HOST_NAME}:${SERVER_PORT}`,
  },
  stats: {
    errorDetails: true,
    assets: false,
    colors: true,
    version: false,
    hash: false,
    timings: true,
    chunks: true,
    chunkModules: false,
  },
}

const webpackServer = new WebpackDevServer(compiler, serverOptions)

webpackServer.listen(WEBPACK_DEV_SERVER_PORT, () => {
  debug(
    `Webpack dev server listening on port ${WEBPACK_DEV_SERVER_PORT}`,
  )
})
