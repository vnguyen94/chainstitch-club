import webpack from 'webpack'
import _debug from 'debug'

import projectConfig from '../../config'

import loaders from './loaders'

const debug = _debug('app:webpack:config:test')
const {
  __DEV__,
  __PROD__,
} = projectConfig

debug('Create configuration.')
const config = {
  devtool: 'source-map',
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [
      'node_modules',
    ],
  },
  module: {
    rules: [
      ...loaders.js,
      ...loaders.json,
      ...loaders.css,
      ...loaders.fonts,
      ...loaders.svg,
      ...loaders.images,
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEV__,
      __PROD__,
    }),
  ],
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
}

export default config
