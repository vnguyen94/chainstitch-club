import webpack from 'webpack'
import _debug from 'debug'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import PurifyCSSPlugin from 'purifycss-webpack-plugin'
import WebpackMd5Hash from 'webpack-md5-hash'
import CompressionPlugin from 'compression-webpack-plugin'
import UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import InlineChunkWebpackPlugin from 'html-webpack-inline-chunk-plugin'
// import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer'

import projectConfig, { paths } from '../../config'

import loaders from './loaders'

const debug = _debug('app:webpack:config:prod')
const {
  VENDOR_DEPENDENCIES,
  __DEV__,
  __PROD__,
} = projectConfig

debug('Create configuration.')

function isVendor({ resource }) {
  return /node_modules/.test(resource)
}

export default {
  performance: {
    hints: 'warning',
  },
  context: paths('base'),
  devtool: 'source-map',
  entry: {
    app: paths('entryApp'),
    vendor: VENDOR_DEPENDENCIES,
  },
  output: {
    path: paths('dist'),
    filename: 'js/[name].[chunkhash].js',
    chunkFilename: 'js/[name].[chunkhash].js',
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [
      'node_modules',
    ],
    alias: {
      joi: 'joi-browser',
    },
  },
  module: {
    rules: [
      ...loaders.cssProduction,
      ...loaders.fonts,
      ...loaders.images,
      ...loaders.js,
      ...loaders.svg,
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        BROWSER: JSON.stringify(true),
      },
      __DEV__,
      __PROD__,
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
    new ExtractTextPlugin({
      filename: 'css/[name].[contenthash].css',
      disable: false,
      allChunks: true,
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: paths('indexFile'),
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
      inject: true,
    }),

    // optimizations
    new PurifyCSSPlugin({
      basePath: __dirname,
      purifyOptions: {
        info: true,
        minify: true,
        whitelist: ['*title*', '*h2*'],
      },
      paths: [
        'src/**/*.jsx',
        'src/**/*.js',
      ],
    }),
    new UglifyJsPlugin({
      sourceMap: true,
      cache: true,
      parallel: true,
    }),
    new CompressionPlugin({
      algorithm: 'gzip',
      minRatio: 0,
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: isVendor,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
    }),
    new InlineChunkWebpackPlugin({
      inlineChunks: ['manifest'],
    }),
    new WebpackMd5Hash(),
    // new BundleAnalyzerPlugin(),
  ],
}
