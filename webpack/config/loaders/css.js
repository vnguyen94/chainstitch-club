import { paths } from '../../../config'

const baseOptions = {
  modules: true,
  sourceMap: true,
  localIdentName: '[name]__[local]___[hash:base64:5]',
}
const srcDir = paths('src')
const loaders = [
  {
    test: /\.css$/,
    include: [srcDir, /flexboxgrid/],
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          ...baseOptions,
          importLoaders: 1,
        },
      },
      'postcss-loader',
    ],
  },
  {
    test: /\.scss$/,
    include: [srcDir],
    loaders: [
      {
        loader: 'style-loader',
        options: {
          sourceMap: true,
        },
      },
      {
        loader: 'css-loader',
        options: {
          ...baseOptions,
          importLoaders: 2,
        },
      },
      {
        loader: 'postcss-loader',
        options: {
          sourceMap: true,
        },
      },
      {
        loader: 'sass-loader',
        options: {
          outputStyle: 'expanded',
          sourceMap: true,
        },
      },
    ],
  },
]


export default loaders
