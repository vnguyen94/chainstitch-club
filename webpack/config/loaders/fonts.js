const baseOptions = {
  prefix: 'fonts/',
  name: '[path][name].[ext]',
  limit: 10000,
}

const loader = [
  {
    test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
    loaders: [
      {
        loader: 'url-loader',
        options: {
          ...baseOptions,
          mimetype: 'application/font-woff',
        },
      },
    ],
  },
  {
    test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
    loaders: [
      {
        loader: 'url-loader',
        options: {
          ...baseOptions,
          mimetype: 'application/font-woff2',
        },
      },
    ],
  },
  {
    test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
    loaders: [
      {
        loader: 'url-loader',
        options: {
          ...baseOptions,
          mimetype: 'application/octet-stream',
        },
      },
    ],
  },
  {
    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    loaders: [
      {
        loader: 'file-loader',
        options: {
          prefix: 'fonts/',
          name: '[path][name].[ext]',
        },
      },
    ],
  },
]

export default loader
