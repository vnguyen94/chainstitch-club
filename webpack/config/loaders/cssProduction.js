import ExtractTextPlugin from 'extract-text-webpack-plugin'

import { paths } from '../../../config'


function cssLoaderOptions(sass = false) {
  const options = [
    'modules',
    'sourceMap',
    'localIdentName=[name]__[local]___[hash:base64:5]',
  ]

  options.push(
    !sass ? 'importLoaders=1' : 'importLoaders=2',
  )

  return options.join('&')
}
const scssLoaderOptions = [
  'outputStyle?',
  'expanded',
  'sourceMap',
].join('&')

const srcDir = paths('src')
const loaders = [
  {
    test: /\.css$/,
    include: [srcDir, /flexboxgrid/],
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: `css-loader?${cssLoaderOptions()}!postcss-loader?sourceMap`,
    }),
  },
  {
    test: /\.scss$/,
    include: [srcDir],
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: `css-loader?${cssLoaderOptions(true)}!postcss-loader?sourceMap!sass-loader?${scssLoaderOptions}`, // eslint-disable-line max-len
    }),
  },
]


export default loaders
