const baseOptions = {
  prefix: 'fonts/',
  name: '[path][name].[ext]',
  limit: 10000,
  mimetype: 'image/svg+xml',
}

const loader = [
  {
    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    loaders: [
      {
        loader: 'url-loader',
        options: {
          ...baseOptions,
        },
      },
    ],
  },
]

export default loader
