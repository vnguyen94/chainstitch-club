import { paths } from '../../../config'


const srcDir = paths('src')
const loaders = [
  {
    test: /\.js[x]?$/,
    loader: 'babel-loader',
    include: [srcDir],
    options: {
      cacheDirectory: true,
      babelrc: true,
    },
  },
]


export default loaders
