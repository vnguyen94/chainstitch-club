const loader = [
  {
    test: /\.(png|jpe?g|gif|ico)$/,
    loader: 'url-loader',
    options: {
      limit: 10240,
    },
  },
]

export default loader
