import css from './css'
import cssProduction from './cssProduction'
import fonts from './fonts'
import images from './images'
import js from './js'
import svg from './svg'


export default {
  css,
  cssProduction,
  fonts,
  images,
  js,
  svg,
}
