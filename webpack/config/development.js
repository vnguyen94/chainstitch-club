import webpack from 'webpack'
import _debug from 'debug'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import AddAssetHtmlPlugin from 'add-asset-html-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'

import projectConfig, { paths } from '../../config'
import dllManifest from '../dll/vendor-manifest.json'

import loaders from './loaders'

const debug = _debug('app:webpack:config:dev')
const {
  HOST_NAME,
  WEBPACK_DEV_SERVER_PORT,
  __DEV__,
  __PROD__,
} = projectConfig

debug('Create configuration.')

export default {
  cache: true,
  devtool: 'source-map',
  context: paths('base'),
  entry: {
    app: [
      'react-hot-loader/patch',
      // eslint-disable-next-line max-len
      `webpack-dev-server/client?http://${HOST_NAME}:${WEBPACK_DEV_SERVER_PORT}`,
      'webpack/hot/only-dev-server',
      paths('entryApp'),
    ],
  },
  output: {
    path: paths('build'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    modules: [
      'node_modules',
    ],
  },
  devServer: {
    hot: true,
  },
  plugins: [
    new webpack.DefinePlugin({
      __DEV__,
      __PROD__,
    }),
    new webpack.LoaderOptionsPlugin({
      debug: true,
      quiet: false,
    }),
    new webpack.DllReferencePlugin({
      context: paths('src'),
      manifest: dllManifest,
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: paths('indexFile'),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new AddAssetHtmlPlugin({
      filepath: paths('dllVendor'),
    }),
    new CopyWebpackPlugin([{
      from: paths('serve'),
    }]),
  ],
  module: {
    rules: [
      ...loaders.js,
      ...loaders.css,
      ...loaders.fonts,
      ...loaders.images,
      ...loaders.svg,
    ],
  },
}
