import webpack from 'webpack'

import config, { paths } from '../../config'

import loaders from './loaders'


export default {
  devtool: 'source-map',
  entry: {
    vendor: config.VENDOR_DEPENDENCIES,
  },
  output: {
    path: paths('dll'),
    filename: 'dll.[name].js',
    library: '[name]',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        BROWSER: JSON.stringify(true),
      },
      __DEV__: true,
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new webpack.DllPlugin({
      path: paths('dllManifest'),
      name: '[name]',
      context: paths('src'),
    }),
  ],
  module: {
    rules: [
      ...loaders.css,
    ],
  },
}
