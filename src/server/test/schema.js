import db from '../db'
import schemas from '../models/schema'


const dbTables = [
  ['entities', schemas.Entities],
  ['entities_likes', schemas.EntitiesLikes],
  ['entities_follows', schemas.EntitiesFollows],
  ['images', schemas.Images],
  ['users', schemas.Users],
  ['creations', schemas.Creations],
  ['creations_communities', schemas.CreationsCommunities],
  ['creations_dates', schemas.CreationsDates],
  ['creations_images', schemas.CreationsImages],
  ['comments', schemas.Comments],
  ['communities', schemas.Communities],
  ['communities_creations', schemas.CommunitiesCreations],
  ['fades', schemas.Fades],
  ['feeds', schemas.Feeds],
  ['fits', schemas.Fits],
  ['users_email_verification_tokens', schemas.UsersEmailVerificationTokens],
  ['users_feeds', schemas.UsersFeeds],
  ['users_images', schemas.UsersImages],
  ['users_profiles', schemas.UsersProfiles],
  ['users_notifications', schemas.UsersNotifications],
]

async function dropTables() {
  try {
    dbTables
      .map(([name]) => db.knex.raw(`DROP TABLE IF EXISTS ${name} CASCADE`))
      .forEach(async (promise) => {
        await promise
      })
  } catch (err) {
    throw err
  }
}

async function createTables() {
  try {
    dbTables
      .map(([name, schema]) => db.knex.schema.createTable(name, schema))
      .forEach(async (promise) => {
        await promise
      })
  } catch (err) {
    throw err
  }
}

async function createSchema() {
  try {
    await dropTables()
    await createTables()
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err)
  } finally {
    await db.knex.destroy()
  }
}


createSchema()
