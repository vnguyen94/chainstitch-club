import Joi from 'joi'
import Boom from 'boom'
import { normalize } from 'normalizr'

import { validatePayload } from '../../../utils'
import { ImagesSchema } from '../../../schemas'
import { uploadImage as libUploadImage } from '../../lib'
import { Images, Creations } from '../../models'


const validationSchema = Joi.object().keys({
  image: Joi.object().required(),
})

/**
 * @apiDescription creates an image.
 * @api {POST} /api/{creation}/images Upload Image
 * @apiName uploadImage
 * @apiGroup creations
 * @apiPermission user
 * @apiVersion 0.0.5
 *
 * @apiError Error {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "results": {
 *           "url": "/uploads/file_1505794511090.png",
 *           "order": 7,
 *           "entityId": 14,
 *           "updated_at": "2017-09-19T04:15:11.117Z",
 *           "created_at": "2017-09-19T04:15:11.117Z",
 *           "id": 9
 *       }
 *   }
 */
async function uploadImage(ctx) {
  try {
    const { body } = ctx.request
    const { user } = ctx.state
    const { creationId } = ctx.params
    const creation = await Creations.findById(
      creationId,
      {
        columns: ['entity_id', 'user_id'],
        require: true,
      },
    )

    if (creation.get('userId') !== user.id) {
      throw Boom.badRequest('you are not the owner.')
    }

    const entityId = creation.get('entityId')
    const payload = await validatePayload(body.files, validationSchema)
    const [originalImage, imageCount] = await Promise.all([
      libUploadImage(payload.image),
      Images.where({ entity_id: entityId }).count(),
    ])
    const image = await Images.create({
      url: originalImage.path,
      height: originalImage.height,
      width: originalImage.width,
      order: parseFloat(imageCount) + 1,
      entityId,
    })
    const normalized = normalize(
      image.toJSON(),
      ImagesSchema.single,
    )

    ctx.status = 200
    ctx.body = {
      results: normalized,
    }
  } catch (err) {
    if (err instanceof Creations.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default uploadImage
