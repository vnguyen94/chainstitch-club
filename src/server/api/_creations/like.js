import Joi from 'joi'
import Boom from 'boom'
import { camelCase } from 'lodash'

import { EntitiesLikes } from '../../models'
import { feed } from '../../lib'
import { validatePayload } from '../../../utils'
import { EntitiesColumns } from '../../../schemas'


const validationSchema = Joi.object().keys({
  creationId: Joi.number().integer().required(),
})

/**
 * @apiDescription likes or unlikes a creation.
 * @api {POST} /api/{creations}/:creationId/likes Like
 * @apiName like
 * @apiGroup creations
 * @apiPermission user
 * @apiVersion 0.1.1
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 */
const like = (Model, schema) => async (ctx) => {
  try {
    const { user } = ctx.state
    const { creationId } = await validatePayload(ctx.params, validationSchema)
    const withRelated = {
      'creations.entities': (qb) => {
        qb.columns(EntitiesColumns)
      },
    }
    const model = await Model.findOne({ creation_id: creationId }, {
      require: true,
      withRelated,
    })
    const entityId = model
      .related('creations')
      .related('entities')
      .get('id')

    if (ctx.method === 'POST') {
      await EntitiesLikes.like(user.entityId, entityId)

      const creation = model.related('creations')

      feed.addActivity({
        actor: feed.models.users(user.id),
        verb: 'like',
        object: feed.models[camelCase(schema.ENTITY_NAME)](creation),
      })
    } else { // DELETE
      await EntitiesLikes.unlike(user.entityId, entityId)
    }

    ctx.status = 204
  } catch (err) {
    if (err instanceof Model.NotFoundError) {
      throw Boom.badRequest('entity not found.')
    }
    if (err instanceof EntitiesLikes.NoRowsDeletedError) {
      throw Boom.badRequest('you have not liked this entity.')
    }
    if (err.constraint === 'entities_likes_entity_1_id_entity_2_id_unique') {
      throw Boom.badRequest('you have already liked this entity.')
    }

    throw err
  }
}


export default like
