import Boom from 'boom'
import { normalize } from 'normalizr'

import { Creations, Images } from '../../models'
import { ImagesSchema } from '../../../schemas'


/**
 * @apiDescription fetches creation's images.
 * @api {GET} /api/{creation}/:creationId/images Fetch Images
 * @apiName fetchImages
 * @apiGroup creations
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object}  data
 * @apiSuccess {Object}  data.entities normalized data
 * @apiSuccess {Integer} data.result   result ID
 *
 * @apiError NotFoundError {Object} 400 Creation not found
 * @apiError Error         {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "entities": {
 *         "fades": {
 *           "1": {
 *             "id": 1,
 *             "creationId": 2,
 *             "title": "test fade",
 *             "description": "yo this is a pbj xx-012 fade",
 *             "imageUrl": "https://s-media-cache-ak0.pinimg.com/originals/1c/b8/93/1cb8932aa4eb7930925ac9931ec1000a.jpg"
 *           }
 *         },
 *         "users": {
 *           "1": {
 *             "id": 1,
 *             "username": "van"
 *           }
 *         },
 *         "creations": {
 *           "2": {
 *             "id": 2,
 *             "userId": 1,
 *             "type": "fades",
 *             "dateCreated": "2017-03-23T13:45:30.635Z",
 *             "dateUpdated": "2017-03-23T13:45:30.635Z",
 *             "fades": 1,
 *             "users": 1
 *           }
 *         }
 *       },
 *       "result": 2
 *     }
 *   }
 */
const fetchImages = (schema) => async (ctx) => {
  try {
    const { creationId } = ctx.params

    const creation = await Creations
      .where({
        id: creationId,
        type: schema.ENTITY_NAME,
      })
      .fetch({
        columns: ['entity_id'],
        require: true,
      })
    const images = await Images
      .where({ entity_id: creation.get('entityId') })
      .fetchAll()
    const normalized = normalize(
      images.toJSON(),
      ImagesSchema.list,
    )

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    if (err instanceof Creations.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default fetchImages
