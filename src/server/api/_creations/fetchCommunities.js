import { normalize } from 'normalizr'

import { CreationsCommunities } from '../../models'
import { CommunitiesColumns, CommunitiesSchema } from '../../../schemas'
import { validatePagination } from '../../../utils'


/**
 * @apiDescription fetches a page of a creation's communities.
 * @api {GET} /api/{creationType}/{creationId}/communities Fetch Communities
 * @apiName fetchCommunities
 * @apiGroup creations
 * @apiPermission all
 * @apiVersion 0.1.1
 *
 * @apiSuccess {Array} fits page of creation communities
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "data": {
 *           "entities": {
 *               "communities": {
 *                   "1": {
 *                       "id": 1,
 *                       "creationId": 8,
 *                       "communityId": 1,
 *                       "communities": [
 *                           {
 *                               "id": 1,
 *                               "creationId": 17,
 *                               "title": "techwear",
 *                               "description": null,
 *                               "imageUrl": "/uploads/default-creation.png"
 *                           }
 *                       ]
 *                   }
 *               }
 *           },
 *           "result": [
 *               1
 *           ],
 *           "pagination": {
 *               "page": 1,
 *               "pageSize": 10,
 *               "rowCount": 1,
 *               "pageCount": 1
 *           }
 *       }
 *   }
 */
async function fetchCommunities(ctx) {
  try {
    const { creationId } = ctx.params
    const {
      pageSize,
      page,
      column,
      direction,
    } = await validatePagination(ctx.query)
    const communities = await CreationsCommunities
      .where({ creation_id: creationId })
      .orderBy(column, direction)
      .fetchPage({
        withRelated: {
          communities: (qb) => {
            qb.columns(CommunitiesColumns)
          },
        },
        pageSize,
        page,
      })
    const normalized = normalize(
      communities.toJSON(),
      CommunitiesSchema.communitiesList,
    )
    const { pagination } = communities

    ctx.status = 200
    ctx.body = {
      data: {
        ...normalized,
        pagination,
      },
    }
  } catch (err) {
    throw err
  }
}


export default fetchCommunities
