import { normalize } from 'normalizr'
import _reduce from 'lodash/reduce'

import config from '../../../../config'
import { Creations } from '../../models'
import {
  CreationsColumns,
  UsersColumns,
  UsersSchema,
} from '../../../schemas'
import { validatePagination } from '../../../utils'


const reduceChildColumns = (entityName) => (acc, prev) => ({
  ...acc,
  [`${entityName}${prev}`]: `${entityName}.${prev}`,
})

/**
 * @apiDescription fetches page of creations.
 * @api {GET} /api/{creations} Fetch Page
 * @apiName fetchPage
 * @apiGroup creations
 * @apiPermission user
 * @apiVersion 0.0.2
 *
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.entities   normalized data
 * @apiSuccess {Array}  data.results    array of result IDs
 * @apiSuccess {Object} data.pagination pagination data
 *
 * @apiError NotFoundError {Object} 400 Creation not found
 * @apiError Error         {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "entities": {
 *         "fades": {
 *           "1": {
 *             "id": 1,
 *             "creationId": 2,
 *             "title": "test fade",
 *             "description": "yo this is a pbj xx-012 fade",
 *             "imageUrl": "https://s-media-cache-ak0.pinimg.com/originals/1c/b8/93/1cb8932aa4eb7930925ac9931ec1000a.jpg"
 *           }
 *         },
 *         "users": {
 *           "1": {
 *             "id": 1,
 *             "username": "van"
 *           }
 *         },
 *         "creations": {
 *           "2": {
 *             "id": 2,
 *             "userId": 1,
 *             "type": "fades",
 *             "dateCreated": "2017-03-23T13:45:30.635Z",
 *             "dateUpdated": "2017-03-23T13:45:30.635Z",
 *             "users": 1,
 *             "fades": 1
 *           }
 *         }
 *       },
 *       "result": [
 *         2
 *       ],
 *       "pagination": {
 *         "page": 1,
 *         "pageSize": 10,
 *         "rowCount": 1,
 *         "pageCount": 1
 *       }
 *     }
 *   }
 */
const fetchPage = (columns, schema) => async (ctx) => {
  try {
    const {
      pageSize,
      page,
      column,
      direction,
    } = await validatePagination(ctx.query)
    const whereFilters = {
      'creations.type': schema.ENTITY_NAME,
    }
    if (
      schema.ENTITY_NAME === 'communities' &&
      config.VALID_COMMUNITY_TYPES.includes(ctx.query.filter)
    ) {
      const { filter } = ctx.query
      const singularFilter = filter.slice(0, filter.length - 1)
      whereFilters['communities.type'] = singularFilter
    }
    const creations = await Creations
      .query((qb) => {
        qb
          .innerJoin(
            schema.ENTITY_NAME,
            'creations.id',
            `${schema.ENTITY_NAME}.creation_id`,
          )
          .column(columns.reduce(
            reduceChildColumns(schema.ENTITY_NAME),
            {},
          ))
          .innerJoin(
            'users',
            'users.id',
            'creations.user_id',
          )
          .column(UsersColumns.reduce(
            reduceChildColumns(UsersSchema.ENTITY_NAME),
            {},
          ))
      })
      .where(whereFilters)
      .orderBy(column, direction)
      .fetchPage({
        columns: CreationsColumns.map((c) => `creations.${c}`),
        pageSize,
        page,
      })

    const creationsJSON = creations.toJSON().map((creation) => {
      const creationDetailObject = _reduce(creation, (acc, value, key) => {
        if (key.startsWith(`${schema.ENTITY_NAME}`)) {
          const actualKey = key.slice(schema.ENTITY_NAME.length)
          acc[actualKey] = value
          delete creation[key]
        }

        return acc
      }, {})
      const userObject = _reduce(creation, (acc, value, key) => {
        if (key.startsWith(`${UsersSchema.ENTITY_NAME}`)) {
          const actualKey = key.slice(UsersSchema.ENTITY_NAME.length)
          acc[actualKey] = value
          delete creation[key]
        }

        return acc
      }, {})
      creation[schema.ENTITY_NAME] = creationDetailObject
      creation.users = userObject
      return creation
    })
    const normalized = normalize(creationsJSON, schema.list)
    const { pagination } = creations

    ctx.status = 200
    ctx.body = {
      data: {
        ...normalized,
        pagination,
      },
    }
  } catch (err) {
    throw err
  }
}


export default fetchPage
