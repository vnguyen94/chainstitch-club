import Boom from 'boom'
import { normalize } from 'normalizr'

import { Creations } from '../../models'
import {
  CreationsColumns,
  UsersColumns,
  ImagesColumns,
} from '../../../schemas'


/**
 * @apiDescription fetches one creation.
 * @api {GET} /api/{creation}/:creationId Fetch One
 * @apiName fetchOne
 * @apiGroup creations
 * @apiPermission user
 * @apiVersion 0.0.2
 *
 * @apiSuccess {Object}  data
 * @apiSuccess {Object}  data.entities normalized data
 * @apiSuccess {Integer} data.result   result ID
 *
 * @apiError NotFoundError {Object} 400 Creation not found
 * @apiError Error         {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "entities": {
 *         "fades": {
 *           "1": {
 *             "id": 1,
 *             "creationId": 2,
 *             "title": "test fade",
 *             "description": "yo this is a pbj xx-012 fade",
 *             "imageUrl": "https://s-media-cache-ak0.pinimg.com/originals/1c/b8/93/1cb8932aa4eb7930925ac9931ec1000a.jpg"
 *           }
 *         },
 *         "users": {
 *           "1": {
 *             "id": 1,
 *             "username": "van"
 *           }
 *         },
 *         "creations": {
 *           "2": {
 *             "id": 2,
 *             "userId": 1,
 *             "type": "fades",
 *             "dateCreated": "2017-03-23T13:45:30.635Z",
 *             "dateUpdated": "2017-03-23T13:45:30.635Z",
 *             "fades": 1,
 *             "users": 1
 *           }
 *         }
 *       },
 *       "result": 2
 *     }
 *   }
 */
const fetchOne = (columns, schema) => async (ctx) => {
  try {
    const { creationId } = ctx.params
    const withRelated = {
      [schema.ENTITY_NAME]: (qb) => {
        qb.columns(columns)
      },
      users: (qb) => {
        qb.columns(UsersColumns)
      },
      'entities.images': (qb) => {
        qb.columns(ImagesColumns)
      },
    }
    const creation = await Creations
      .where({ id: creationId })
      .fetch({
        columns: CreationsColumns,
        require: true,
        withRelated,
      })
    const filteredCreation = creation.toJSON()
    filteredCreation.images = filteredCreation.entities.images
    delete filteredCreation.entities
    const normalized = normalize(
      filteredCreation,
      schema.single,
    )

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    if (err instanceof Creations.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default fetchOne
