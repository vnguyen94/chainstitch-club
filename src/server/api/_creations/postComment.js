import { camelCase } from 'lodash'
import Joi from 'joi'

import { Creations, Comments } from '../../models'
import { CreationsColumns } from '../../../schemas'
import { feed } from '../../lib'
import { validatePayload } from '../../../utils'


const validationSchema = Joi.object().keys({
  comment: Joi.string().required(),
})

/**
 * @apiDescription posts a comment to a creation.
 * @api {POST} /api/{creation}/:creationId/comments Post Comment
 * @apiName postComment
 * @apiGroup creations
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiParam {String} comment comment body
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 */
const postComment = (schema) => async (ctx) => {
  try {
    const { body } = ctx.request
    const { user } = ctx.state
    const { creationId } = ctx.params
    const creation = await Creations.findOne(
      { id: creationId },
      {
        columns: CreationsColumns,
        require: true,
      },
    )
    const recipientEntityId = creation.get('entityId')
    const payload = await validatePayload(body, validationSchema)

    const commentEntities = await Comments.post({
      user: ctx.state.user,
      body: payload.comment,
      recipientEntityId,
    })

    feed.addActivity({
      actor: feed.models.users(user.id),
      verb: 'comment',
      object: feed.models.comments(commentEntities.bookshelf.comment),
      target: feed.models[camelCase(schema.ENTITY_NAME)](creation),
    })

    ctx.status = 200
    ctx.body = {
      data: commentEntities.payload,
    }
  } catch (err) {
    throw err
  }
}


export default postComment
