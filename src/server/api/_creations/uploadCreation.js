import { camelCase } from 'lodash'

import config from '../../../../config'
import { validatePayload } from '../../../utils'
import db from '../../db'
import { Creations, Entities } from '../../models'
import { feed } from '../../lib'


/**
 * @apiDescription creates a creation type.
 * @api {POST} /api/{creation} Upload Creation
 * @apiName uploadCreation
 * @apiGroup creations
 * @apiPermission user
 * @apiVersion 0.0.1
 *
 * @apiSuccess {Object} data
 *
 * @apiError Error {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     redirect: true,
 *     redirectUrl: '/fits/7'
 *   }
 */
const uploadCreation = (
  columns,
  schema,
  Model,
  validationSchema,
) => async (ctx) => {
  try {
    const { body } = ctx.request
    const { user } = ctx.state

    const payload = await validatePayload(body, validationSchema)

    let creation

    await db.transaction(async (transacting) => {
      try {
        const entity = await Entities.create({}, { transacting })
        creation = await Creations.create({
          type: schema.ENTITY_NAME,
          user_id: user.id,
          entity_id: entity.get('id'),
        }, { transacting })
        return await Model.create({
          creation_id: creation.get('id'),
          image_url: config.DEFAULT_CREATION,
          ...payload,
        }, { transacting })
      } catch (err) {
        throw err
      }
    })

    feed.addActivity({
      actor: feed.models.users(user.id),
      verb: 'create',
      object: feed.models[camelCase(schema.ENTITY_NAME)](creation),
    })

    ctx.status = 200
    ctx.body = {
      data: {
        redirect: true,
        redirectUrl: `/${schema.ENTITY_NAME}/${creation.get('id')}`,
      },
    }
  } catch (err) {
    throw err
  }
}


export default uploadCreation
