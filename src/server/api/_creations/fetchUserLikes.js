import Boom from 'boom'
import Joi from 'joi'
import { normalize } from 'normalizr'

import { Creations, EntitiesLikes } from '../../models'
import { UsersColumns, UsersSchema } from '../../../schemas'
import { validatePayload } from '../../../utils'

const validationSchema = Joi.object().keys({
  creationId: Joi.number().integer().required(),
})


/**
 * @apiDescription fetches creation's likes.
 * @api {GET} /api/{creation}/:creationId Fetch User Likes
 * @apiName fetchUserLikes
 * @apiGroup Creations
 * @apiPermission creation
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object} likes entities the creation likes
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "data": {
 *           "entities": {
 *               "Creations": {
 *                   "2": {
 *                       "id": 2,
 *                       "username": "van2",
 *                       "imageUrl": "/uploads/default-avatar.png",
 *                       "creationId": 3
 *                   }
 *               }
 *           },
 *           "result": [
 *               2
 *           ]
 *       }
 *   }
 */
const fetchUserLikes = (schema) => async (ctx) => {
  try {
    const { creationId } = await validatePayload(ctx.params, validationSchema)
    const creation = await Creations
      .findOne({ id: creationId, type: schema.ENTITY_NAME }, {
        columns: ['entity_id'],
        require: true,
      })
    const entities = await EntitiesLikes
      .where({ entity_2_id: creation.get('entityId') })
      .fetchAll({
        withRelated: {
          likers: (qb) => {
            qb.columns([...UsersColumns, 'entity_id'])
          },
        },
      })
    const normalized = normalize(
      entities.toJSON().map((e) => e.likers),
      UsersSchema.list,
    )

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    if (err.isJoi) {
      throw Boom.badRequest()
    }
    if (err instanceof Creations.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default fetchUserLikes
