import { normalize } from 'normalizr'

import { Creations } from '../../models'
import { CreationsColumns, UsersColumns } from '../../../schemas'
import { validatePagination } from '../../../utils'


/**
 * @apiDescription fetches a page of a user's creations.
 * @api {GET} /api/users/:userId/{creation} Fetch User Creations
 * @apiName fetchUserCreations
 * @apiGroup users
 * @apiPermission all
 * @apiVersion 0.1.1
 *
 * @apiSuccess {Array} fits page of user fits
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "data": {
 *           "entities": {
 *               "fades": {
 *                   "1": {
 *                       "id": 1,
 *                       "creationId": 6,
 *                       "title": "fade1",
 *                       "description": "sdfd",
 *                       "imageUrl": "/uploads/file_1499152846883.png"
 *                   },
 *                   "2": {
 *                       "id": 2,
 *                       "creationId": 7,
 *                       "title": "titel",
 *                       "description": "sdds",
 *                       "imageUrl": "/uploads/file_1499152885747.png"
 *                   },
 *                   "3": {
 *                       "id": 3,
 *                       "creationId": 8,
 *                       "title": "dfdf",
 *                       "description": "dfdf",
 *                       "imageUrl": "/uploads/file_1499153290371.png"
 *                   },
 *                   "4": {
 *                       "id": 4,
 *                       "creationId": 9,
 *                       "title": "sdfdf",
 *                       "description": "dfdf",
 *                       "imageUrl": "/uploads/file_1499153417746.png"
 *                   },
 *                   "5": {
 *                       "id": 5,
 *                       "creationId": 10,
 *                       "title": "test",
 *                       "description": "testst",
 *                       "imageUrl": "/uploads/file_1499153500378.png"
 *                   }
 *               },
 *               "users": {
 *                   "1": {
 *                       "id": 1,
 *                       "username": "van"
 *                   }
 *               },
 *               "creations": {
 *                   "6": {
 *                       "id": 6,
 *                       "userId": 1,
 *                       "entityId": 12,
 *                       "type": "fades",
 *                       "createdAt": "2017-07-04T07:20:46.923Z",
 *                       "updatedAt": "2017-07-04T07:20:46.923Z",
 *                       "fades": 1,
 *                       "users": 1
 *                   },
 *                   "7": {
 *                       "id": 7,
 *                       "userId": 1,
 *                       "entityId": 13,
 *                       "type": "fades",
 *                       "createdAt": "2017-07-04T07:21:25.784Z",
 *                       "updatedAt": "2017-07-04T07:21:25.784Z",
 *                       "fades": 2,
 *                       "users": 1
 *                   },
 *                   "8": {
 *                       "id": 8,
 *                       "userId": 1,
 *                       "entityId": 14,
 *                       "type": "fades",
 *                       "createdAt": "2017-07-04T07:28:10.436Z",
 *                       "updatedAt": "2017-07-04T07:28:10.436Z",
 *                       "fades": 3,
 *                       "users": 1
 *                   },
 *                   "9": {
 *                       "id": 9,
 *                       "userId": 1,
 *                       "entityId": 15,
 *                       "type": "fades",
 *                       "createdAt": "2017-07-04T07:30:17.777Z",
 *                       "updatedAt": "2017-07-04T07:30:17.777Z",
 *                       "fades": 4,
 *                       "users": 1
 *                   },
 *                   "10": {
 *                       "id": 10,
 *                       "userId": 1,
 *                       "entityId": 16,
 *                       "type": "fades",
 *                       "createdAt": "2017-07-04T07:31:40.416Z",
 *                       "updatedAt": "2017-07-04T07:31:40.416Z",
 *                       "fades": 5,
 *                       "users": 1
 *                   }
 *               }
 *           },
 *           "result": [
 *               6,
 *               7,
 *               8,
 *               9,
 *               10
 *           ],
 *           "pagination": {
 *               "page": 1,
 *               "pageSize": 10,
 *               "rowCount": 5,
 *               "pageCount": 1
 *           }
 *       }
 *   }
 */
const fetchUserCreations = (columns, schema) => async (ctx) => {
  try {
    const { userId } = ctx.params
    const {
      pageSize,
      page,
      column,
      direction,
    } = await validatePagination(ctx.query)
    const withRelated = {
      [schema.ENTITY_NAME]: (qb) => {
        qb.columns(columns)
      },
      users(qb) {
        qb.columns(UsersColumns)
      },
    }
    const creations = await Creations
      .where({ user_id: userId, type: schema.ENTITY_NAME })
      .orderBy(column, direction)
      .fetchPage({
        columns: CreationsColumns,
        pageSize,
        page,
        withRelated,
      })
    const normalized = normalize(creations.toJSON(), schema.list)
    const { pagination } = creations


    ctx.status = 200
    ctx.body = {
      data: {
        ...normalized,
        pagination,
      },
    }
  } catch (err) {
    throw err
  }
}


export default fetchUserCreations
