import Router from 'koa-router'
import passport from 'koa-passport'

import authenticate from '../authentication'

import login from './login'
import sendChangeSecuritySettingsEmail from './sendChangeSecuritySettingsEmail'
import changeSecuritySettings from './changeSecuritySettings'

const router = new Router()

router.post(
  '/login',
  passport.authenticate('local', { session: false }),
  login,
)
router.post('/security', authenticate, sendChangeSecuritySettingsEmail)
router.patch('/security', authenticate, changeSecuritySettings)


export default router
