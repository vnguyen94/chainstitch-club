import { mail } from '../../lib'
import { UsersEmailVerificationTokens } from '../../models'


/**
 * @apiDescription send email to change email or password.
 * @api {POST} /api/auth/security Send Change Security Settings Email
 * @apiName sendChangeSecuritySettingsEmail
 * @apiGroup auth
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiParam {String} token verification token
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 */
export default async function sendChangeSecuritySettingsEmail(ctx) {
  try {
    const { user } = ctx.state

    const emailToken = await UsersEmailVerificationTokens.generate(user.id)
    mail.changeEmailAndPassword(user.email, emailToken)

    ctx.status = 204
  } catch (err) {
    throw err
  }
}
