import { pick } from 'lodash'

import { createWebToken } from '../../lib'
import { UsersProfiles } from '../../models'


/**
 * @apiDescription tries to login a user and
 *   sends back JWT if successful.
 * @api {POST} /api/auth Login
 * @apiName login
 * @apiGroup auth
 * @apiPermission any
 * @apiVersion 0.0.3
 *
 * @apiParam {String} emailOrUsername user emailOrUsername
 * @apiParam {String} password user password
 *
 * @apiSuccess {String} token JWT token
 * @apiSuccess {Object} user user credentials i.e. name, role
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "data": {
 *           "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicGFzc3dvcmQiOiIkMmEkMTAkTGc3NEU1STBTWk9sdi5qa2Jkb3VGLkk0eE54dUJCM1ZFaFQ0WEpTVnFDYzNEeHBuQ3c5WEciLCJpYXQiOjE0OTc1MTQ0MTMsImV4cCI6MTUyOTA3MjAxMywiYXVkIjoidXNlciIsImlzcyI6ImNoYWluc3RpdGNoIGNsdWIifQ.cPOysbDuffm9Uth6GIzHrlkzei_4Z_Tedq45FkUX3ks",
 *           "user": {
 *               "firstName": "Van",
 *               "lastName": "Nguyen",
 *               "email": "vnguyen94@gmail.com",
 *               "role": "admin"
 *           }
 *       }
 *   }
 *
 * @apiError NotFoundError emailOrUsername was not found.
 * @apiError AuthenticationError password was incorrect.
 *
 * @apiErrorExample {JSON} Error-Response:
 *   HTTP/1.1 400 Bad Request
 *   {
 *     "error": {
 *       "name": "NotFoundError",
 *       "message": "did not find user with specified emailOrUsername."
 *     }
 *   }
 */
export default async function login(ctx) {
  try {
    const { user } = ctx.state
    const token = createWebToken(pick(user, ['id', 'password', 'role']))
    const profile = await UsersProfiles.findOne(
      { id: user.id },
      {
        columns: ['first_name', 'last_name'],
        require: true,
      },
    )
    const userAttributes = pick(user, [
      'id',
      'username',
      'email',
      'imageUrl',
      'role',
    ])
    const profileAttributes = profile.pick('firstName', 'lastName')

    ctx.status = 200
    ctx.body = {
      data: {
        user: {
          ...userAttributes,
          ...profileAttributes,
        },
        token,
      },
    }
  } catch (err) {
    throw err
  }
}
