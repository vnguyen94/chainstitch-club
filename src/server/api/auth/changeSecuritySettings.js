import Joi from 'joi'
import Boom from 'boom'

import { validatePayload } from '../../../utils'
import { UsersEmailVerificationTokens, Users } from '../../models'
import { createPasswordHash } from '../../lib'


const validationSchema = Joi.object().keys({
  email: Joi.string(),
  password: Joi.string(),
})

/**
 * @apiDescription if correct token, able to change email or password.
 * @api {PATCH} /api/auth/security Change Security Settings
 * @apiName changeSecuritySettings
 * @apiGroup auth
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiParam {String} token verification token
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 *
 * @apiError NotFoundError token was not found.
 *
 * @apiErrorExample {JSON} Error-Response:
 *   HTTP/1.1 400 Bad Request
 *   {
 *     "error": {
 *       "name": "NotFoundError",
 *       "message": "did not find user with specified token."
 *     }
 *   }
 */
export default async function changeSecuritySettings(ctx) {
  try {
    const { user } = ctx.state
    const { body } = ctx.request
    const {
      email,
      password,
      token,
    } = await validatePayload(body, validationSchema)

    await UsersEmailVerificationTokens.findOne(
      { user_id: user.id, token },
      { require: true },
    )

    const patchData = {}

    if (email) {
      patchData.email = email
    }
    if (password) {
      patchData.password = await createPasswordHash(password)
    }

    await Users.update(patchData, { id: user.id })

    ctx.status = 204
  } catch (err) {
    if (err instanceof UsersEmailVerificationTokens.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}
