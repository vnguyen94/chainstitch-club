import Boom from 'boom'
import Joi from 'joi'
import { toInteger } from 'lodash'

import {
  Users,
  UsersNotifications,
  Creations,
  EntitiesFollows,
  EntitiesLikes,
} from '../../models'
import {
  CreationsColumns,
  UsersColumns,
  UsersProfilesColumns,
  UsersNotificationsColumns,
} from '../../../schemas'
import { validatePayload } from '../../../utils'


const validationSchema = Joi.object().keys({
  userId: Joi.number().integer().required(),
})

async function userPromise(user, userId) {
  const columns = user && user.id === userId
    ? [...UsersColumns, 'email', 'entity_id']
    : [...UsersColumns, 'entity_id']
  const promise = Users.findById(userId, {
    require: true,
    columns,
  })

  return promise
}

async function userNotificationsPromise(userId) {
  const promise = UsersNotifications.findOne({ userId }, {
    columns: UsersNotificationsColumns,
    require: true,
  })

  return promise
}

async function creationPromise(userId) {
  const withRelated = {
    usersProfiles(qb) {
      qb.columns(UsersProfilesColumns)
    },
  }
  const promise = Creations.findOne(
    { type: 'users_profiles', userId },
    {
      columns: CreationsColumns,
      require: true,
      withRelated,
    },
  )

  return promise
}

async function followersCountPromise(entityId) {
  return EntitiesFollows
    .where({ entity_2_id: entityId })
    .count()
}

async function followingCountPromise(entityId) {
  return EntitiesFollows
    .where({ entity_1_id: entityId })
    .count()
}

async function likesCountPromise(entityId) {
  return EntitiesLikes
    .where({ entity_1_id: entityId })
    .count()
}

async function fitsCountPromise(userId) {
  return Creations
    .where({ type: 'fits', user_id: userId })
    .count()
}

async function fadesCountPromise(userId) {
  return Creations
    .where({ type: 'fades', user_id: userId })
    .count()
}

/**
 * @apiDescription fetches a user's profile.
 * @api {GET} /api/users/:userId Fetch User Profile
 * @apiName fetchUserProfile
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Array} fits user profile data
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *      "data": {
 *          "user": {
 *              "id": 2,
 *              "username": "van2",
 *              "email": "vnguyen@supplyframe.com"
 *          },
 *          "profile": {
 *              "id": 2,
 *              "creationId": 2,
 *              "firstName": "van",
 *              "lastName": "van",
 *              "gender": null,
 *              "location": null,
 *              "description": null
 *          },
 *          "notifications": {
 *              "userId": 2,
 *              "id": 1,
 *              "messages": true,
 *              "followers": true,
 *              "likers": true,
 *              "feed": true
 *          }
 *      }
 *  }
 */
async function fetchUserProfile(ctx) {
  try {
    const { userId } = await validatePayload(ctx.params, validationSchema)
    const [user, notifications, creation] = await Promise.all([
      userPromise(ctx.state.user, userId),
      userNotificationsPromise(userId),
      creationPromise(userId),
    ])
    let isFollowingPromise = Promise.resolve(false)

    if (ctx.state.user) {
      const entity1Id = ctx.state.user.entityId
      const entity2Id = user.get('entityId')

      isFollowingPromise = Users.isFollowing(entity1Id, entity2Id)
    }

    const isFollowing = await isFollowingPromise
    user.set({ isFollowing })

    const entityId = user.get('entityId')
    const profile = creation.related('usersProfiles').toJSON()
    const response = { user, profile, notifications }

    if (ctx.query.counts) {
      const [
        followers,
        following,
        fits,
        fades,
        likes,
      ] = await Promise.all([
        followersCountPromise(entityId),
        followingCountPromise(entityId),
        fitsCountPromise(userId),
        fadesCountPromise(userId),
        likesCountPromise(entityId),
      ])

      response.counts = {
        followers: toInteger(followers),
        following: toInteger(following),
        fits: toInteger(fits),
        fades: toInteger(fades),
        likes: toInteger(likes),
      }
    }
    if (ctx.query.topCreations) {
      response.creations = await Creations.fetchUserTopCreations(userId)
    }

    ctx.status = 200
    ctx.body = {
      data: response,
    }
  } catch (err) {
    if (
      err instanceof Creations.NotFoundError ||
      err instanceof Users.NotFoundError ||
      err instanceof UsersNotifications.NotFoundError
    ) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default fetchUserProfile
