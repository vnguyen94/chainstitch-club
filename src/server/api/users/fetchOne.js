import Boom from 'boom'
import Joi from 'joi'
import { normalize } from 'normalizr'

import { Users } from '../../models'
import {
  UsersColumns,
  UsersSchema,
} from '../../../schemas'
import { validatePayload } from '../../../utils'

const validationSchema = Joi.object().keys({
  userId: Joi.number().integer().required(),
})


/**
 * @apiDescription fetches one user.
 * @api {GET} /api/users/:userId Fetch One
 * @apiName fetchOne
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.0.1
 *
 * @apiSuccess {Object} user found user
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "id": 47,
 *       "username": "tester",
 *       "firstName": "test1111",
 *       "lastName": "user",
 *     }
 *   }
 */
async function fetchOne(ctx) {
  try {
    const payload = await validatePayload(ctx.params, validationSchema)
    let isFollowingPromise = Promise.resolve(false)
    let hasUserLikedPromise = Promise.resolve(false)

    const user = await Users.findById(payload.userId, {
      columns: [...UsersColumns, 'entity_id'],
      require: true,
    })

    if (ctx.state.user) {
      const entity1Id = ctx.state.user.entityId
      const entity2Id = user.get('entityId')

      isFollowingPromise = Users.isFollowing(entity1Id, entity2Id)
      hasUserLikedPromise = Users.hasUserLiked(entity1Id, entity2Id)
    }

    const [isFollowing, hasUserLiked] = await Promise.all([
      isFollowingPromise,
      hasUserLikedPromise,
    ])
    user.set({ isFollowing, hasUserLiked })
    const normalized = normalize(user.toJSON(), UsersSchema.single)

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    if (err.isJoi) {
      throw Boom.badRequest()
    }
    if (err instanceof Users.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default fetchOne
