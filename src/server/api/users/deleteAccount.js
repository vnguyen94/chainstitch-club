import { Users } from '../../models'


/**
 * @apiDescription deletes a user account.
 * @api {DELETE} /api/users Delete Account
 * @apiName deleteAccount
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object} user the created user
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 */
export default async function deleteAccount(ctx) {
  try {
    const { user } = ctx.state
    await Users.update({ is_deleted: true }, { id: user.id })

    ctx.status = 204
  } catch (err) {
    throw err
  }
}
