import { normalize } from 'normalizr'

import { UsersProfiles, Comments } from '../../models'
import {
  CommentsColumns,
  CommentsSchema,
  CreationsColumns,
  UsersColumns,
} from '../../../schemas'
import { validatePagination } from '../../../utils'


/**
 * @apiDescription fetches page of user profile comments.
 * @api {GET} /api/{creations} Fetch Comments User Profile
 * @apiName fetchCommentsUserProfile
 * @apiGroup users
 * @apiPermission all
 * @apiVersion 0.0.2
 *
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.entities   normalized data
 * @apiSuccess {Array}  data.results    array of result IDs
 * @apiSuccess {Object} data.pagination pagination data
 *
 * @apiError NotFoundError {Object} 400 User not found
 * @apiError Error         {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "entities": {
 *         "creations": {
 *           "14": {
 *             "id": 14,
 *             "userId": 1,
 *             "entityId": 20,
 *             "type": "comments",
 *             "createdAt": "2017-07-05T19:40:53.833Z",
 *             "updatedAt": "2017-07-05T19:40:53.833Z"
 *           }
 *         },
 *         "comments": {
 *           "4": {
 *             "id": 4,
 *             "creationId": 14,
 *             "recipientEntityId": 4,
 *             "body": "<p>sdfsdfsd</p>",
 *             "creations": 14
 *           }
 *         },
 *         "users": {
 *           "1": {
 *             "id": 1,
 *             "username": "test"
 *           }
 *         }
 *       },
 *       "result": [
 *         4
 *       ],
 *       "pagination": {
 *         "page": 1,
 *         "pageSize": 10,
 *         "rowCount": 1,
 *         "pageCount": 1
 *       }
 *     }
 *   }
 */
export default async function fetchCommentsUserProfile(ctx) {
  try {
    const { userId } = ctx.params
    const {
      pageSize,
      page,
      column,
      direction,
    } = await validatePagination(ctx.query)
    const userProfile = await UsersProfiles.findOne(
      { userId },
      {
        withRelated: ['creations.entities'],
        require: true,
      },
    )
    const recipientEntityId = userProfile
      .related('creations')
      .related('entities')
      .get('id')
    const withRelated = {
      creations(qb) {
        qb.columns(CreationsColumns)
      },
      'creations.users': (qb) => {
        qb.columns(UsersColumns)
      },
    }
    const comments = await Comments
      .where({ recipient_entity_id: recipientEntityId })
      .orderBy(column, direction)
      .fetchPage({
        columns: CommentsColumns,
        pageSize,
        page,
        withRelated,
      })
    const normalized = normalize(
      comments.toJSON(),
      CommentsSchema.list,
    )
    const { pagination } = comments

    ctx.status = 200
    ctx.body = {
      data: {
        ...normalized,
        pagination,
      },
    }
  } catch (err) {
    throw err
  }
}
