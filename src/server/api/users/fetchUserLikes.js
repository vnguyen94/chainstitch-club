import Boom from 'boom'
import Joi from 'joi'
import { normalize } from 'normalizr'

import { Users, EntitiesLikes } from '../../models'
import {
  UsersColumns,
  UsersSchema,
} from '../../../schemas'
import { validatePayload } from '../../../utils'

const validationSchema = Joi.object().keys({
  userId: Joi.number().integer().required(),
})


/**
 * @apiDescription fetches user's likes.
 * @api {GET} /api/users/:userId/likes Fetch User Likes
 * @apiName fetchUserLikes
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object} likes entities the user likes
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "data": {
 *           "entities": {
 *               "users": {
 *                   "2": {
 *                       "id": 2,
 *                       "username": "van2",
 *                       "imageUrl": "/uploads/default-avatar.png",
 *                       "entityId": 3
 *                   }
 *               }
 *           },
 *           "result": [
 *               2
 *           ]
 *       }
 *   }
 */
const fetchUserLikes = (entityType) => async (ctx) => {
  try {
    const { userId } = await validatePayload(ctx.params, validationSchema)
    const user = await Users
      .findById(userId, {
        columns: [...UsersColumns, 'entity_id'],
        require: true,
      })
    const entities = await EntitiesLikes
      .where({ [entityType]: user.get('entityId') })
      .fetchAll({
        withRelated: {
          likers: (qb) => {
            qb.columns([...UsersColumns, 'entity_id'])
          },
        },
      })
    const normalized = normalize(
      entities.toJSON().map((e) => e.likers),
      UsersSchema.list,
    )

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    if (err.isJoi) {
      throw Boom.badRequest()
    }
    if (err instanceof Users.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default fetchUserLikes
