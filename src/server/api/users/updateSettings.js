import Joi from 'joi'
import Boom from 'boom'
import { isEmpty, pick } from 'lodash'

import {
  Users,
  UsersProfiles,
  UsersNotifications,
} from '../../models'
import { createPasswordHash } from '../../lib'
import { validatePayload } from '../../../utils'


const validationSchema = Joi.object().keys({
  // user
  username: Joi.string(),

  // profile
  firstName: Joi.string().allow(''),
  lastName: Joi.string().allow(''),
  gender: Joi.string().valid(['male', 'female', 'other']),
  location: Joi.string().allow(''),
  description: Joi.string().allow(''),

  // notifications
  notifMessages: Joi.boolean(),
  notifFollowers: Joi.boolean(),
  notifLikers: Joi.boolean(),
  notifFeed: Joi.boolean(),
})


/**
 * @apiDescription update a user's settings.
 * @api {PATCH} /api/users/settings Update Settings
 * @apiName updateSettings
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.1.1
 *
 * @apiParam {String} id id of user to updateSettings
 * @apiParam {Boolean} action updateSettings/unupdateSettings
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 *
 * @apiErrorExample {JSON} Bad Request
 *   HTTP/1.1 400
 *   {
 *       "statusCode": 400,
 *       "error": "Bad Request",
 *       "message": "no parameters provided."
 *   }
 */
export default async function updateSettings(ctx) {
  try {
    const { body } = ctx.request
    const { user } = ctx.state
    const payload = await validatePayload(body, validationSchema)

    if (isEmpty(payload)) {
      throw Boom.badRequest('no parameters provided.')
    }

    const userPayload = pick(
      payload,
      [
        'username',
      ],
    )
    const profilePayload = pick(
      payload,
      [
        'firstName',
        'lastName',
        'gender',
        'location',
        'description',
      ],
    )
    const notificationPayload = pick(
      payload,
      [
        'notifMessages',
        'notifFollowers',
        'notifLikers',
        'notifFeed',
      ],
    )

    if (!isEmpty(userPayload)) {
      if (userPayload.password) {
        userPayload.password = await createPasswordHash(payload.password)
      }

      await Users.update(userPayload, { id: user.id })
    }
    if (!isEmpty(profilePayload)) {
      await UsersProfiles.update(profilePayload, { id: user.id })
    }
    if (!isEmpty(notificationPayload)) {
      await UsersNotifications.update(
        {
          messages: notificationPayload.notifMessages,
          followers: notificationPayload.notifFollowers,
          likers: notificationPayload.notifLikers,
          feed: notificationPayload.notifFeed,
        },
        { id: user.id },
      )
    }

    ctx.status = 204
  } catch (err) {
    if (err.constraint === 'users_username_unique') {
      throw Boom.badRequest('username already taken.')
    }
    if (err.constraint === 'users_email_unique') {
      throw Boom.badRequest('email already taken.')
    }

    throw err
  }
}
