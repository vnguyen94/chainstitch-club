import Boom from 'boom'
import Joi from 'joi'
import { normalize } from 'normalizr'

import { Users, EntitiesFollows } from '../../models'
import {
  UsersColumns,
  UsersSchema,
} from '../../../schemas'
import { validatePayload } from '../../../utils'

const validationSchema = Joi.object().keys({
  userId: Joi.number().integer().required(),
})


/**
 * @apiDescription fetches user's follows.
 * @api {GET} /api/users/:userId/follows Fetch User Likes
 * @apiName fetchUserFollows
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object} follows entities the user follows
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "data": {
 *           "entities": {
 *               "users": {
 *                   "2": {
 *                       "id": 2,
 *                       "username": "van2",
 *                       "imageUrl": "/uploads/default-avatar.png",
 *                       "entityId": 3
 *                   }
 *               }
 *           },
 *           "result": [
 *               2
 *           ]
 *       }
 *   }
 */
const fetchUserFollows = (entityType) => async (ctx) => {
  try {
    const { userId } = await validatePayload(ctx.params, validationSchema)
    const user = await Users
      .findById(userId, {
        columns: [...UsersColumns, 'entity_id'],
        require: true,
      })
    const entities = await EntitiesFollows
      .where({ [entityType]: user.get('entityId') })
      .fetchAll({
        withRelated: {
          followers: (qb) => {
            qb.columns([...UsersColumns, 'entity_id'])
          },
        },
      })
    const normalized = normalize(
      entities.toJSON().map((e) => e.followers),
      UsersSchema.list,
    )

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    if (err.isJoi) {
      throw Boom.badRequest()
    }
    if (err instanceof Users.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}


export default fetchUserFollows
