import Joi from 'joi'
import Boom from 'boom'

import { EntitiesFollows, Users } from '../../models'
import { feed } from '../../lib'
import { validatePayload } from '../../../utils'


const validationSchema = Joi.object().keys({
  userId: Joi.number().integer().required(),
})

/**
 * @apiDescription follows or unfollows a user.
 * @api {POST|DELETE} /api/users/:userId/follows Follow
 * @apiName follow
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.1.1
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 */
export default async function follow(ctx) {
  try {
    const { user } = ctx.state
    const {
      userId: followeeId,
    } = await validatePayload(ctx.params, validationSchema)

    if (user.id === followeeId) {
      throw Boom.badRequest('cannot follow yourself.')
    }

    const followee = await Users.findOne({ id: followeeId }, { require: true })

    if (ctx.method === 'POST') {
      await EntitiesFollows.follow(user.entityId, followee.get('entityId'))

      feed.addActivity({
        actor: feed.models.users(user.id),
        verb: 'follow',
        object: feed.models.users(followeeId),
      })
    } else { // DELETE
      await EntitiesFollows.unfollow(user.entityId, followee.get('entityId'))
    }

    ctx.status = 204
  } catch (err) {
    if (err instanceof Users.NotFoundError) {
      throw Boom.badRequest('user not found.')
    }
    if (err instanceof EntitiesFollows.NoRowsDeletedError) {
      throw Boom.badRequest('you are not following this user.')
    }
    if (err.constraint === 'entities_follows_entity_1_id_entity_2_id_unique') {
      throw Boom.badRequest('you have already followed this user.')
    }

    throw err
  }
}
