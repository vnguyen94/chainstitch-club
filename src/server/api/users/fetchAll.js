import { normalize } from 'normalizr'

import { Users } from '../../models'
import {
  UsersColumns,
  UsersSchema,
} from '../../../schemas'


/**
 * @apiDescription fetches all users.
 * @api {GET} /api/users Fetch All
 * @apiName fetchAll
 * @apiGroup users
 * @apiPermission admin
 * @apiVersion 0.0.1
 *
 * @apiSuccess {Array} users all found users
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": [
 *       {
 *         "id": 47,
 *         "username": "tester",
 *         "firstName": "test1111",
 *         "lastName": "user",
 *         "email": "test111@test.com",
 *         "role": "user",
 *         "created_at": "2016-09-08T17:24:21.242Z",
 *         "updated_at": "2016-09-08T17:24:21.242Z",
 *         "is_deleted": false
 *       }
 *     ]
 *   }
 */
async function fetchAll(ctx) {
  try {
    const users = await Users
      .forge()
      .fetchAll({
        columns: UsersColumns,
      })
    const normalized = normalize(users.toJSON(), UsersSchema.list)

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    throw err
  }
}


export default fetchAll
