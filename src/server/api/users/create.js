import Joi from 'joi'

import { Users } from '../../models'
import { feed } from '../../lib'
import { validatePayload } from '../../../utils'


const validationSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  username: Joi.string().required(),

  firstName: Joi.string(),
  lastName: Joi.string(),
})

/**
 * @apiDescription creates a user.
 * @api {POST} /api/users Create
 * @apiName Create
 * @apiGroup users
 * @apiPermission all
 * @apiVersion 0.0.3
 *
 * @apiParam {String} firstName user first name
 * @apiParam {String} lastName user last name
 * @apiParam {String} email user email
 * @apiParam {String} password user password
 *
 * @apiSuccess {Object} user the created user
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     "data": {
 *       "firstName": "test1111",
 *       "lastName": "user",
 *       "email": "test111@test.com",
 *     }
 *   }
 *
 * @apiError IncorrectParametersError invalid parameters.
 * @apiError ResourceExistsError email already taken.
 *
 * @apiErrorExample {JSON} Error-Response:
 *   HTTP/1.1 400 Bad Request
 *   {
 *     "error": {
 *       "name": "IncorrectParametersError",
 *       "message": "child \"email\" fails because [\"email\" is required]"
 *     }
 *   }
 */
export default async function create(ctx) {
  try {
    const { body } = ctx.request
    const payload = await validatePayload(body, validationSchema)
    const userData = await Users.createUser(payload)
    const userId = userData.user.id

    feed.addActivity({
      actor: feed.models.users(userId),
      verb: 'signup',
      object: feed.models.users(userId),
    })

    ctx.status = 200
    ctx.body = { data: userData }
  } catch (err) {
    throw err
  }
}
