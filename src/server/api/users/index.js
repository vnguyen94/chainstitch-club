import Router from 'koa-router'

import {
  FitsColumns,
  FitsSchema,
  FadesColumns,
  FadesSchema,
} from '../../../schemas'
import authenticate from '../authentication'
import { fetchUserCreations } from '../_creations'

import create from './create'
import follow from './follow'
import deleteAccount from './deleteAccount'
import updateSettings from './updateSettings'
import uploadImage from './uploadImage'
import fetchAll from './fetchAll'
import fetchOne from './fetchOne'
import fetchUserFollows from './fetchUserFollows'
import fetchUserLikes from './fetchUserLikes'
import fetchUserProfile from './fetchUserProfile'
import fetchCommentsUserProfile from './fetchCommentsUserProfile'
import postCommentUserProfile from './postCommentUserProfile'

const router = new Router()


router.get('/', fetchAll)
router.post('/', create)
router.patch('/', authenticate, updateSettings)
router.delete('/', authenticate, deleteAccount)
router.post('/images', authenticate, uploadImage)
router.get('/:userId', fetchOne)
router.get('/:userId/fits', fetchUserCreations(FitsColumns, FitsSchema))
router.get('/:userId/fades', fetchUserCreations(FadesColumns, FadesSchema))
router.get('/:userId/likes', fetchUserLikes('entity_1_id'))
router.get('/:userId/likers', fetchUserLikes('entity_2_id'))
router.get('/:userId/following', fetchUserFollows('entity_1_id'))
router.get('/:userId/followers', fetchUserFollows('entity_2_id'))
router.get('/:userId/profile', fetchUserProfile)
router.get('/:userId/profile/comments', fetchCommentsUserProfile)
router.post('/:userId/profile/comments', authenticate, postCommentUserProfile)
router.post('/:userId/follows', authenticate, follow)
router.delete('/:userId/follows', authenticate, follow)


export default router
