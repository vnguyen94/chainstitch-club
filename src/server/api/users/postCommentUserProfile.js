import Joi from 'joi'

import { UsersProfiles, Comments } from '../../models'
import { feed } from '../../lib'
import { validatePayload } from '../../../utils'


const validationSchema = Joi.object().keys({
  comment: Joi.string().required(),
})

/**
 * @apiDescription posts a comment to a user's profile.
 * @api {POST} /api/users Post Comment User Profile
 * @apiName postCommentUserProfile
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiParam {String} comment comment body
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 */
export default async function postCommentUserProfile(ctx) {
  try {
    const { body } = ctx.request
    const { user } = ctx.state
    const { userId } = ctx.params
    const withRelated = {
      creations(qb) {
        qb.columns(['id', 'entity_id'])
      },
    }
    const userProfile = await UsersProfiles.findOne(
      { userId },
      {
        columns: ['creation_id'],
        require: true,
        withRelated,
      },
    )
    const recipientEntityId = userProfile
      .related('creations')
      .get('entityId')
    const payload = await validatePayload(body, validationSchema)

    const comment = await Comments.post({
      body: payload.comment,
      user,
      recipientEntityId,
    })

    feed.addActivity({
      actor: feed.models.users(user.id),
      verb: 'comment',
      object: feed.models.comments(comment),
      target: feed.models.usersProfiles(userProfile.related('creations')),
    }, {}, { allowUnknown: true })

    ctx.status = 204
  } catch (err) {
    throw err
  }
}
