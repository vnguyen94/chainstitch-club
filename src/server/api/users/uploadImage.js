import Joi from 'joi'

import { validatePayload } from '../../../utils'
import { uploadImage as libUploadImage } from '../../lib'
import { Images, Users } from '../../models'


const validationSchema = Joi.object().keys({
  image: Joi.object().required(),
})

/**
 * @apiDescription creates an image.
 * @api {POST} /api/users/images Upload Image
 * @apiName uploadImage
 * @apiGroup users
 * @apiPermission user
 * @apiVersion 0.0.5
 *
 * @apiError Error {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *     results: 'cdn.chainstitch.club/images/15364646.jpg'
 *   }
 */
async function uploadImage(ctx) {
  try {
    const { body } = ctx.request
    const user = await Users.findById(
      ctx.state.user.id,
      { columns: ['entity_id'], require: true },
    )
    const entityId = user.get('entityId')
    const payload = await validatePayload(body.files, validationSchema)
    const [originalImage, imageCount] = await Promise.all([
      libUploadImage(payload.image),
      Images.where({ entity_id: entityId }).count(),
    ])

    await Images.create({
      url: originalImage.path,
      height: originalImage.height,
      width: originalImage.width,
      order: parseFloat(imageCount) + 1,
      entityId,
    })

    if (ctx.query.profileImage) {
      Users.update(
        { image_url: originalImage.path },
        { id: ctx.state.user.id },
      )
    }

    ctx.status = 200
    ctx.body = {
      results: originalImage.path,
    }
  } catch (err) {
    throw err
  }
}


export default uploadImage
