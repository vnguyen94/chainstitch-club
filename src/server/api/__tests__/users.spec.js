import supertest from 'supertest'

import config from '../../../../config'
import app from '../../app'


describe('Users', () => {
  let server
  let request

  beforeAll((done) => {
    server = app.listen(config.SERVER_PORT, done)
    request = supertest(server)
  })

  it('tests `fetchOne`', async () => {
    const res = await request.get('/api/users/1')

    expect(res.type).toEqual('application/json')
    expect(res.status).toEqual(200)
    expect(res.body).toHaveProperty('data.entities')
    expect(res.body).toHaveProperty('data.result', 1)
  })

  afterAll(() => {
    server.close()
  })
})
