import Boom from 'boom'


export default async function authenticate(ctx, next) {
  const { user } = ctx.state

  if (!user) {
    throw Boom.unauthorized('you are not logged in.')
  }
  if (!['user', 'admin'].includes(user.role)) {
    throw Boom.forbidden('you do not have the privileges.')
  }

  await next()
}
