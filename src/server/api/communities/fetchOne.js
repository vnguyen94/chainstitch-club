import Boom from 'boom'
import { normalize } from 'normalizr'

import { Communities, CommunitiesCreations } from '../../models'
import {
  CommunitiesColumns,
  CommunitiesSchema,
} from '../../../schemas'


/**
 * @apiDescription fetches one community.
 * @api {GET} /api/{community}/:creationId Fetch One
 * @apiName fetchOne
 * @apiGroup communities
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object}  data
 * @apiSuccess {Object}  data.entities normalized data
 * @apiSuccess {Integer} data.result   result ID
 *
 * @apiError NotFoundError {Object} 400 Community not found
 * @apiError Error         {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *       "data": {
 *           "entities": {
 *               "communities": {
 *                   "1": {
 *                       "id": 1,
 *                       "entityId": 40,
 *                       "title": "techwear",
 *                       "imageUrl": "/uploads/file_1497231358581.jpeg",
 *                       "createdAt": "2017-08-13T22:47:03.732Z"
 *                   }
 *               }
 *           },
 *           "result": 1
 *       }
 *   }
 */
export default async function fetchOne(ctx) {
  try {
    const { creationId } = ctx.params
    const { related } = ctx.query

    const community = await Communities
      .where({ creation_id: creationId })
      .fetch({
        columns: CommunitiesColumns,
        require: true,
      })
    const communityJSON = community.toJSON()

    if (related === 'creations') {
      const communityCreations = await CommunitiesCreations
        .where({ community_id: community.get('id') })
        .fetchAll({
          withRelated: [
            'creations.fades',
            'creations.fits',
            'creations.users',
          ],
        })
      const creations = communityCreations
        .toJSON()
        .map((c) => c.creations)

      communityJSON.creations = creations
    }

    const normalized = normalize(
      communityJSON,
      CommunitiesSchema.single,
    )

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    if (err instanceof Communities.NotFoundError) {
      throw Boom.notFound()
    }

    throw err
  }
}
