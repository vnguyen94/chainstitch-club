import Router from 'koa-router'
import Joi from 'joi'

import { CommunitiesColumns, CommunitiesSchema } from '../../../schemas'
import {
  fetchCommunities,
  fetchImages,
  fetchPage,
  uploadImage,
  uploadCreation,
  like,
  fetchComments,
  fetchUserLikes,
  postComment,
} from '../_creations'
import { Communities } from '../../models'
import authenticate from '../authentication'

import addCreation from './addCreation'
import fetchOne from './fetchOne'
import searchCommunities from './searchCommunities'

const router = new Router()

const validationSchema = Joi.object().keys({
  title: Joi.string().required(),
  description: Joi.string(),
})

router.get('/', fetchPage(CommunitiesColumns, CommunitiesSchema))
router.post(
  '/',
  authenticate,
  uploadCreation(
    CommunitiesColumns,
    CommunitiesSchema,
    Communities,
    validationSchema,
  ),
)
router.get('/search', searchCommunities)
router.get('/:creationId', fetchOne)
router.get('/:creationId/comments', fetchComments)
router.post(
  '/:creationId/comments',
  authenticate,
  postComment(CommunitiesSchema),
)
router.get('/:creationId/communities', fetchCommunities)
router.post('/:creationId/creations', authenticate, addCreation)
router.get('/:creationId/images', fetchImages(CommunitiesSchema))
router.post('/:creationId/images', authenticate, uploadImage)
router.get('/:creationId/likes', fetchUserLikes(CommunitiesSchema))
router.post(
  '/:creationId/likes',
  authenticate,
  like(Communities, CommunitiesSchema),
)
router.delete(
  '/:creationId/likes',
  authenticate,
  like(Communities, CommunitiesSchema),
)


export default router
