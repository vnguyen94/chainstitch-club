import Boom from 'boom'
import { normalize } from 'normalizr'

import db from '../../db'
import {
  CommunitiesSchema,
} from '../../../schemas'


/**
 * @apiDescription searches communities by title.
 * @api {GET} /api/communities/search Search Communities
 * @apiName searchCommunities
 * @apiGroup communities
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object}  data
 * @apiSuccess {Object}  data.entities normalized data
 * @apiSuccess {Integer} data.result   result ID
 *
 * @apiError Error         {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *      "data": {
 *          "entities": {
 *              "communities": {
 *                  "1": {
 *                      "id": 1,
 *                      "creation_id": 17,
 *                      "title": "techwear",
 *                      "image_url": "/uploads/default-creation.png",
 *                      "description": null
 *                  },
 *                  "2": {
 *                      "id": 2,
 *                      "creation_id": 18,
 *                      "title": "techwear",
 *                      "image_url": "/uploads/default-creation.png",
 *                      "description": null
 *                  }
 *              }
 *          },
 *          "result": [
 *              1,
 *              2
 *          ]
 *      }
 *   }
 */
export default async function searchCommunities(ctx) {
  try {
    const { query } = ctx.query

    if (!query) {
      throw Boom.badRequest('missing `query` query parameter.')
    }

    const community = await db.knex.raw(`
      SELECT *
      FROM communities
      WHERE title LIKE '%${query}%'
    `)
    const normalized = normalize(
      community.rows,
      CommunitiesSchema.communitiesList,
    )

    ctx.status = 200
    ctx.body = { data: normalized }
  } catch (err) {
    throw err
  }
}
