import Joi from 'joi'
import Boom from 'boom'

import config from '../../../../config'
import { validatePayload } from '../../../utils'
import {
  Creations,
  Communities,
  CommunitiesCreations,
} from '../../models'


const validationSchema = Joi.object().keys({
  creationId: Joi.number().required(),
})

function getCreation(creationId) {
  return Creations.findById(
    creationId,
    {
      columns: ['id', 'type'],
      require: true,
    },
  )
}

function getCommunity(communityId) {
  return Communities
    .where({ creation_id: communityId })
    .fetch({
      columns: ['id'],
      require: true,
    })
}

/**
 * @apiDescription adds a creation to a community.
 * @api {POST} /api/communities/:creationId/creations Add Creation
 * @apiName addCreation
 * @apiGroup communities
 * @apiPermission user
 * @apiVersion 0.0.3
 *
 * @apiError NotFoundError   {Object} 400 Creation not found
 * @apiError BadRequestError {Object} 400 Not the owner
 * @apiError BadRequestError {Object} 400 Incorrect creation type
 * @apiError BadRequestError {Object} 400 Creation already added
 * @apiError Error           {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 204 OK
 */
export default async function addCreation(ctx) {
  try {
    const { body } = ctx.request
    const { user } = ctx.state
    const { creationId: communityId } = ctx.params
    const { creationId } = await validatePayload(body, validationSchema)
    const [creation, community] = await Promise.all([
      getCreation(creationId),
      getCommunity(communityId),
    ])

    if (user.id !== creation.get('userId')) {
      throw Boom.badRequest('you are not the owner of this creation.')
    }
    if (!config.VALID_COMMUNITY_CREATIONS.includes(creation.get('type'))) {
      const [first, ...tail] = config.VALID_COMMUNITY_CREATIONS
      throw Boom.badRequest(`can only add ${first.join(', ')}, or ${tail}.`)
    }

    await CommunitiesCreations.create({
      community_id: community.get('id'),
      creation_id: creationId,
    })

    ctx.status = 204
  } catch (err) {
    if (err instanceof Creations.NotFoundError) {
      throw Boom.notFound('creation not found.')
    }
    // eslint-disable-next-line max-len
    if (err.constraint === 'communities_creations_community_id_creation_id_unique') {
      throw Boom.badRequest('creation already added.')
    }

    throw err
  }
}
