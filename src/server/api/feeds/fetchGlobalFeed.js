import { normalize } from 'normalizr'
import { reduce } from 'lodash'

import {
  Feeds,
  Users,
  Communities,
  Fades,
  Fits,
  UsersProfiles,
} from '../../models'
import {
  FeedsColumns,
  FeedsSchema,
  UsersColumns,
  UsersSchema,
  CommunitiesColumns,
  CommunitiesSchema,
  FadesColumns,
  FadesSchema,
  FitsColumns,
  FitsSchema,
  UsersProfilesColumns,
  UsersProfilesSchema,
} from '../../../schemas'
import { validatePagination } from '../../../utils'


const objectBlacklist = {
  comments: true,
}

function divideIntoEntityGroups(feeds) {
  return feeds.toJSON().reduce((acc, prev) => {
    if (acc[prev.actor.type]) {
      acc[prev.actor.type].push(prev.actor.id)
    } else {
      acc[prev.actor.type] = [prev.actor.id]
    }

    if (!objectBlacklist[prev.object.type]) {
      if (acc[prev.object.type]) {
        acc[prev.object.type].push(prev.object.id)
      } else {
        acc[prev.object.type] = [prev.object.id]
      }
    }

    if (prev.target) {
      if (prev.target && acc[prev.target.type]) {
        acc[prev.target.type].push(prev.target.id)
      } else {
        acc[prev.target.type] = [prev.target.id]
      }
    }

    return acc
  }, {})
}

function findEntitiesByIds({
  ids,
  model,
  columns,
  columnId,
  withRelated = [],
}) {
  return model
    .query('whereIn', columnId, ids)
    .fetchAll({
      columns,
      withRelated,
    })
}

export async function getFeedEntities(feeds) {
  const entityGroups = divideIntoEntityGroups(feeds)
  const entities = []
  const schemas = []

  if (entityGroups.users) {
    entities.push(findEntitiesByIds({
      ids: entityGroups.users,
      model: Users,
      columns: UsersColumns,
      columnId: 'id',
    }))
    schemas.push(UsersSchema)
  }
  if (entityGroups.fades) {
    entities.push(findEntitiesByIds({
      ids: entityGroups.fades,
      model: Fades,
      columns: FadesColumns,
      columnId: 'creation_id',
      withRelated: ['creations'],
    }))
    schemas.push(FadesSchema)
  }
  if (entityGroups.fits) {
    entities.push(findEntitiesByIds({
      ids: entityGroups.fits,
      model: Fits,
      columns: FitsColumns,
      columnId: 'creation_id',
      withRelated: ['creations'],
    }))
    schemas.push(FitsSchema)
  }
  if (entityGroups.communities) {
    entities.push(findEntitiesByIds({
      ids: entityGroups.communities,
      model: Communities,
      columns: CommunitiesColumns,
      columnId: 'creation_id',
      withRelated: ['creations'],
    }))
    schemas.push(CommunitiesSchema)
  }
  if (entityGroups.users_profiles) {
    entities.push(findEntitiesByIds({
      ids: entityGroups.users_profiles,
      model: UsersProfiles,
      columns: UsersProfilesColumns,
      columnId: 'creation_id',
      withRelated: ['creations'],
    }))
    schemas.push(UsersProfilesSchema)
  }

  const promises = await Promise.all(entities)

  return promises
    .map((p, index) => {
      const normalized = normalize(p.toJSON(), schemas[index].listOfEntities)
      if (normalized.entities.creations) {
        normalized.entities.creations = reduce(
          normalized.entities.creations,
          (acc, prev) => {
            prev.id = prev.creationId
            delete prev.creationId
            acc[prev.id] = prev

            return acc
          },
          {},
        )
      }

      return normalized
    })
    .reduce((acc, prev) => {
      if (prev.entities.creations) {
        if (acc.creations) {
          return {
            ...acc,
            creations: {
              ...acc.creations,
              ...prev.entities.creations,
            },
          }
        }

        return {
          ...acc,
          creations: prev.entities.creations,
        }
      }

      return {
        ...acc,
        ...prev.entities,
      }
    }, {})
}

/**
 * @apiDescription fetches a page of a creation's comments.
 * @api {GET} /api/feeds Fetch Global Feed
 * @apiName fetchGlobalFeed
 * @apiGroup feeds
 * @apiPermission public
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.entities   normalized data
 * @apiSuccess {Array}  data.results    array of result IDs
 * @apiSuccess {Object} data.pagination pagination data
 *
 * @apiError Error {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 */
export default async function fetchGlobalFeed(ctx) {
  try {
    const {
      pageSize,
      page,
      column,
      direction,
    } = await validatePagination(ctx.query)
    const feeds = await Feeds
      .forge()
      .orderBy(column, direction)
      .fetchPage({
        columns: FeedsColumns,
        pageSize,
        page,
      })
    const normalized = normalize(
      feeds.toJSON(),
      FeedsSchema.list,
    )
    const { pagination } = feeds
    const feedEntities = await getFeedEntities(feeds)

    // mix in object entities with feed entities
    normalized.entities = {
      ...normalized.entities,
      ...feedEntities,
    }

    ctx.status = 200
    ctx.body = {
      data: {
        ...normalized,
        pagination,
      },
    }
  } catch (err) {
    throw err
  }
}
