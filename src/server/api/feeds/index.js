import Router from 'koa-router'

import fetchGlobalFeed from './fetchGlobalFeed'


const router = new Router()


router.get('/', fetchGlobalFeed)
// router.get('/users/:userId', fetchUserFeed)


export default router
