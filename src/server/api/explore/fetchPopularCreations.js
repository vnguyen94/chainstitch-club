import { validatePagination } from '../../../utils'
import { Creations } from '../../models'


/**
 * @apiDescription fetches currently popular creations.
 * @api {GET} /api/{creations} Fetch Popular Creations
 * @apiName fetchPopularCreations
 * @apiGroup explore
 * @apiPermission all
 * @apiVersion 0.0.3
 *
 * @apiSuccess {Object} data
 * @apiSuccess {Object} data.entities   normalized data
 * @apiSuccess {Array}  data.results    array of result IDs
 * @apiSuccess {Object} data.pagination pagination data
 *
 * @apiError Error         {Object} 500 Server error
 *
 * @apiSuccessExample {JSON} Success-Response:
 *   HTTP/1.1 200 OK
 *   {
 *      "data": {
 *          "entities": {
 *              "pagination": {
 *                  "page": 1,
 *                  "pageSize": 25,
 *                  "rowCount": 3,
 *                  "pageCount": 1
 *              },
 *              "creations": {
 *                  "2": {
 *                      "id": 2,
 *                      "userId": 1,
 *                      "entityId": 3,
 *                      "type": "fits",
 *                      "createdAt": "2017-09-10T23:14:33.128Z",
 *                      "updatedAt": "2017-09-10T23:14:33.128Z"
 *                  },
 *                  "8": {
 *                      "id": 8,
 *                      "userId": 1,
 *                      "entityId": 14,
 *                      "type": "fades",
 *                      "createdAt": "2017-09-14T04:40:40.050Z",
 *                      "updatedAt": "2017-09-14T04:40:40.050Z"
 *                  }
 *              },
 *              "fades": {
 *                  "8": {
 *                      "id": 1,
 *                      "creationId": 8,
 *                      "title": "test",
 *                      "description": "testes",
 *                      "imageUrl": "/uploads/default-creation.png"
 *                  }
 *              },
 *              "fits": {
 *                  "2": {
 *                      "id": 1,
 *                      "creationId": 2,
 *                      "title": "new fit",
 *                      "description": "desc",
 *                      "imageUrl": "/uploads/default-creation.png"
 *                  }
 *              }
 *          },
 *          "result": [
 *              8,
 *              2
 *          ]
 *      }
 *  }
 */
export default async function fetchPopularCreations(ctx) {
  try {
    const creationType = ctx.query.sort
    const pagination = await validatePagination(ctx.query)
    pagination.direction = 'DESC'

    const normalized = await Creations.fetchMixedCreations({
      type: creationType,
      pagination,
    })

    ctx.status = 200
    ctx.body = {
      data: normalized,
    }
  } catch (err) {
    throw err
  }
}
