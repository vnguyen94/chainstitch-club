import Router from 'koa-router'

import fetchFeaturedCreations from './fetchFeaturedCreations'
import fetchNewCreations from './fetchNewCreations'
import fetchPopularCreations from './fetchPopularCreations'

const router = new Router()


router.get('/featured', fetchFeaturedCreations)
router.get('/popular', fetchPopularCreations)
router.get('/new', fetchNewCreations)


export default router
