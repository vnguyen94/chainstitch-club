import Router from 'koa-router'
import Joi from 'joi'

import { FadesColumns, FadesSchema } from '../../../schemas'
import {
  fetchCommunities,
  fetchImages,
  fetchPage,
  uploadCreation,
  uploadImage,
  fetchOne,
  like,
  fetchComments,
  fetchUserLikes,
  postComment,
} from '../_creations'
import { Fades } from '../../models'
import authenticate from '../authentication'

const router = new Router()

const validationSchema = Joi.object().keys({
  title: Joi.string().required(),
  description: Joi.string(),
})


router.get('/', fetchPage(FadesColumns, FadesSchema))
router.post(
  '/',
  authenticate,
  uploadCreation(FadesColumns, FadesSchema, Fades, validationSchema),
)
router.get('/:creationId', fetchOne(FadesColumns, FadesSchema))
router.get('/:creationId/comments', fetchComments)
router.post('/:creationId/comments', authenticate, postComment(FadesSchema))
router.get('/:creationId/communities', fetchCommunities)
router.get('/:creationId/images', fetchImages(FadesSchema))
router.post('/:creationId/images', authenticate, uploadImage)
router.get('/:creationId/likes', fetchUserLikes(FadesSchema))
router.post('/:creationId/likes', authenticate, like(Fades, FadesSchema))
router.delete('/:creationId/likes', authenticate, like(Fades, FadesSchema))


export default router
