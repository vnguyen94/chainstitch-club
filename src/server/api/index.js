import Router from 'koa-router'
import Boom from 'boom'

import communities from './communities'
import users from './users'
import auth from './auth'
import explore from './explore'
import fades from './fades'
import feeds from './feeds'
import fits from './fits'


export default function attachApiRoutes(app) {
  const api = new Router({ prefix: '/api' })

  api.use('/communities', communities.routes())
  api.use('/users', users.routes())
  api.use('/auth', auth.routes())
  api.use('/explore', explore.routes())
  api.use('/fades', fades.routes())
  api.use('/feeds', feeds.routes())
  api.use('/fits', fits.routes())

  // if no match, then request hits bad api endpoint
  api.all('*', (ctx) => {
    const endpoint = `${ctx.method} ${ctx.url}`

    throw Boom.notImplemented(
      `The requested API endpoint (${endpoint}) has not been defined. ` +
      'Please verify the endpoint and verb.',
    )
  })

  app.use(api.routes())
}
