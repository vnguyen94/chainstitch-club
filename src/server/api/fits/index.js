import Router from 'koa-router'
import Joi from 'joi'

import { FitsColumns, FitsSchema } from '../../../schemas'
import {
  fetchCommunities,
  fetchImages,
  fetchPage,
  uploadCreation,
  uploadImage,
  fetchOne,
  like,
  fetchComments,
  fetchUserLikes,
  postComment,
} from '../_creations'
import { Fits } from '../../models'
import authenticate from '../authentication'

const router = new Router()

const validationSchema = Joi.object().keys({
  title: Joi.string().required(),
  description: Joi.string(),
})


router.get('/', fetchPage(FitsColumns, FitsSchema))
router.post(
  '/',
  authenticate,
  uploadCreation(FitsColumns, FitsSchema, Fits, validationSchema),
)
router.get('/:creationId', fetchOne(FitsColumns, FitsSchema))
router.get('/:creationId/comments', fetchComments)
router.post('/:creationId/comments', authenticate, postComment(FitsSchema))
router.get('/:creationId/communities', fetchCommunities)
router.get('/:creationId/images', fetchImages(FitsSchema))
router.post('/:creationId/images', authenticate, uploadImage)
router.get('/:creationId/likes', fetchUserLikes(FitsSchema))
router.post('/:creationId/likes', authenticate, like(Fits, FitsSchema))
router.delete('/:creationId/likes', authenticate, like(Fits, FitsSchema))


export default router
