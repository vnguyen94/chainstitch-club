import bodyParser from 'koa-body'
import favicon from 'koa-favicon'
import helmet from 'koa-helmet'
import jwt from 'koa-jwt'
import qs from 'qs'

import { paths } from '../../../config'

import attachDevMiddlewares from './middleware.dev'
import attachProdMiddlewares from './middleware.prod'
import attachAuthMiddleware from './authentication'
import { errorHandler, errorMonitoring } from './error'


const formidableConfig = {
  onFileBegin(name, file) {
    const extension = file.type
      .replace('/', '')
      .replace('image', '')

    file.path += `.${extension}`
  },
}

export default function attachMiddlewares(app) {
  app.on('error', errorMonitoring)

  app.use(errorHandler)
  app.use(favicon(paths('favicon')))
  app.use(bodyParser({
    multipart: true,
    formLimit: '5mb',
    querystring: qs,
    formidable: formidableConfig,
  }))
  app.use(helmet())
  app.use(jwt({
    secret: process.env.JWT_PRIVATE_KEY,
    passthrough: true,
  }))
  attachAuthMiddleware(app)

  if (__DEV__) {
    attachDevMiddlewares(app)
  } else {
    attachProdMiddlewares(app)
  }
}
