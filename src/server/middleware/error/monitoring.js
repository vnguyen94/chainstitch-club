import config from '../../../../config'


export default function errorMonitoring(err) {
  if (__PROD__ || config.ERROR_MONITORING_ENABLED) {
    // eslint-disable-next-line no-console
    console.log('sending to logging service: %s', err)
  } else {
    // eslint-disable-next-line no-console
    console.error(err)
  }
}
