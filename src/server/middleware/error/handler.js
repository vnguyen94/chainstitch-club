import Boom from 'boom'


export default async function errorHandler(ctx, next) {
  try {
    await next()
  } catch (err) {
    // ensure any non-Boom errors are wrapped
    const wrapped = Boom.boomify(err)
    const {
      output: { statusCode, payload },
    } = wrapped

    ctx.status = statusCode
    ctx.body = payload
    ctx.app.emit('error', wrapped, ctx)
  }
}
