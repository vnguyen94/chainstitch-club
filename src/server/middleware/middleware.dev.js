import proxy from 'koa-proxy'
import convert from 'koa-convert'
import logger from 'koa-logger'

import config from '../../../config'

const {
  SERVER_HOST,
  SERVER_PORT,
  ENABLE_DEV_USER,
  DEV_USER,
} = config


export default function attachDevMiddlewares(app) {
  app.use(async (ctx, next) => {
    if (ENABLE_DEV_USER) {
      ctx.state.user = DEV_USER
    }

    await next()
  })

  app.use(logger())

  app.use(convert(proxy({
    host: `http://${SERVER_HOST}:${SERVER_PORT}`,
    match: /^\/build\//,
  })))
}
