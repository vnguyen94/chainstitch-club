import Joi from 'joi'
import passport from 'koa-passport'
import { Strategy as LocalStrategy } from 'passport-local'

import { validatePayload } from '../../utils'
import { Users } from '../models'

const validationSchema = Joi.object().keys({
  emailOrUsername: Joi.string().required(),
  password: Joi.string().required(),
})

const localStrategy = new LocalStrategy({
  usernameField: 'emailOrUsername',
  passwordField: 'password',
}, async (emailOrUsername, password, callback) => {
  try {
    await validatePayload({ emailOrUsername, password }, validationSchema)
    const user = await Users.login(emailOrUsername, password)
    callback(null, user)
  } catch (err) {
    callback(err)
  }
})

function serializeUser(user, callback) {
  const serialized = { id: user.id }

  callback(null, serialized)
}

async function deserializeUser(serialized, callback) {
  try {
    const user = await Users.findOne({ id: serialized.id })

    callback(null, user)
  } catch (err) {
    if (err.isBoom && err.error === 'Bad Request') {
      callback(null, false, { message: err.message })
      return
    }

    callback(err)
  }
}


export default function attachAuthenticationMiddleware(app) {
  passport.serializeUser(serializeUser)
  passport.deserializeUser(deserializeUser)

  passport.use(localStrategy)

  app.use(passport.initialize())
}
