import Koa from 'koa'

import config from '../../config'

import attachMiddleware from './middleware'
import attachApiRoutes from './api'

const app = new Koa()

/* eslint-disable no-underscore-dangle */
global.__DEV__ = config.__DEV__
global.__PROD__ = config.__PROD__
/* eslint-enable no-underscore-dangle */

attachMiddleware(app)
attachApiRoutes(app)


export default app
