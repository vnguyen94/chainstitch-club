import Joi from 'joi'


const activity = Joi.object().keys({
  actor: Joi.object().keys({
    id: Joi.number().integer(),
    type: Joi.string(),
  }).required(),
  verb: Joi.string().required(),
  object: Joi.object().keys({
    id: Joi.number().integer(),
    type: Joi.string(),
    meta: Joi.object().optional(),
  }).required(),
  target: Joi.object().keys({
    id: Joi.number().integer(),
    type: Joi.string(),
  }),
  isPublic: Joi.boolean().required(),
})

const follow = Joi.object().keys({
  followerId: Joi.number().integer(),
  followeeId: Joi.number().integer(),
})


export default {
  activity,
  follow,
}
