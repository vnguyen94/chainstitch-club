import { validatePayload } from '../../../utils'
import {
  CommunitiesSchema,
  UsersSchema,
  UsersProfilesSchema,
  CommentsSchema,
  FadesSchema,
  FitsSchema,
} from '../../../schemas'
import { Feeds, UsersFeeds } from '../../models'

import schema from './schema'

// const config = {
//   decay: 0.3, // The score that a value at scale distance from the origin should receive.
//   scale: 5, // Determines how quickly the score drops from 1.0 to the decay value
//   offset: 5, // Values below the offset will start to receive a lower score.
// }


const feed = {
  models: {
    users: (id) => ({
      id,
      type: UsersSchema.ENTITY_NAME,
    }),
    usersProfiles: (id) => ({
      id,
      type: UsersProfilesSchema.ENTITY_NAME,
    }),
    comments: (creation) => ({
      id: creation.get('id'),
      type: CommentsSchema.ENTITY_NAME,
      meta: {
        body: creation.get('body'),
      },
    }),
    communities: (creation) => ({
      id: creation.get('id'),
      type: CommunitiesSchema.ENTITY_NAME,
    }),
    fades: (creation) => ({
      id: creation.get('id'),
      type: FadesSchema.ENTITY_NAME,
    }),
    fits: (creation) => ({
      id: creation.get('id'),
      type: FitsSchema.ENTITY_NAME,
    }),
  },

  get(unparsedOptions = {}) {
    const defaults = {
      limit: 25,
      ranking: 'desc',
      filters: [], // only get likes, follows, creation, etc.
    }
    // eslint-disable-next-line
    const options = { ...defaults, ...unparsedOptions }
  },

  async follow(followerId, followeeId) {
    try {
      const payload = await validatePayload(
        schema.follow,
        { followerId, followeeId },
      )

      return UsersFeeds.create(payload)
    } catch (err) {
      throw err
    }
  },

  async unfollow(followerId, followeeId) {
    try {
      const payload = await validatePayload(
        schema.follow,
        { followerId, followeeId },
      )

      return UsersFeeds
        .forge(payload)
        .destroy({ require: true })
    } catch (err) {
      throw err
    }
  },

  async addActivity(unparsedActivity, unparsedOptions = {}, joiOptions = {}) {
    const defaults = {
      isPublic: false,
    }
    const options = { ...defaults, ...unparsedOptions }

    try {
      const payload = await validatePayload(
        { ...unparsedActivity, ...options },
        schema.activity,
        joiOptions,
      )
      const feedRow = await Feeds.create(payload)

      return feedRow
    } catch (err) {
      throw err
    }
  },

  async updateActivity(activityId, data) {
    return Feeds.update(data, { id: activityId })
  },

  async removeActivity(activityId) {
    return this.updateActivity(activityId, { is_deleted: 0 })
  },
}


export default feed
