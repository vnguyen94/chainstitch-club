import { mjml2html } from 'mjml'


export default function renderMmail(template) {
  try {
    const { errors, html } = mjml2html(template)

    if (errors.length) {
      throw new Error('email template errors', errors)
    }

    return html
  } catch (err) {
    throw err
  }
}
