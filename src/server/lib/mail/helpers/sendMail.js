require('dotenv').config() /* eslint-disable import/first */

import sendgrid from '@sendgrid/mail'
import Joi from 'joi'

import { validatePayload } from '../../../../utils'

const sg = sendgrid.setApiKey(process.env.SENDGRID_API_KEY)
const validationSchema = Joi.object().keys({
  to: Joi.string().email().required(),
  from: Joi.string().email().required(),
  subject: Joi.string().required(),
  text: Joi.string().required(),
  html: Joi.string().required(),
  categories: Joi.array().items(Joi.string()),
})


/* eslint-disable new-cap */
export default async function sendMail(emailData) {
  try {
    const payload = await validatePayload(emailData, validationSchema)

    await sg.send(payload)
  } catch (err) {
    throw err
  }
}
/* eslint-enable new-cap */
