import renderMail from './renderMail'
import sendMail from './sendMail'


export {
  renderMail,
  sendMail,
}
