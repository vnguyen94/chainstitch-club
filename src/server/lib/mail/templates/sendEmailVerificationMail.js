import {
  renderMail,
  sendMail,
} from '../helpers'


const styleWrap = [
  'overflow-wrap: break-word',
  'word-wrap: break-word',
  'word-break: break-word',
].join(';')

const createHTMLTemplate = ({ host, token }) => {
  const verifyUrl = `${host}/verify?token=${token}`

  return `
    <mjml>
      <mj-body>
        <mj-container background-color="#f8f8f8">

          <mj-section padding-bottom="20px">
            <mj-column width="100%">
              <mj-text
                align="center"
                font-size="20"
                color="grey"
                font-family="Helvetica Neue"
                font-weight="200"
              >
                chainstitch-club
              </mj-text>
            </mj-column>
          </mj-section>

          <mj-section
            background-color="#fcfcfc"
            padding-top="20px"
            border="1px solid #d8d8d8"
          >
            <mj-column>
              <mj-text align="middle" font-size="20" color="#555">
                almost there, just confirm your email
              </mj-text>
              <mj-button background-color="#88bbdd" href="${verifyUrl}">
                verify
              </mj-button>
              <mj-text align="middle" color="#555">
                or use this link
              </mj-text>
              <mj-text align="middle" color="#555">
                <a style="${styleWrap}" href="${verifyUrl}">${verifyUrl}</a>
              </mj-text>
            </mj-column>
          </mj-section>

          <mj-section background-color="#f8f8f8">
            <mj-column>
              <mj-text align="center" color="#555">
                contact: van@chainstitch.club
              </mj-text>
            </mj-column>
          </mj-section>
        </mj-container>
      </mj-body>
    </mjml>
  `
}

const createTextTemplate = ({ host, token }) => {
  const verifyUrl = `${host}/verify?token=${token}`

  return (
    'almost there, just confirm your email ' +
    `with the following link: ${verifyUrl}`
  )
}


export default function sendEmailVerificationMail(to, token) {
  const params = {
    host: 'localhost:3000',
    token,
  }
  const html = renderMail(createHTMLTemplate(params))
  const text = createTextTemplate(params)

  sendMail({
    from: 'noreply@chainstitch.club',
    subject: 'email verification',
    categories: ['security'],
    html,
    text,
    to,
  })
}
