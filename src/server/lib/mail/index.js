import {
  sendEmailVerificationMail,
  changeEmailAndPassword,
} from './templates'


export default {
  sendEmailVerificationMail,
  changeEmailAndPassword,
}
