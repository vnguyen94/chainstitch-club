require('dotenv').config() /* eslint-disable import/first */

import crypto from 'crypto'

import uuidv4 from 'uuid/v4'

import config from '../../../config'


export function generateUUID() {
  return uuidv4()
}

export function encryptText(text) {
  const cipher = crypto.createCipher(
    config.ENCRYPTION_ALGORITHM,
    process.env.ENCRYPTION_PASSWORD,
  )
  let crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex')

  return crypted
}

export function decryptText(text) {
  const decipher = crypto.createCipher(
    config.ENCRYPTION_ALGORITHM,
    process.env.ENCRYPTION_PASSWORD,
  )
  let dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8')

  return dec
}
