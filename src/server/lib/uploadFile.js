import fs from 'fs'
import { promisify } from 'util'

import gm from 'gm'

import { paths } from '../../../config'

const renameAsync = promisify(fs.rename)

function getImageDimensions(imagePath) {
  return new Promise((resolve, reject) => {
    gm(imagePath).size((err, size) => {
      if (err) {
        reject(err)
        return
      }

      resolve(size)
    })
  })
}

async function saveFileLocally(file) {
  try {
    const currentTime = (new Date()).getTime()
    const processedContentType = file.type
      .replace('/', '')
      .replace('image', '')
    const filename = `file_${currentTime}.${processedContentType}`
    const imagePath = `${paths('uploads')}/${filename}`

    await renameAsync(file.path, imagePath)

    return `/uploads/${filename}`
  } catch (err) {
    throw err
  }
}

// fill in later
async function uploadFileToS3(file) {
  return file
}

export async function uploadFile(file) {
  try {
    const imagePath = __DEV__
      ? await saveFileLocally(file)
      : await uploadFileToS3(file)

    return imagePath
  } catch (err) {
    throw err
  }
}

export async function uploadImage(image) {
  try {
    const { height, width } = await getImageDimensions(image.path)
    const path = await uploadFile(image)

    return { height, width, path }
  } catch (err) {
    throw err
  }
}
