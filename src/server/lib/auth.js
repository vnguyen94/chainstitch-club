import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

import config from '../../../config'


/**
 * validates password.
 * @param  {String} password plaintext password
 * @param  {String} hash password from database
 * @return {Promise}
 */
export async function validatePassword(password, hash) {
  try {
    return await bcrypt.compare(password, hash)
  } catch (err) {
    throw err
  }
}

/**
 * generate a random salt.
 * @return {Promise}
 */
export async function generateSalt() {
  try {
    return await bcrypt.genSalt(config.BCRYPT_ROUNDS)
  } catch (err) {
    throw err
  }
}

/**
 * hash a password using plaintext password and salt.
 * @param  {String} password
 * @param  {String} salt
 * @return {Promise}
 */
export async function hashPassword(password, salt) {
  try {
    return await bcrypt.hash(password, salt)
  } catch (err) {
    throw err
  }
}

export async function createPasswordHash(password) {
  try {
    const salt = await generateSalt()
    const hash = await hashPassword(password, salt)

    return hash
  } catch (err) {
    throw err
  }
}

/**
 * create a webtoken mixing in payload to sign it.
 * @param  {Object} jwtPayload
 * @return {String}
 */
export function createWebToken(jwtPayload) {
  return jwt.sign(jwtPayload, process.env.JWT_PRIVATE_KEY, config.JWT_CONFIG)
}
