import db from '../../db'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('creation_id').unsigned().notNullable()
  table.integer('image_id').unsigned().notNullable()

  table.index('id')
  table.unique(['creation_id', 'image_id'])
  table.foreign('creation_id').references('creations.id')
  table.foreign('image_id').references('images.id')
}

const CreationsImages = db.Model.extend({
  tableName: 'creations_images',
  hasTimestamps: false,
}, {
})


export default CreationsImages
