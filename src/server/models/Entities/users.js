import { Users } from '../'


/**
 * get an entity's user table.
 * @return {Promise}
 */
function users() {
  return this.hasOne(Users)
}

export default users
