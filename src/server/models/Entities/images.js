import { Images } from '../'


/**
 * get a entity's images.
 * @return {Promise}
 */
function images() {
  return this.hasMany(Images)
}

export default images
