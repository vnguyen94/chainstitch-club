import { Creations } from '../'


/**
 * get an entity's creation table.
 * @return {Promise}
 */
function creations() {
  return this.hasOne(Creations)
}

export default creations
