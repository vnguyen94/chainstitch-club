import db from '../../db'

import creations from './creations'
import images from './images'
import users from './users'


export function schema(table) {
  table.increments('id').unsigned().primary()

  table.index('id')
}

const Entities = db.Model.extend({
  tableName: 'entities',
  hasTimestamps: false,

  creations,
  images,
  users,
}, {
})


export default Entities
