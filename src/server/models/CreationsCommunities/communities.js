import { Communities } from '../'


/**
 * get the creation's communities.
 * @return {Promise}
 */
function communities() {
  return this.hasMany(Communities, 'id')
}

export default communities
