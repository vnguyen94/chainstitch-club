import db from '../../db'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('entity_id').unsigned().notNullable()
  table.integer('height').unsigned().notNullable()
  table.integer('width').unsigned().notNullable()
  table.string('url', 50).notNullable()
  table.float('order').unsigned().notNullable()

  table.index('id')
  table.timestamps(true, true)
  table.foreign('entity_id').references('entities.id')
}

const Images = db.Model.extend({
  tableName: 'images',
  hasTimestamps: true,
}, {
})


export default Images
