import db from '../../db'

import communities from './communities'
import fades from './fades'
import fits from './fits'
import users from './users'
import entities from './entities'
import usersProfiles from './usersProfiles'
import fetchMixedCreations from './fetchMixedCreations'
import fetchUserTopCreations from './fetchUserTopCreations'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('entity_id').unsigned().notNullable()
  table.integer('user_id').unsigned().notNullable()
  table.enum('type', [
    'communities',
    'comments',
    'fades',
    'fits',
    'users_profiles',
  ]).notNullable()
  table.boolean('is_deleted').notNullable().defaultTo(false)
  table.boolean('is_private').notNullable().defaultTo(false)
  table.boolean('is_featured').notNullable().defaultTo(false)

  table.index('id')
  table.timestamps(true, true)
  table.foreign('entity_id').references('entities.id')
  table.foreign('user_id').references('users.id')
}

const Creations = db.Model.extend({
  tableName: 'creations',
  hasTimestamps: true,

  communities,
  fades,
  fits,
  users,
  entities,
  usersProfiles,
}, {
  fetchMixedCreations,
  fetchUserTopCreations,
})


export default Creations
