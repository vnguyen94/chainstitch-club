import { Communities } from '../'


/**
 * get a creation's community.
 * @return {Promise}
 */
function communities() {
  return this.hasOne(Communities)
}

export default communities
