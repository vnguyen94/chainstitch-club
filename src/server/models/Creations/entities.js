import { Entities } from '../'


/**
 * get a creation's entity.
 * @return {Promise}
 */
function entities() {
  return this.belongsTo(Entities)
}

export default entities
