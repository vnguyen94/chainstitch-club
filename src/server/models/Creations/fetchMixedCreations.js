import { normalize } from 'normalizr'
import { map, groupBy } from 'lodash'

import {
  Fades,
  Fits,
  UsersProfiles,
} from '../../models'
import {
  CreationsColumns,
  CreationsSchema,
  FadesColumns,
  FadesSchema,
  FitsColumns,
  FitsSchema,
  UsersProfilesColumns,
  UsersProfilesSchema,
  UsersColumns,
} from '../../../schemas'


function fetchCreations(whereParams, type, pagination) {
  const creationTypes = type
    ? [type]
    : ['fits', 'fades']

  return this
    .query('whereIn', 'type', creationTypes)
    .where(whereParams)
    .orderBy(pagination.column, pagination.direction)
    .fetchPage({
      columns: CreationsColumns,
      withRelated: {
        users: (qb) => {
          qb.columns(UsersColumns)
        },
      },
      pageSize: pagination.pageSize,
      page: pagination.page,
    })
}

async function fetchEntitiesForCreations(creations) {
  const entityGroups = groupBy(creations.toJSON(), 'type')
  const entities = []
  const schemas = []

  if (entityGroups.fades) {
    const fadePromise = Fades
      .query('whereIn', 'creation_id', map(entityGroups.fades, 'id'))
      .fetchAll({ columns: FadesColumns })
    entities.push(fadePromise)
    schemas.push(FadesSchema)
  }
  if (entityGroups.fits) {
    const fitPromise = Fits
      .query('whereIn', 'creation_id', map(entityGroups.fits, 'id'))
      .fetchAll({ columns: FitsColumns })
    entities.push(fitPromise)
    schemas.push(FitsSchema)
  }
  if (entityGroups.users_profiles) {
    const usersProfilesPromise = UsersProfiles
      .query('whereIn', 'creation_id', map(entityGroups.users_profiles, 'id'))
      .fetchAll({ columns: UsersProfilesColumns })
    entities.push(usersProfilesPromise)
    schemas.push(UsersProfilesSchema)
  }

  const promises = await Promise.all(entities)

  return promises
    .map((p, index) => (
      normalize(
        p.toJSON(),
        schemas[index].listOfEntities,
      )
    ))
    .reduce((acc, prev) => ({
      ...acc,
      ...prev.entities,
    }), {})
}

const initialWhereParams = { is_deleted: 0 }

/**
 * finds user's top creations.
 * @return {Promise}
 */
export default async function fetchMixedCreations({
  query = initialWhereParams,
  type,
  pagination,
}) {
  const creations = await fetchCreations.call(this, query, type, pagination)
  const feedEntities = await fetchEntitiesForCreations(creations)
  const normalized = normalize(creations.toJSON(), CreationsSchema.listWithUser)

  return {
    result: normalized.result,
    pagination: creations.pagination,
    entities: {
      ...normalized.entities,
      ...feedEntities,
    },
  }
}
