import { Fades } from '../'


/**
 * get all of a creation's fade table.
 * @return {Promise}
 */
function fades() {
  return this.hasOne(Fades)
}

export default fades
