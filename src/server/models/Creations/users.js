import { Users } from '../'


/**
 * get a creation's user.
 * @return {Promise}
 */
function users() {
  return this.belongsTo(Users)
}

export default users
