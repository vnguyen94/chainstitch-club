import { Fits } from '../'


/**
 * get all of a creation's fit table.
 * @return {Promise}
 */
function fits() {
  return this.hasOne(Fits)
}

export default fits
