import { UsersProfiles } from '../'


/**
 * get all of a creation's users_profiles table.
 * @return {Promise}
 */
function usersProfiles() {
  return this.hasOne(UsersProfiles)
}

export default usersProfiles
