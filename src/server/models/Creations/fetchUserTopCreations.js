import { normalize } from 'normalizr'

import {
  CreationsColumns,
  FadesSchema,
  FitsSchema,
} from '../../../schemas'


function fetchCreations(userId, creationType) {
  return this
    .where({ user_id: userId, is_deleted: 0, type: creationType })
    .orderBy('id', 'DESC')
    .fetchPage({
      columns: CreationsColumns,
      withRelated: {
        [creationType]: (qb) => {
          qb.columns([
            'id',
            'creation_id',
            'title',
            'image_url',
          ])
        },
        users: (qb) => {
          qb.columns(['id', 'username'])
        },
      },
      pageSize: 3,
      page: 1,
    })
}

/**
 * finds user's top creations.
 * @return {Promise}
 */
export default async function fetchUserTopCreations(userId) {
  let [fades, fits] = await Promise.all([
    fetchCreations.call(this, userId, FadesSchema.ENTITY_NAME),
    fetchCreations.call(this, userId, FitsSchema.ENTITY_NAME),
  ])

  fades = normalize(fades.toJSON(), FadesSchema.list)
  fits = normalize(fits.toJSON(), FitsSchema.list)

  return { fades, fits }
}
