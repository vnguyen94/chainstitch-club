import { schema as Comments } from './Comments'
import { schema as Communities } from './Communities'
import { schema as CommunitiesCreations } from './CommunitiesCreations'
import { schema as Creations } from './Creations'
import { schema as CreationsCommunities } from './CreationsCommunities'
import { schema as CreationsDates } from './CreationsDates'
import { schema as CreationsImages } from './CreationsImages'
import { schema as Entities } from './Entities'
import { schema as Fades } from './Fades'
import { schema as Feeds } from './Feeds'
import { schema as Fits } from './Fits'
import { schema as Images } from './Images'
import { schema as Users } from './Users'
import {
  schema as UsersEmailVerificationTokens,
} from './UsersEmailVerificationTokens'
import { schema as UsersImages } from './UsersImages'
import { schema as UsersFeeds } from './UsersFeeds'
import { schema as UsersProfiles } from './UsersProfiles'
import { schema as UsersNotifications } from './UsersNotifications'
import { schema as EntitiesLikes } from './EntitiesLikes'
import { schema as EntitiesFollows } from './EntitiesFollows'


export default {
  Comments,
  Communities,
  CommunitiesCreations,
  Creations,
  CreationsCommunities,
  CreationsDates,
  CreationsImages,
  Entities,
  EntitiesLikes,
  EntitiesFollows,
  Fades,
  Feeds,
  Fits,
  Images,
  Users,
  UsersEmailVerificationTokens,
  UsersFeeds,
  UsersImages,
  UsersProfiles,
  UsersNotifications,
}
