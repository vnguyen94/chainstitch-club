import db from '../../db'

import creations from './creations'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('user_id').unsigned().notNullable()
  table.boolean('messages').notNullable().defaultTo(true)
  table.boolean('followers').notNullable().defaultTo(true)
  table.boolean('likers').notNullable().defaultTo(true)
  table.boolean('feed').notNullable().defaultTo(true)

  table.index('id')
  table.foreign('user_id').references('users.id')
}

const UsersNotifications = db.Model.extend({
  tableName: 'users_notifications',
  idAttribute: 'user_id',
  hasTimestamps: false,

  creations,
}, {
})


export default UsersNotifications
