import { Creations } from '../'


/**
 * get a user notification's creation.
 * @return {Promise}
 */
function creations() {
  return this.belongsTo(Creations)
}

export default creations
