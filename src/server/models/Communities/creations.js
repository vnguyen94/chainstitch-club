import { Creations } from '../'


/**
 * get an community's creation.
 * @return {Promise}
 */
function creations() {
  return this.belongsTo(Creations)
}

export default creations
