import config from '../../../../config'
import db from '../../db'

import creations from './creations'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('creation_id').unsigned().notNullable()
  table.string('title', 30).notNullable()
  table.enum('type', config.VALID_COMMUNITY_TYPES_SINGULAR).notNullable()
  table.string('image_url', 80).notNullable()

  table.string('description', 500)

  table.index('id')
  table.foreign('creation_id').references('creations.id')
}

const Communities = db.Model.extend({
  tableName: 'communities',
  hasTimestamps: false,

  creations,
}, {
})


export default Communities
