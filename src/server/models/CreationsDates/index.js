import db from '../../db'


export async function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('creation_id').unsigned().notNullable()
  table.integer('user_id').unsigned().notNullable()
  table.timestamp('worn_at').notNullable()

  table.index('id')
  table.timestamps(true, true)
  await db.knex.schema.raw(`
    CREATE UNIQUE INDEX creations_dates_unique
    ON creations_dates(
      creation_id,
      user_id,
      date_trunc('day', worn_at AT TIME ZONE 'GMT')
    )
  `)
  table.foreign('creation_id').references('creations.id')
  table.foreign('user_id').references('users.id')
}

const CreationsDates = db.Model.extend({
  tableName: 'creations_dates',
  hasTimestamps: true,
}, {
})


export default CreationsDates
