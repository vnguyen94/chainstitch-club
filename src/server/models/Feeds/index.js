import db from '../../db'


export function schema(table) {
  table.increments('id').unsigned().primary()
  table.jsonb('actor').notNullable()
  table.string('verb', 30).notNullable()
  table.jsonb('object').notNullable()
  table.boolean('is_deleted').notNullable().defaultTo(false)
  table.boolean('is_public').notNullable().defaultTo(false)

  table.jsonb('target')

  table.index('id')
  table.timestamps(true, true)
}

const Feeds = db.Model.extend({
  tableName: 'feeds',
  hasTimestamps: true,
}, {
})


export default Feeds
