import db from '../../db'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('user_id').unsigned().notNullable()
  table.integer('image_id').unsigned().notNullable()

  table.index('id')
  table.unique(['user_id', 'image_id'])
  table.foreign('user_id').references('users.id')
  table.foreign('image_id').references('images.id')
}

const UsersImages = db.Model.extend({
  tableName: 'users_images',
  hasTimestamps: false,
}, {
})


export default UsersImages
