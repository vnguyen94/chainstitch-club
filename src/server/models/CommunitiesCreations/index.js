import db from '../../db'

import creations from './creations'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('community_id').unsigned().notNullable()
  table.integer('creation_id').unsigned().notNullable()

  table.index('id')
  table.unique(['community_id', 'creation_id'])
  table.foreign('community_id').references('communities.id')
  table.foreign('creation_id').references('creations.id')
}

const CommunitiesCreations = db.Model.extend({
  tableName: 'communities_creations',
  hasTimestamps: false,

  creations,
}, {
})


export default CommunitiesCreations
