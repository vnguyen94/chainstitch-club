import { Creations } from '../'


/**
 * get the collection's creation.
 * @return {Promise}
 */
function creations() {
  return this.belongsTo(Creations)
}

export default creations
