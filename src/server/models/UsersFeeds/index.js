import db from '../../db'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('user_id').unsigned().notNullable()
  table.integer('feed_id').unsigned().notNullable()

  table.index('id')
  table.unique(['user_id', 'feed_id'])
  table.foreign('user_id').references('users.id')
  table.foreign('feed_id').references('feeds.id')
}

const UsersFeeds = db.Model.extend({
  tableName: 'users_feeds',
  hasTimestamps: false,
}, {
})


export default UsersFeeds
