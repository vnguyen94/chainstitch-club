import db from '../../db'

import creations from './creations'
import post from './post'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('creation_id').unsigned().notNullable()
  table.integer('recipient_entity_id').unsigned().notNullable()
  table.string('body', 200).notNullable()

  table.index('id')
  table.foreign('creation_id').references('creations.id')
  table.foreign('recipient_entity_id').references('entities.id')
}

const Comments = db.Model.extend({
  tableName: 'comments',
  hasTimestamps: false,

  creations,
}, {
  post,
})


export default Comments
