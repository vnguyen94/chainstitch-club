import { Creations } from '../'


/**
 * get a comment's creation.
 * @return {Promise}
 */
function creations() {
  return this.belongsTo(Creations)
}

export default creations
