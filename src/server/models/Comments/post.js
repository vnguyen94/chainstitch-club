import db from '../../db'
import Entities from '../Entities'
import Creations from '../Creations'


/**
 * posts a comment.
 * @param  {String} body
 * @return {Object}
 */
export default function post({ user, body, recipientEntityId }) {
  return db.transaction(async (transacting) => {
    try {
      const entity = await Entities.create({}, { transacting })
      const creation = await Creations.create(
        {
          entityId: entity.get('id'),
          userId: user.id,
          type: 'comments',
        },
        { transacting },
      )
      const comment = await this.create(
        {
          creationId: creation.get('id'),
          body,
          recipientEntityId,
        },
        { transacting },
      )

      return {
        bookshelf: { comment, creation },
        payload: {
          entities: {
            creations: {
              [creation.id]: {
                // save doesn't call `parse`
                // https://github.com/bookshelf/bookshelf/issues/1076#issuecomment-215370103
                ...creation.parse(creation.toJSON()),
                users: user.id,
              },
            },
            comments: {
              [comment.id]: {
                ...comment.toJSON(),
                creations: creation.id,
              },
            },
          },
          results: comment.id,
        },
      }
    } catch (err) {
      throw err
    }
  })
}
