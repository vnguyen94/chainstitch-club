import db from '../../db'

import entities from './entities'
import profiles from './profiles'
import usersNotifications from './usersNotifications'
import createUser from './createUser'
import findByEmail from './findByEmail'
import hasUserLiked from './hasUserLiked'
import isFollowing from './isFollowing'
import login from './login'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('entity_id').unsigned().notNullable()
  table.string('username', 20).unique().notNullable()
  table.string('email', 30).unique().notNullable()
  table.string('password', 72).notNullable()
  table.string('role', 10).notNullable().defaultTo('user')
  table.boolean('is_deleted').notNullable().defaultTo(false)
  table.boolean('is_verified').notNullable().defaultTo(false)

  table.string('image_url', 80)

  table.index('id')
  table.timestamps(true, true)
  table.foreign('entity_id').references('entities.id')
}

const Users = db.Model.extend({
  tableName: 'users',
  hasTimestamps: true,

  entities,
  profiles,
  usersNotifications,
}, {
  createUser,
  findByEmail,
  isFollowing,
  hasUserLiked,
  login,
})


export default Users
