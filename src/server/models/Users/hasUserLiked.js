import { EntitiesLikes } from '../'


/**
 * return whether a user has liked the compared user.
 * @return {Promise}
 */
function hasUserLiked(likerEntityId, likeeEntityId) {
  return EntitiesLikes.findOne({
    entity_1_id: likerEntityId,
    entity_2_id: likeeEntityId,
  }, { require: false })
    .then((user) => !!user)
}


export default hasUserLiked
