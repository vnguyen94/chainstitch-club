import { UsersNotifications } from '../'


/**
 * get all of a user's users_notifications table.
 * @return {Promise}
 */
function usersNotifications() {
  return this.hasOne(UsersNotifications)
}

export default usersNotifications
