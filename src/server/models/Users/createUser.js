import Boom from 'boom'
import { pick } from 'lodash'

import config from '../../../../config'
import db from '../../db'
import {
  createPasswordHash,
  createWebToken,
  mail,
} from '../../lib'
import Entities from '../Entities'
import UsersProfiles from '../UsersProfiles'
import UsersNotifications from '../UsersNotifications'
import UsersEmailVerificationTokens from '../UsersEmailVerificationTokens'


/**
 * transaction to insert user into database.
 * @param  {Object} userData
 * @return {Promise}
 */
export function createUserTransaction({
  email,
  password,
  username,
  ...userProfileData
}) {
  return db.transaction(async (transacting) => {
    try {
      const entity = await Entities.create({}, { transacting })
      const user = await this.create(
        {
          entityId: entity.get('id'),
          imageUrl: config.DEFAULT_AVATAR,
          email,
          password,
          username,
        },
        { transacting },
      )
      const userId = user.get('id')

      await Promise.all([
        UsersProfiles.createWithEntity(
          { userId, ...userProfileData },
          { transacting },
        ),
        UsersNotifications.create(
          { userId },
          { transacting },
        ),
      ])

      return user.attributes
    } catch (err) {
      if (err.constraint === 'users_username_unique') {
        throw Boom.badRequest('username already taken.')
      }
      if (err.constraint === 'users_email_unique') {
        throw Boom.badRequest('email already taken.')
      }

      throw err
    }
  })
}

/**
 * creates a user and returns a JWT.
 * @param  {Object} userData
 * @return {Promise}
 */
export default async function createUser(userData) {
  try {
    const userIfExists = await this.findByEmail(userData.email)

    if (userIfExists) {
      throw Boom.badRequest('email already in use.')
    }

    const hash = await createPasswordHash(userData.password)
    const userCreationData = {
      ...userData,
      password: hash,
    }
    const user = await createUserTransaction.call(this, userCreationData)
    const token = createWebToken(pick(user, ['id', 'password']))
    const userAttributesToReturn = pick(user, [
      'id',
      'firstName',
      'lastName',
      'email',
      'role',
    ])

    const emailToken = await UsersEmailVerificationTokens.generate(user.id)
    mail.sendEmailVerificationMail(userData.email, emailToken)

    return {
      user: userAttributesToReturn,
      token,
    }
  } catch (err) {
    throw err
  }
}
