import { Entities } from '../'


/**
 * get a user's entity.
 * @return {Promise}
 */
function entities() {
  return this.belongsTo(Entities)
}

export default entities
