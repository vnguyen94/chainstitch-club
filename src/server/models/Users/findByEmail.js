/**
 * finds a user by email.
 * @param  {String} email
 * @return {Promise}
 */
export default function findByEmail(email, { require } = { require: false }) {
  return this.findOne({ email, isDeleted: 0 }, { require })
}
