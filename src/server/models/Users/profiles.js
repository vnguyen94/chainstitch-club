import { UsersProfiles } from '../'


/**
 * get a user's profile.
 * @return {Promise}
 */
function profiles() {
  return this.hasOne(UsersProfiles)
}

export default profiles
