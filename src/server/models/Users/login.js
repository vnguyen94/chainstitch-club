import Boom from 'boom'

import { validatePassword } from '../../lib'


/**
 * logs in a user and passes back JWT and some user data.
 * @param  {String} email
 * @param  {String} password
 * @return {Object}
 */
export default async function login(email, password) {
  try {
    const user = await this.findByEmail(email, { require: true })
    const attr = user.attributes
    const passwordIsValid = await validatePassword(password, attr.password)

    if (!passwordIsValid) {
      throw Boom.badRequest('username or email was incorrect.')
    }

    return attr
  } catch (err) {
    if (err instanceof this.NotFoundError) {
      throw Boom.badRequest('username or email was incorrect.')
    }

    throw err
  }
}
