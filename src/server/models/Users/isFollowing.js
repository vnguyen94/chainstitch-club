import { EntitiesFollows } from '../'


/**
 * return whether a user is following the compared user.
 * @return {Promise}
 */
function isFollowing(followerEntityId, followeeEntityId) {
  return EntitiesFollows.findOne({
    entity_1_id: followerEntityId,
    entity_2_id: followeeEntityId,
  }, { require: false })
    .then((user) => !!user)
}


export default isFollowing
