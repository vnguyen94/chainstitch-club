import { Creations } from '../'


/**
 * get a user profile's creation.
 * @return {Promise}
 */
function creations() {
  return this.belongsTo(Creations)
}

export default creations
