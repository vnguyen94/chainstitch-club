import db from '../../db'

import creations from './creations'
import createWithEntity from './createWithEntity'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('creation_id').unsigned().notNullable()
  table.integer('user_id').unsigned().notNullable()

  table.string('first_name', 20)
  table.string('last_name', 20)
  table.enum('gender', ['male', 'female', 'other', 'undisclosed', null])
    .defaultTo(null)
  table.string('location', 40)
  table.string('description', 150)

  table.index('id')
  table.foreign('creation_id').references('creations.id')
  table.foreign('user_id').references('users.id')
}

const UsersProfiles = db.Model.extend({
  tableName: 'users_profiles',
  idAttribute: 'user_id',
  hasTimestamps: false,

  creations,
}, {
  createWithEntity,
})


export default UsersProfiles
