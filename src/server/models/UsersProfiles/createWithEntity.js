import Entities from '../Entities'
import Creations from '../Creations'


/**
 * transaction to create a user profile with attached entity.
 * @param  {Object}
 * @return {Promise}
 */
export default async function createWithEntity(
  { userId, ...userProfileData },
  { transacting },
) {
  try {
    const entity = await Entities.create({}, { transacting })
    const creation = await Creations.create(
      {
        entityId: entity.get('id'),
        type: 'users_profiles',
        userId,
      },
      { transacting },
    )
    const userProfile = await this.create(
      {
        creationId: creation.get('id'),
        userId,
        ...userProfileData,
      },
      { transacting },
    )

    return userProfile
  } catch (err) {
    throw err
  }
}
