import db from '../../db'

import creations from './creations'


export function schema(table) {
  table.increments('id').unsigned().primary().notNullable()
  table.integer('creation_id').unsigned().notNullable()
  table.string('title', 30).notNullable()
  table.string('image_url', 80).notNullable()

  table.string('description', 500)

  table.index('id')
  table.foreign('creation_id').references('creations.id')
}

const Fades = db.Model.extend({
  tableName: 'fades',
  hasTimestamps: false,

  creations,
}, {
})


export default Fades
