import { Creations } from '../'


/**
 * get a fade's creation.
 * @return {Promise}
 */
function creations() {
  return this.belongsTo(Creations)
}

export default creations
