import { decryptText } from '../../lib'


function parseToken(emailToken) {
  try {
    const parsedToken = JSON.parse(emailToken)
    const { userId, token } = parsedToken

    return { userId, token }
  } catch (err) {
    throw new Error('was not able to parse token.')
  }
}

/**
 * decrypts an email token and checks if valid.
 * @return {Promise}
 */
export default async function verify(emailToken) {
  try {
    const decryptedToken = decryptText(emailToken)
    const payload = parseToken(decryptedToken)

    await this.findOne(payload, { require: true })

    return true
  } catch (err) {
    return false
  }
}

