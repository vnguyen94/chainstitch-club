import db from '../../db'

import generate from './generate'
import verify from './verify'


export function schema(table) {
  table.integer('user_id').unsigned().notNullable()
  table.string('token', 40).notNullable()

  table.index('user_id')
  table.foreign('user_id').references('users.id')
}

const UsersEmailVerificationTokens = db.Model.extend({
  tableName: 'users_email_verification_tokens',
  idAttribute: 'user_id',
  hasTimestamps: false,
}, {
  generate,
  verify,
})


export default UsersEmailVerificationTokens
