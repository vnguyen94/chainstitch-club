import { generateUUID, encryptText } from '../../lib'


/**
 * generates a UUID token for the user and encrypts it.
 * inserts if doesn't exist, updates if it does.
 * @param  {Integer}  userId
 * @return {Promise}
 */
export default async function generate(userId) {
  const token = generateUUID()
  const payload = { token, userId }

  await this.upsert(payload, payload)

  const emailToken = encryptText(JSON.stringify(payload))

  return emailToken
}
