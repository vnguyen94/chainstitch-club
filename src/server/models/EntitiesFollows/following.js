import { Users } from '../'


/**
 * get an entity's user following table.
 * @return {Promise}
 */
function following() {
  return this.belongsTo(Users, 'entity_2_id', 'entity_id')
}

export default following
