/**
 * follows another entity
 * @param  {Integer} entity1_id
 * @param  {Integer} entity2_id
 * @return {Promise}
 */
export default function follow(entity1Id, entity2Id) {
  return this.create({ entity1Id, entity2Id })
}
