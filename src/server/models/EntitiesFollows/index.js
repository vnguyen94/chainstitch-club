import db from '../../db'

import follow from './follow'
import followers from './followers'
import following from './following'
import unfollow from './unfollow'


export function schema(table) {
  table.integer('entity_1_id').unsigned().notNullable()
  table.integer('entity_2_id').unsigned().notNullable()

  table.index(['entity_1_id', 'entity_2_id'])
  table.unique(['entity_1_id', 'entity_2_id'])
  table.foreign('entity_1_id').references('entities.id')
  table.foreign('entity_2_id').references('entities.id')
}

const EntitiesFollows = db.Model.extend({
  tableName: 'entities_follows',
  idAttribute: ['entity_1_id', 'entity_2_id'],
  hasTimestamps: false,

  followers,
  following,
}, {
  follow,
  unfollow,
})


export default EntitiesFollows
