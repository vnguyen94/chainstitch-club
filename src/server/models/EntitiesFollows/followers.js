import { Users } from '../'


/**
 * get an entity's user followers table.
 * @return {Promise}
 */
function followers() {
  return this.belongsTo(Users, 'entity_1_id', 'entity_id')
}

export default followers
