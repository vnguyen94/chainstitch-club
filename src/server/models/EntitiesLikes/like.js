/**
 * likes another entity
 * @param  {Integer} entity1_id
 * @param  {Integer} entity2_id
 * @return {Promise}
 */
export default function like(entity1Id, entity2Id) {
  return this.create({ entity1Id, entity2Id })
}
