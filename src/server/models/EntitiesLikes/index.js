import db from '../../db'

import like from './like'
import likers from './likers'
import unlike from './unlike'


export function schema(table) {
  table.integer('entity_1_id').unsigned().notNullable()
  table.integer('entity_2_id').unsigned().notNullable()

  table.index(['entity_1_id', 'entity_2_id'])
  table.unique(['entity_1_id', 'entity_2_id'])
  table.foreign('entity_1_id').references('entities.id')
  table.foreign('entity_2_id').references('entities.id')
}

const EntitiesLikes = db.Model.extend({
  tableName: 'entities_likes',
  idAttribute: ['entity_1_id', 'entity_2_id'],
  hasTimestamps: false,

  likers,
}, {
  like,
  unlike,
})


export default EntitiesLikes
