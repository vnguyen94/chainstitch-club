/**
 * unfollows another entity
 * @param  {Integer} entity1_id
 * @param  {Integer} entity2_id
 * @return {Promise}
 */
export default function unfollow(entity1Id, entity2Id) {
  return this.where({
    entity_1_id: entity1Id,
    entity_2_id: entity2Id,
  }).destroy({ require: true })
}
