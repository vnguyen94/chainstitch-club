import { Users } from '../'


/**
 * get an entity's user likers table.
 * @return {Promise}
 */
function likers() {
  return this.belongsTo(Users, 'entity_1_id', 'entity_id')
}

export default likers
