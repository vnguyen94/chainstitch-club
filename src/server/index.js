import _debug from 'debug'

import config from '../../config'

import app from './app'

const { SERVER_HOST, SERVER_PORT } = config
const { NODE_ENV } = process.env
const debug = _debug(`app:server:${NODE_ENV}`)


app.listen(SERVER_PORT, () => {
  debug(
    'Koa server listening at ' +
    `http://${SERVER_HOST}:${SERVER_PORT} ` +
    `in ${NODE_ENV} mode`,
  )
})
