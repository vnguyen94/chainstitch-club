import knex from 'knex'
import bookshelf from 'bookshelf'
import ModelBase from 'bookshelf-modelbase'

import config from '../../../config'


export const dbConfig = {
  client: 'pg',
  connection: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    charset: 'utf8',
  },
  // eslint-disable-next-line no-underscore-dangle
  debug: config.__DEV__,
}
const knexDb = knex(dbConfig)
const db = bookshelf(knexDb)

// bookshelf plugins
db.plugin(ModelBase.pluggable)
db.plugin('bookshelf-camelcase')
db.plugin('pagination')


export default db
