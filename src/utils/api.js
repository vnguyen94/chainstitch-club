import axios from 'axios'
import Qs from 'qs'

import config from '../../config'


const defaultAxiosConfig = {
  baseURL: config.API_PATH,
  paramsSerializer: (params) => (
    Qs.stringify(params, { arrayFormat: 'brackets' })
  ),
  timeout: 5000,
  responseType: 'json',
  validateStatus: (status) => (
    status >= 200 &&
    status < 300
  ),
}

export const json = axios.create({
  ...defaultAxiosConfig,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})

export const formData = axios.create({
  ...defaultAxiosConfig,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  },
})
