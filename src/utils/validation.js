import Joi from 'joi'
import Boom from 'boom'


/**
 * sync validation utility function for redux-form forms.
 * @param  {Object} schema Joi schema
 * @return {Object}
 */
export function validateClientForm(schema) {
  return (formObject) => {
    const result = Joi.validate(formObject, schema, { abortEarly: false })

    if (!result.error) {
      return {}
    }

    // reduce Joi error array for redux-form to consume
    return result.error.details.reduce((errors, currentErr) => {
      errors[currentErr.path] = currentErr.message
      return errors
    }, {})
  }
}

/**
 * wraps Joi in a promise for server-side validation.
 * @param  {Object} formObject
 * @param  {Object} schema
 * @return {Object}
 */
export function validatePayload(formObject, schema, options = {}) {
  return new Promise((resolve, reject) => {
    Joi.validate(formObject, schema, options, (err, value) => {
      if (!err) {
        resolve(value)
      } else {
        reject(Boom.badRequest(err))
      }
    })
  })
}

const defaultPaginationSchema = {
  pageSize: Joi.number().min(1).required(),
  page: Joi.number().min(1).required(),
  column: Joi.string().only(['id']).required(),
  direction: Joi.string().only('asc', 'desc').required(),
}
const defaultPagination = {
  pageSize: 25,
  page: 1,
  column: 'id',
  direction: 'asc',
}

/**
 * pass in pagination query params from a route and replaces with
 * default pagination params if necessary.
 * @param  {Object} query
 * @param  {Object} schema Joi schema
 * @return {Object}
 */
export function validatePagination(query, schema = defaultPaginationSchema) {
  return new Promise((resolve /* , reject */) => {
    Joi.validate(query, schema, { abortEarly: false }, (err, value) => {
      if (!err) {
        resolve(value)
        return
      }

      const defaultValues = err.details.reduce(
        (acc, prev) => Object.assign(acc, {
          [prev.path]: defaultPagination[prev.path[0]],
        }),
        value,
      )

      resolve(defaultValues)
    })
  })
}
