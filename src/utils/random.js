/**
 * https://stackoverflow.com/a/1527820
 * @param  {Integer} min
 * @param  {Integer} max
 * @return {Integer}
 */
export function getRandomInt(min = 0, max = 1000000) {
  return Math.floor(Math.random() * (max - (min + 1))) + min
}

/**
/* Specially used to fill in an ID for a React component
 * while waiting for completed data to come back.
 * React components use an ID in order to create unique components,
 * and by using a negative integer we can ensure no collisions
 * with existing IDs.
 * @return {Integer} negative integer
 */
export function getRandomIdPlaceholder() {
  return getRandomInt() * -1
}
