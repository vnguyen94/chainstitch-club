/**
 * helper function to create redux constants.
 * @param  {String} route base route name
 * @param  {[String]} constants
 * @return {Object}
 */
function createConstants(route, constants) {
  return constants.reduce((acc, constant) => {
    acc[constant] = `${route}/${constant}`
    return acc
  }, {})
}

/**
 * helper function to create a reducer.
 * @param  {Object} initialState
 * @param  {Object} reducerMap
 * @return {Function}
 */
function createReducer(initialState, reducerMap) {
  return (state = initialState, action) => {
    const reducer = reducerMap[action.type]
    return reducer ? reducer(state, action) : state
  }
}


export {
  createConstants,
  createReducer,
}
