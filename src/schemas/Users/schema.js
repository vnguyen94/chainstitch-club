import { schema } from 'normalizr'


export const ENTITY_NAME = 'users'

export const single = new schema.Entity(ENTITY_NAME)
export const list = new schema.Array(single)
export const listOfEntities = new schema.Array(single)
