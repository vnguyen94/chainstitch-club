import { schema } from 'normalizr'

import { schema as UsersSchema } from '../Users'
import { schema as ImagesSchema } from '../Images'


export const ENTITY_NAME = 'creations'

export const single = new schema.Entity(ENTITY_NAME)
export const singleWithUser = new schema.Entity(
  ENTITY_NAME,
  {
    fits: new schema.Entity('fits'),
    fades: new schema.Entity('fades'),
    users: UsersSchema.single,
    images: ImagesSchema.list,
  },
)
export const list = new schema.Array(single)
export const listWithUser = new schema.Array(singleWithUser)
