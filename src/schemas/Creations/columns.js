export const columns = [
  'id',
  'user_id',
  'entity_id',
  'type',
  'created_at',
  'updated_at',
]
