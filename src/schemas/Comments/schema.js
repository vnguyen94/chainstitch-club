import { schema } from 'normalizr'

import { schema as CreationsSchema } from '../Creations'


export const ENTITY_NAME = 'comments'

export const single = new schema.Entity(
  ENTITY_NAME,
  { creations: CreationsSchema.singleWithUser },
)
export const list = new schema.Array(single)
