import { schema } from 'normalizr'


export const ENTITY_NAME = 'users_notifications'

export const single = new schema.Entity(ENTITY_NAME)
export const list = new schema.Array(single)
