export const columns = [
  'id',
  'creation_id',
  'title',
  'description',
  'image_url',
]
