import { schema } from 'normalizr'

import { schema as CreationsSchema } from '../Creations'
import { schema as UserSchema } from '../Users'
import { schema as ImagesSchema } from '../Images'


export const ENTITY_NAME = 'fades'

export const fades = new schema.Entity(ENTITY_NAME)
export const listOfEntities = new schema.Array(
  new schema.Entity(ENTITY_NAME, {}, { idAttribute: 'creationId' }),
)

export const single = new schema.Entity(
  CreationsSchema.ENTITY_NAME,
  {
    fades,
    users: UserSchema.single,
    images: ImagesSchema.list,
  },
)
export const list = new schema.Array(single)
