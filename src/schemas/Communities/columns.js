export const columns = [
  'id',
  'creation_id',
  'title',
  'type',
  'description',
  'image_url',
]
