import { schema } from 'normalizr'
import { isEmpty } from 'lodash'

import { ENTITY_NAME as CREATION_ENTITY_NAME } from '../Creations/schema'
import { schema as UserSchema } from '../Users'


export const ENTITY_NAME = 'communities'

export const creationCommunityRelation = new schema.Entity(
  CREATION_ENTITY_NAME,
  {
    fades: new schema.Entity('fades'),
    fits: new schema.Entity('fits'),
    communities: new schema.Entity('communities'),
    users: new schema.Entity('users'),
  },
  {
    processStrategy(value) {
      if (isEmpty(value.fades)) {
        delete value.fades
      }
      if (isEmpty(value.fits)) {
        delete value.fits
      }
      if (isEmpty(value.communities)) {
        delete value.communities
      }

      return value
    },
  },
)

export const communities = new schema.Entity(
  ENTITY_NAME,
  { creations: new schema.Array(creationCommunityRelation) },
)
export const communitiesList = new schema.Array(communities)
export const listOfEntities = new schema.Array(
  new schema.Entity(ENTITY_NAME, {}, { idAttribute: 'creationId' }),
)

export const single = new schema.Entity(
  CREATION_ENTITY_NAME,
  { communities, users: UserSchema.single },
)

export const list = new schema.Array(single)
