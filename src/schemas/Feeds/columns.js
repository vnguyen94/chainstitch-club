export const columns = [
  'id',
  'actor',
  'verb',
  'object',
  'is_public',
  'target',
  'created_at',
  'updated_at',
]
