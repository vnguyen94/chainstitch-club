import { schema } from 'normalizr'

import { schema as UsersSchema } from '../Users'


export const ENTITY_NAME = 'feeds'

export const single = new schema.Entity(
  ENTITY_NAME,
  { actor: UsersSchema.single },
)
export const list = new schema.Array(single)
