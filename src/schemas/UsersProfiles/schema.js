import { schema } from 'normalizr'

import { schema as CreationsSchema } from '../Creations'


export const ENTITY_NAME = 'users_profiles'

export const usersProfiles = new schema.Entity(ENTITY_NAME)
export const listOfEntities = new schema.Array(
  new schema.Entity(ENTITY_NAME, {}, { idAttribute: 'creationId' }),
)

export const single = new schema.Entity(
  CreationsSchema.ENTITY_NAME,
  { users_profiles: usersProfiles },
)
export const list = new schema.Array(single)
