export const columns = [
  'id',
  'creation_id',
  'first_name',
  'last_name',
  'gender',
  'location',
  'description',
]
