import { schema } from 'normalizr'


export const ENTITY_NAME = 'images'

export const single = new schema.Entity(ENTITY_NAME)
export const list = new schema.Array(single)
