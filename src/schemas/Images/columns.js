export const columns = [
  'id',
  'entity_id',
  'url',
  'height',
  'width',
  'order',
  'created_at',
  'updated_at',
]
