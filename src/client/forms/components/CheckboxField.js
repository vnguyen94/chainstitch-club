import PropTypes from 'prop-types'
import React from 'react'
import { Field } from 'redux-form'

import RenderField from './RenderField'
import styles from './styles.scss'


const propTypes = {
  name: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
}

const CheckboxField = ({ name, caption, ...rest }) => (
  <div className={styles.checkboxContainer}>
    <label htmlFor={`input--${name}`}>
      <Field
        id={`input--${name}`}
        component={RenderField}
        name={name}
        type="checkbox"
        {...rest}
      />
      <span>{caption}</span>
    </label>
  </div>
)

CheckboxField.propTypes = propTypes


export default CheckboxField
