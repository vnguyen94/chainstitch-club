import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'

import RenderField from './RenderField'


const DateField = ({ name, caption, ...rest }) => (
  <div>
    <label htmlFor={`input--${name}`}>
      {caption}
      <Field
        id={`input--${name}`}
        component={RenderField}
        name={name}
        {...rest}
      />
    </label>
  </div>
)

DateField.propTypes = {
  name: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
}


export default DateField
