import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'redux-form'
import Select from 'react-select'

import styles from './styles.scss'


const RenderField = ({ input, config, elements }) => (
  <Select
    className={styles.dropdown}
    options={elements.map((element) => ({
      value: element,
      label: element,
    }))}
    value={input.value}
    {...config}
    {...input}
  />
)

RenderField.propTypes = {
  input: PropTypes.object.isRequired,
  elements: PropTypes.array.isRequired,
  config: PropTypes.object,
}

const DropdownField = ({ name, caption, elements, config, ...rest }) => (
  <div>
    <label htmlFor={`input--${name}`}>
      <Field
        id={`input--${name}`}
        component={RenderField}
        name={name}
        elements={elements}
        config={config}
        {...rest}
      />
    </label>
  </div>
)

DropdownField.propTypes = {
  name: PropTypes.string.isRequired,
  caption: PropTypes.string.isRequired,
  elements: PropTypes.array.isRequired,
  config: PropTypes.object,
}


export default DropdownField
