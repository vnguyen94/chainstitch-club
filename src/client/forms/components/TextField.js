import PropTypes from 'prop-types'
import React from 'react'
import { Field } from 'redux-form'

import RenderField from './RenderField'


const propTypes = {
  name: PropTypes.string.isRequired,
}

const TextField = ({ name, ...rest }) => (
  <div>
    <label htmlFor={`input--${name}`}>
      <Field
        id={`input--${name}`}
        component={RenderField}
        name={name}
        {...rest}
      />
    </label>
  </div>
)

TextField.propTypes = propTypes


export default TextField
