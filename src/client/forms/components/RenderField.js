import PropTypes from 'prop-types'
import React from 'react'

import styles from './styles.scss'


const propTypes = {
  id: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  caption: PropTypes.string,
  meta: PropTypes.object,
  type: PropTypes.string,
}

const RenderField = ({ id, input, caption, meta, type = 'text' }) => (
  <span>
    <input
      id={id}
      type={type}
      placeholder={caption}
      className={styles.input}
      {...input}
    />
    {meta.touched && meta.error && (
      <p>{meta.error}</p>
    )}
  </span>
)

RenderField.propTypes = propTypes


export default RenderField
