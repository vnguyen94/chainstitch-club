import React from 'react'
import PropTypes from 'prop-types'
import Select from 'react-select'
import { Field } from 'redux-form'


const RenderField = ({ elements, config }) => (
  <Select
    options={elements}
    {...config}
  />
)

RenderField.propTypes = {
  elements: PropTypes.array.isRequired,
  config: PropTypes.object.isRequired,
}

const SelectAsyncField = ({ name, caption, elements, config, ...rest }) => (
  <div>
    <label htmlFor={`input--${name}`}>
      {caption}
      <Field
        id={`input--${name}`}
        component={RenderField}
        name={name}
        elements={elements}
        config={config}
        {...rest}
      />
    </label>
  </div>
)

SelectAsyncField.propTypes = {
  name: PropTypes.string.isRequired,
  elements: PropTypes.array.isRequired,
  caption: PropTypes.string,
  config: PropTypes.object.isRequired,
}


export default SelectAsyncField
