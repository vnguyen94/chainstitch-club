import PropTypes from 'prop-types'
import React from 'react'


function adaptFileEventToValue(delegate) {
  return (e) => delegate(e.target.files[0])
}

const propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object,
}

const ImageFileField = ({
  input: {
    value: omitValue,
    onChange,
    onBlur,
    ...inputProps
  },
  meta,
  ...props
}) => (
  <span>
    <input
      onChange={adaptFileEventToValue(onChange)}
      onBlur={adaptFileEventToValue(onBlur)}
      type="file"
      {...inputProps}
      {...props}
    />
    {meta.touched && meta.error && (
      <p>{meta.error}</p>
    )}
  </span>
)

ImageFileField.propTypes = propTypes


export default ImageFileField
