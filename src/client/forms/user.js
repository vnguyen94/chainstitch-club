import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import { TextField, DropdownField } from './components'


export const UserSection = () => (
  <div>
    <TextField
      name="emailOrUsername"
      caption="email or username"
    />
    <TextField
      name="password"
      type="password"
      caption="password"
    />
  </div>
)

export const FullProfileSection = () => (
  <div>
    <TextField
      name="username"
      caption="username"
    />
    <TextField
      name="email"
      caption="email"
    />
    <TextField
      name="password"
      type="password"
      caption="password"
    />
    <TextField
      name="confirmPassword"
      type="password"
      caption="confirm password"
    />
  </div>
)

export const EmailSection = () => (
  <div>
    <TextField
      name="email"
      caption="email"
    />
  </div>
)

export const PasswordSection = () => (
  <div>
    <TextField
      name="password"
      type="password"
      caption="password"
    />
    <TextField
      name="confirmPassword"
      type="password"
      caption="confirm password"
    />
  </div>
)

export const UsernameSection = () => (
  <div>
    <TextField
      name="username"
      caption="username"
    />
  </div>
)

export const UserProfileSection = () => (
  <div>
    <Row>
      <Col xs={12}>
        <TextField
          name="firstName"
          caption="first name"
        />
      </Col>
      <Col xs={12}>
        <TextField
          name="lastName"
          caption="last name"
        />
      </Col>
    </Row>
    <Row>
      <Col xs={12}>
        <DropdownField
          name="gender"
          caption="gender"
          elements={['undisclosed', 'male', 'female', 'other']}
          config={{
            placeholder: 'gender',
            required: true,
            searchable: false,
            clearableValue: false,
          }}
        />
      </Col>
      <Col xs={12}>
        <TextField
          name="location"
          caption="location"
        />
      </Col>
    </Row>
    <Row>
      <Col xs={12}>
        <TextField
          name="description"
          placeholder="description"
          component="textarea"
        />
      </Col>
    </Row>
  </div>
)
