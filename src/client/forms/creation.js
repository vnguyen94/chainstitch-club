import React from 'react'

import { TextField } from './components'


export const FitSection = () => (
  <div>
    <TextField
      name="title"
      caption="title"
    />
    <TextField
      name="description"
      caption="description"
    />
  </div>
)

export const FadeSection = () => (
  <div>
    <TextField
      name="title"
      caption="title"
    />
    <TextField
      name="description"
      caption="description"
    />
  </div>
)
