import { App, AuthenticatedRoute } from './components'


/**
 * takes a route object and translates it to a
 * react-router async component.
 * @param  {String} options.path      URL path
 * @param  {String} options.component React component
 * @return {Object}
 */
function createCodeSplitRoute({ path, component, ...rest }) {
  return {
    async getComponent(location, callback) {
      try {
        const asyncComponent = await import(
          `./pages/${component}/${component}`,
          /* webpackChunkName: "[request]" */
        )

        callback(null, asyncComponent.default)
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error('Dynamic page loading failed.', err.stack)
      }
    },
    path,
    ...rest,
  }
}

const plainRoutes = [
  {
    path: '/',
    component: 'Explore',
    activePage: 'popular',
  },
  {
    path: '/featured',
    component: 'Explore',
    activePage: 'featured',
  },
  {
    path: '/new',
    component: 'Explore',
    activePage: 'new',
  },
  {
    path: '/about',
    component: 'About',
  },
  {
    path: '/feed',
    component: 'Feed',
  },
  {
    path: '/search',
    component: 'Search',
  },
  {
    path: '/fades',
    component: 'CreationsList',
    creationType: 'fades',
  },
  {
    path: '/fades/:creationId',
    component: 'CreationsDetail',
    creationType: 'fades',
  },
  {
    path: '/fits',
    component: 'CreationsList',
    creationType: 'fits',
  },
  {
    path: '/fits/:creationId',
    component: 'CreationsDetail',
    creationType: 'fits',
  },
  {
    path: '/communities',
    component: 'CommunitiesList',
    activePage: 'popular',
  },
  {
    path: '/communities/styles',
    component: 'CommunitiesList',
    activePage: 'styles',
  },
  {
    path: '/communities/brands',
    component: 'CommunitiesList',
    activePage: 'brands',
  },
  {
    path: '/communities/regions',
    component: 'CommunitiesList',
    activePage: 'regions',
  },
  {
    path: '/communities/:communityId',
    component: 'CommunitiesDetail',
  },
  {
    path: '/login',
    component: 'Login',
  },
  {
    path: '/signup',
    component: 'Signup',
  },
  {
    path: '/help',
    component: 'Help',
    activePage: 'general',
  },
  {
    path: '/help/account',
    component: 'Help',
    activePage: 'account',
  },
  {
    path: '/help/posting',
    component: 'Help',
    activePage: 'posting',
  },
  {
    path: '/help/moderation',
    component: 'Help',
    activePage: 'moderation',
  },
  {
    path: '/users/:userId',
    component: 'UserProfile',
    activePage: 'overview',
  },
  {
    path: '/users/:userId/fades',
    component: 'UserProfile',
    activePage: 'creations',
    creationName: 'fades',
  },
  {
    path: '/users/:userId/fits',
    component: 'UserProfile',
    activePage: 'creations',
    creationName: 'fits',
  },
  {
    path: '/users/:userId/likes',
    component: 'UserProfile',
    activePage: 'likes',
  },
  {
    path: '/users/:userId/about',
    component: 'UserProfile',
    activePage: 'about',
  },
  {
    path: '/users/:userId/followers',
    component: 'UserProfile',
    activePage: 'followers',
  },
  {
    path: '/users/:userId/following',
    component: 'UserProfile',
    activePage: 'following',
  },
  {
    path: '/sitemap',
    component: 'Sitemap',
  },
  {
    path: '/404',
    component: 'ErrorNotFound',
  },
  {
    path: '/403',
    component: 'ErrorUnauthorized',
  },
]

const userRoutes = [
  {
    path: '/settings',
    component: 'Settings',
    activePage: 'general',
  },
  {
    path: '/settings/general',
    component: 'Settings',
    activePage: 'general',
  },
  {
    path: '/settings/security',
    component: 'Settings',
    activePage: 'security',
  },
  {
    path: '/settings/notifications',
    component: 'Settings',
    activePage: 'notifications',
  },
  {
    path: '/settings/delete',
    component: 'Settings',
    activePage: 'delete',
  },
  {
    path: '/fits/:creationId/edit',
    component: 'CreationsDashboard',
    creationType: 'fit',
    activePage: 'general',
  },
  {
    path: '/fits/:creationId/edit/privacy',
    component: 'CreationsDashboard',
    creationType: 'fit',
    activePage: 'privacy',
  },
  {
    path: '/fits/:creationId/edit/communities',
    component: 'CreationsDashboard',
    creationType: 'fit',
    activePage: 'communities',
  },
  {
    path: '/fades/:creationId/edit',
    component: 'CreationsDashboard',
    creationType: 'fade',
    activePage: 'general',
  },
  {
    path: '/fades/:creationId/edit/timeline',
    component: 'CreationsDashboard',
    creationType: 'fade',
    activePage: 'timeline',
  },
  {
    path: '/fades/:creationId/edit/privacy',
    component: 'CreationsDashboard',
    creationType: 'fade',
    activePage: 'privacy',
  },
  {
    path: '/fades/:creationId/edit/communities',
    component: 'CreationsDashboard',
    creationType: 'fade',
    activePage: 'communities',
  },
  {
    path: '/upload/fit',
    component: 'UploadCreation',
    creationType: 'fit',
  },
  {
    path: '/upload/fade',
    component: 'UploadCreation',
    creationType: 'fade',
  },
]

function onChangePagePreserveScroll(prevState, nextState) {
  if (nextState.location.action !== 'POP') {
    window.scrollTo(0, 0)
  }
}

export default {
  component: App,
  onChange: onChangePagePreserveScroll,
  childRoutes: [
    ...plainRoutes.map(createCodeSplitRoute),
    {
      component: AuthenticatedRoute.user,
      childRoutes: userRoutes.map(createCodeSplitRoute),
    },
    createCodeSplitRoute({
      path: '*',
      component: 'ErrorNotFound',
    }),
  ],
}
