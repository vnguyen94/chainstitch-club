import { createConstants, createReducer } from '../../utils'


export const NAME = 'entities'

export const types = createConstants(NAME, [
  'INSERT_ENTITY',
])

export const initialState = {
}

export function insertEntity(entities) {
  return {
    type: types.INSERT_ENTITY,
    payload: {
      ...entities,
    },
  }
}

export default createReducer(initialState, {
  [types.INSERT_ENTITY]: (state, { payload }) => (
    Object.keys(payload).reduce((acc, entityKey) => {
      if (acc[entityKey]) {
        acc[entityKey] = {
          ...acc[entityKey],
          ...payload[entityKey],
        }
      } else {
        acc[entityKey] = payload[entityKey]
      }

      return acc
    }, state)
  ),
})
