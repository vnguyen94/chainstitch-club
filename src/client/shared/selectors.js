import { createSelector } from 'reselect'

import { getStateLoginSelector } from '../pages/Login/selectors'

import { NAME as ENTITY_NAME } from './entities'


export const getStateAuthSelector = (state) => state.auth
export const getStateEntitiesSelector = (state) => state[ENTITY_NAME]

export const getAuthHeadersSelector = (state) => ({
  headers: {
    Authorization: `Bearer ${state.auth.token}`,
  },
})

export const getCurrentUserSelector = createSelector(
  [getStateLoginSelector],
  (state) => state.user,
)
