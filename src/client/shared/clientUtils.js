const vh = Math.max(
  document.documentElement.clientHeight,
  window.innerHeight || 0,
)

export function normalizeDimensions(width, height) {
  const newHeight = vh * 0.4
  const newWidth = (newHeight * width) / height

  if (newWidth > width) {
    return { width, height }
  }

  return { width: newWidth, height: newHeight }
}
