import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'

import EntityReducer, { NAME as ENTITY_NAME } from './shared/entities'
import * as Explore from './pages/Explore'
import * as Feed from './pages/Feed'
import * as Login from './pages/Login'
import * as Signup from './pages/Signup'
import * as Settings from './pages/Settings'
import * as UserProfile from './pages/UserProfile'
import * as UploadCreation from './pages/UploadCreation'
import * as CommunitiesList from './pages/CommunitiesList'
import * as CommunitiesDetail from './pages/CommunitiesDetail'
import * as CreationsDashboard from './pages/CreationsDashboard'
import * as CreationsDetail from './pages/CreationsDetail'
import * as CreationsList from './pages/CreationsList'


const rootReducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  [ENTITY_NAME]: EntityReducer,
  [Explore.NAME]: Explore.reducer,
  [Feed.NAME]: Feed.reducer,
  [Login.NAME]: Login.reducer,
  [Signup.NAME]: Signup.reducer,
  [UserProfile.NAME]: UserProfile.reducer,
  [Settings.NAME]: Settings.reducer,
  [UploadCreation.NAME]: UploadCreation.reducer,
  [CommunitiesDetail.NAME]: CommunitiesDetail.reducer,
  [CommunitiesList.NAME]: CommunitiesList.reducer,
  [CreationsDashboard.NAME]: CreationsDashboard.reducer,
  [CreationsDetail.NAME]: CreationsDetail.reducer,
  [CreationsList.NAME]: CreationsList.reducer,
})


export default rootReducer
