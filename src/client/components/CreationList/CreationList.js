import PropTypes from 'prop-types'
import React from 'react'
import className from 'classnames'
import { Row, Col } from 'react-flexbox-grid/lib'

import CreationCard from '../CreationCard'

import styles from './styles.scss'


const propTypes = {
  creations: PropTypes.array.isRequired,
  rowProps: PropTypes.object,
  colStyles: PropTypes.string,
  colProps: PropTypes.object,
  creationCardProps: PropTypes.object,
}

const defaultProps = {
  rowProps: {
    center: 'xs',
  },
  colStyles: '',
  colProps: {
    md: 8,
    mdOffset: 2,
  },
  creationCardProps: {},
}

const CreationList = ({
  creations,
  rowProps,
  colStyles,
  colProps,
  creationCardProps,
}) => (
  <Row {...rowProps}>
    {creations && creations.map((creation) => (
      <Col
        key={creation.id}
        className={className(styles.card, colStyles)}
        {...colProps}
      >
        <CreationCard
          creation={creation}
          {...creationCardProps}
        />
      </Col>
    ))}
  </Row>
)

CreationList.propTypes = propTypes
CreationList.defaultProps = defaultProps


export default CreationList
