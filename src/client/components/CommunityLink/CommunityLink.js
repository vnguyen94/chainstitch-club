import PropTypes from 'prop-types'
import React from 'react'
import classNames from 'classnames'
import { Link } from 'react-router'
import { kebabCase } from 'lodash'


const propTypes = {
  community: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
  className: PropTypes.string,
}

const CommunityLink = ({ className, community, children }) => {
  const title = kebabCase(community.title)
  const to = {
    pathname: `/communities/${community.creationId}`,
    query: { title },
  }

  return (
    <Link
      to={to}
      className={className
        ? classNames(className)
        : undefined
      }
    >
      {children}
    </Link>
  )
}

CommunityLink.propTypes = propTypes


export default CommunityLink
