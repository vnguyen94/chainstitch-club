import React from 'react'
import PropTypes from 'prop-types'
import className from 'classnames'

import styles from './styles.scss'


const propTypes = {
  className: PropTypes.any,
}

const DropdownCaret = (props) => (
  <svg className={className(props.className, styles.caret)}>
    <path
      d="M0,10 20,10 10,0z"
    />
    <path
      className={styles.caretBorder}
      d="M0,10 10,0 20,10"
    />
  </svg>
)

DropdownCaret.propTypes = propTypes


export default DropdownCaret
