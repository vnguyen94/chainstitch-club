import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import { CommunityLink } from '../'

import styles from './styles.scss'


const propTypes = {
  community: PropTypes.object.isRequired,
}

const CommunityCard = ({ community }) => (
  <Row center="xs" className={styles.card}>
    <Col xs={12} className={styles.imageHolder}>
      <CommunityLink community={community}>
        <img
          src={community.imageUrl}
          alt={community.title}
          className={styles.image}
        />
      </CommunityLink>
    </Col>
    <Col xs={12} className={styles.description}>
      <CommunityLink community={community}>
        {community.title}
      </CommunityLink>
      <h5>5 members</h5>
    </Col>
  </Row>
)

CommunityCard.propTypes = propTypes


export default CommunityCard
