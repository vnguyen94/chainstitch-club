import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Link } from 'react-router'
import { Col } from 'react-flexbox-grid/lib'

import styles from './styles.scss'


const propTypes = {
  filter: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  basePath: PropTypes.string.isRequired,
  sort: PropTypes.string,
}

const DropdownLink = ({ filter, sort, basePath, onClick }) => (
  <li className={styles.option}>
    <Col xs>
      <Link
        className={classNames({
          [styles.active]: filter.isActive(sort),
        })}
        to={`${basePath}${filter.query}`}
        onClick={onClick}
      >
        {filter.name}
      </Link>
    </Col>
  </li>
)

DropdownLink.propTypes = propTypes


export default DropdownLink
