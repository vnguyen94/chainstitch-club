import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Link } from 'react-router'
import { Col } from 'react-flexbox-grid/lib'

import styles from './styles.scss'


const propTypes = {
  route: PropTypes.object.isRequired,
  option: PropTypes.object.isRequired,
}

const NavbarOption = ({ route, option }) => (
  <li className={styles.option}>
    <Col xs>
      <Link
        className={classNames({
          [styles.active]: route.activePage === option.name,
        })}
        to={option.url}
      >
        {option.name}
      </Link>
    </Col>
  </li>
)

NavbarOption.propTypes = propTypes


export default NavbarOption
