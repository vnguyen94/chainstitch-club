import React, { Component } from 'react'
import PropTypes from 'prop-types'
import className from 'classnames'
import Dropdown, {
  DropdownTrigger,
  DropdownContent,
} from 'react-simple-dropdown'
import CaretDownIcon from 'react-icons/lib/fa/caret-down'

import { DropdownCaret } from '../../'

import styles from './styles.scss'
import DropdownLink from './DropdownLink'


const propTypes = {
  location: PropTypes.object.isRequired,
  filters: PropTypes.array.isRequired,
}

class CreationDropdown extends Component {
  onClickLinkHandler = () => {
    this.dropdown.hide()
  }

  bindDropdownRef = (input) => {
    this.dropdown = input
  }

  render() {
    const {
      location: { pathname, query },
      filters,
    } = this.props
    const possibleFilter = filters.find((filter) => (
      filter.name === query.sort
    ))
    const selectedFilter = possibleFilter
      ? possibleFilter.name
      : 'all'

    return (
      <div className="dropdown">
        <Dropdown ref={this.bindDropdownRef}>
          <DropdownTrigger className="dropdown--trigger">
            <span>{selectedFilter}</span>
            <CaretDownIcon />
          </DropdownTrigger>
          <DropdownContent
            className={className(styles.dropdownCreation, 'dropdown--content')}
          >
            <ul className="dropdown--content__list">
              {filters && filters.map((filter) => (
                <DropdownLink
                  key={filter.name}
                  filter={filter}
                  sort={query.sort}
                  basePath={pathname}
                  onClick={this.onClickLinkHandler}
                />
              ))}
            </ul>
            <DropdownCaret />
          </DropdownContent>
        </Dropdown>
      </div>
    )
  }
}

CreationDropdown.propTypes = propTypes


export default CreationDropdown
