import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'
import classNames from 'classnames'

import styles from './styles.scss'
import { CreationDropdown, NavbarOption } from './components'


const propTypes = {
  location: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  options: PropTypes.array.isRequired,
  filters: PropTypes.array,
}

const defaultProps = {
  filters: [],
}

const Navbar = ({ location, route, options, filters }) => (
  <Row start="xs" className={classNames(styles.container, 'navbar')}>
    <Col xs={12} md={9}>
      <ul className={classNames(styles.explore, 'navbar--list')}>
        {options && options.map((option) => (
          <NavbarOption
            key={option.name}
            option={option}
            route={route}
          />
        ))}
      </ul>
    </Col>
    <Col xs={12} md={3}>
      {filters.length ? (
        <ul className={classNames(styles.filters, 'navbar--list')}>
          <li>
            <CreationDropdown
              location={location}
              filters={filters}
            />
          </li>
        </ul>
      ) : undefined}
    </Col>
  </Row>
)

Navbar.propTypes = propTypes
Navbar.defaultProps = defaultProps


export default Navbar
