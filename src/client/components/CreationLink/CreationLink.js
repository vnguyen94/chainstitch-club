import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'
import { kebabCase } from 'lodash'


const propTypes = {
  creation: PropTypes.object.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string,
  ]).isRequired,
}

const CreationLink = ({ creation, children, ...rest }) => {
  const typeData = creation[creation.type]
  const title = kebabCase(typeData.title)
  const wornBy = creation.users.username
  const to = {
    pathname: `/${creation.type}/${creation.id}`,
    query: { title, wornBy },
  }

  return (
    <Link {...rest} to={to}>
      {children}
    </Link>
  )
}

CreationLink.propTypes = propTypes


export default CreationLink
