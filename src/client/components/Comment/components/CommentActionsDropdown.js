import React, { Component } from 'react'
import Dropdown, {
  DropdownTrigger,
  DropdownContent,
} from 'react-simple-dropdown'
import className from 'classnames'
import EllipsisIcon from 'react-icons/lib/fa/ellipsis-h'

import { DropdownCaret } from '../../../components'
import styles from '../styles.scss'


/**
 * show elippsis by 2 ways:
 * 1) CSS (on hover)
 * 2) JS
 *     attaches onHide/onShow handlers to dropdown
 *     to track dropdown state to keep ellipsis visible
 */
class CommentActionsDropdown extends Component {
  constructor() {
    super()

    this.state = {
      isDropdownActive: false,
    }
  }

  onClickLinkHandler = () => {
    this.dropdown.hide()
  }

  onHideDropdownHandler = () => {
    this.setState({ isDropdownActive: false })
  }

  onShowDropdownHandler = () => {
    this.setState({ isDropdownActive: true })
  }

  bindDropdownRef = (input) => {
    this.dropdown = input
  }

  render() {
    const { isDropdownActive } = this.state

    return (
      <Dropdown
        ref={this.bindDropdownRef}
        onHide={this.onHideDropdownHandler}
        onShow={this.onShowDropdownHandler}
      >
        <DropdownTrigger className="dropdown--trigger">
          <EllipsisIcon
            className={className({
              [styles.show]: isDropdownActive,
              [styles.ellipsis]: true,
            })}
          />
        </DropdownTrigger>
        <DropdownContent
          className={className(
            styles.actionsDropdown,
            'dropdown--content',
          )}
        >
          <ul className="dropdown--content__list">
            <li>
              <button
                className="dropdown--button"
                onClick={this.onClickLinkHandler}
              >
                delete
              </button>
            </li>
            <li>
              <button
                className="dropdown--button"
                onClick={this.onClickLinkHandler}
              >
                report
              </button>
            </li>
          </ul>
          <DropdownCaret className={styles.caret} />
        </DropdownContent>
      </Dropdown>
    )
  }
}


export default CommentActionsDropdown
