import PropTypes from 'prop-types'
import React from 'react'
import classNames from 'classnames'
import { Row, Col } from 'react-flexbox-grid/lib'
import Linkify from 'linkifyjs/react'

import { UserLink, UserAvatar, DateElapsed } from '../'

import styles from './styles.scss'
import { CommentActionsDropdown } from './components'


const Link = (props) => <a href={props.to}>{props.children}</a>
Link.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node,
}

const propTypes = {
  comment: PropTypes.object.isRequired,
}

const Comment = ({ comment }) => (
  <Row
    className={classNames(styles.comment, styles.containerPadding)}
    start="xs"
  >
    <Col xs={2}>
      <UserLink user={comment.creations.users}>
        <UserAvatar user={comment.creations.users} />
      </UserLink>
    </Col>
    <Col xs={9}>
      <Row>
        <Col xs={6}>
          <UserLink user={comment.creations.users} />
        </Col>
        <Col xs={6} className={styles.ellipsisContainer}>
          <CommentActionsDropdown />
        </Col>
        <Col xs={12}>
          <DateElapsed
            time={comment.creations.createdAt}
            isPosting={comment.isPosting}
          />
        </Col>
        <Col xs={12}>
          <Linkify
            tagName="p"
            options={{
              tagName: {
                mention: () => Link,
              },
              attributes: (href, type) => {
                if (type === 'email') {
                  return {
                    href: null,
                  }
                } else if (type === 'mention') {
                  return {

                  }
                }
                return {
                  rel: 'nofollow noreferrer',
                  email: null,
                }
              },
            }}
          >
            {comment.body}
          </Linkify>
        </Col>
      </Row>
    </Col>
  </Row>
)

Comment.propTypes = propTypes


export default Comment
