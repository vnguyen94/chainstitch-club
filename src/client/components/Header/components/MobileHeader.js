import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'
import { Row, Col } from 'react-flexbox-grid/lib/index'
import HamburgerIcon from 'react-icons/lib/fa/bars'

import config from '../../../config'

import styles from './styles.scss'


const propTypes = {
  isMobileMenuOpen: PropTypes.bool.isRequired,
  isUploadMenuOpen: PropTypes.bool.isRequired,
  toggleMobileNavbar: PropTypes.func.isRequired,
  toggleUploadMenu: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
}

const MobileHeader = ({
  isMobileMenuOpen,
  isUploadMenuOpen,
  toggleMobileNavbar,
  toggleUploadMenu,
  auth,
}) => (
  <Row className={styles.headerMobile}>
    <Col>
      <Link to="/">{config.app.title}</Link>
      <button
        className={styles.hamburger}
        onClick={toggleMobileNavbar}
      >
        <HamburgerIcon />
      </button>
    </Col>
    {isMobileMenuOpen && (
      <ul>
        <li><Link to="/explore">explore</Link></li>
        <li><Link to="/search">search</Link></li>
        <li>
          <button
            className={styles.uploadToggle}
            onClick={toggleUploadMenu}
          >
            upload {isUploadMenuOpen ? '-' : '+'}
          </button>
        </li>
        {isUploadMenuOpen && (
          <ul>
            <li><Link to="/upload/fit">upload fit</Link></li>
            <li><Link to="/upload/fade">upload fade</Link></li>
          </ul>
        )}
        {auth.user && (
          <li><Link to="/logout">logout</Link></li>
        )}
        {!auth.user && (
          <li><Link to="/login">login</Link></li>
        )}
        {!auth.user && (
          <li><Link to="/signup">signup</Link></li>
        )}
      </ul>
    )}
  </Row>
)

MobileHeader.propTypes = propTypes


export default MobileHeader
