import React, { Component } from 'react'
import { Link } from 'react-router'
import Dropdown, {
  DropdownTrigger,
  DropdownContent,
} from 'react-simple-dropdown'
import className from 'classnames'
import CaretDownIcon from 'react-icons/lib/fa/caret-down'
import PlusIcon from 'react-icons/lib/fa/plus'

import { DropdownCaret } from '../../'

import styles from './styles.scss'


class UploadDropdown extends Component {
  onClickLinkHandler = () => {
    this.dropdown.hide()
  }

  bindDropdownRef = (input) => {
    this.dropdown = input
  }

  render() {
    return (
      <div className="dropdown">
        <Dropdown ref={this.bindDropdownRef}>
          <DropdownTrigger className="dropdown--trigger">
            <PlusIcon />
            <CaretDownIcon />
          </DropdownTrigger>
          <DropdownContent
            className={className(styles.dropdownUpload, 'dropdown--content')}
          >
            <ul className="dropdown--content__list">
              <li>
                <Link
                  to="/upload/fit"
                  onClick={this.onClickLinkHandler}
                >
                  new fit
                </Link>
              </li>
              <li>
                <Link
                  to="/upload/fade"
                  onClick={this.onClickLinkHandler}
                >
                  new fade
                </Link>
              </li>
            </ul>
            <DropdownCaret />
          </DropdownContent>
        </Dropdown>
      </div>
    )
  }
}


export default UploadDropdown
