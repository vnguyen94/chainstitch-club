import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router'
import Dropdown, {
  DropdownTrigger,
  DropdownContent,
} from 'react-simple-dropdown'
import className from 'classnames'
import CaretDownIcon from 'react-icons/lib/fa/caret-down'

import { UserLink, DropdownCaret, UserAvatar } from '../../'

import styles from './styles.scss'


const propTypes = {
  user: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
}

class LoginDropdown extends Component {
  onClickLinkHandler = () => {
    this.dropdown.hide()
  }

  onClickLogoutHandler = () => {
    const { logout } = this.props

    this.onClickLinkHandler()
    logout()
  }

  bindDropdownRef = (input) => {
    this.dropdown = input
  }

  render() {
    const { user } = this.props

    return (
      <div className="dropdown">
        <Dropdown ref={this.bindDropdownRef}>
          <DropdownTrigger className="dropdown--trigger">
            <UserAvatar user={user} />
            <CaretDownIcon />
          </DropdownTrigger>
          <DropdownContent
            className={className(styles.dropdownAccount, 'dropdown--content')}
          >
            <ul className="dropdown--content__list">
              <li>
                <UserLink
                  user={user}
                  textContent="profile"
                  onClick={this.onClickLinkHandler}
                />
              </li>
              <li>
                <Link
                  to={`/users/${user.id}/likes`}
                  onClick={this.onClickLinkHandler}
                >
                  my likes
                </Link>
              </li>
              <li>
                <Link
                  to={`/users/${user.id}/following`}
                  onClick={this.onClickLinkHandler}
                >
                  my follows
                </Link>
              </li>
              {false && (
                <li>
                  <Link
                    to="/statistics"
                    onClick={this.onClickLinkHandler}
                  >
                    statistics
                  </Link>
                </li>
              )}
              <li className="dropdown--content__list-divider" />
              <li>
                <Link
                  to="/help"
                  onClick={this.onClickLinkHandler}
                >
                  help
                </Link>
              </li>
              <li>
                <Link
                  to="/settings"
                  onClick={this.onClickLinkHandler}
                >
                  settings
                </Link>
              </li>
              <li>
                <button
                  className="dropdown--button"
                  onClick={this.onClickLogoutHandler}
                >
                    logout
                </button>
              </li>
            </ul>
            <DropdownCaret />
          </DropdownContent>
        </Dropdown>
      </div>
    )
  }
}

LoginDropdown.propTypes = propTypes


export default LoginDropdown
