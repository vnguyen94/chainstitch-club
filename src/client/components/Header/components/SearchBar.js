import React from 'react'
import SearchIcon from 'react-icons/lib/fa/search'

import styles from './styles.scss'


const SearchBar = () => (
  <div className={styles.search}>
    <label htmlFor="header--search">
      <SearchIcon className={styles.searchIcon} />
      <input
        id="header--search"
        className={styles.searchInput}
        name="search"
        type="text"
      />
    </label>
  </div>
)


export default SearchBar
