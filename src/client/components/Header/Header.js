import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router'
import classNames from 'classnames'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import {
  UploadDropdown,
  LoginDropdown,
  SearchBar,
  MobileHeader,
} from './components'
import styles from './styles.scss'


const propTypes = {
  auth: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  pathname: PropTypes.string.isRequired,
}

class Header extends Component {
  constructor() {
    super()

    this.state = {
      isMobileMenuOpen: false,
      isUploadMenuOpen: false,
    }
  }

  onClickMobileNavbarHandler = (e) => {
    e.preventDefault()

    const { isMobileMenuOpen } = this.state

    this.setState({ isMobileMenuOpen: !isMobileMenuOpen })
  }

  onClickUploadMenuHandler = (e) => {
    e.preventDefault()

    const { isUploadMenuOpen } = this.state

    this.setState({ isUploadMenuOpen: !isUploadMenuOpen })
  }

  render() {
    const { auth, logout, pathname } = this.props
    const { isMobileMenuOpen, isUploadMenuOpen } = this.state
    const redirectUrl = pathname === '/'
      ? ''
      : `?redirect=${encodeURIComponent(pathname)}`

    return (
      <header className={styles.container}>
        <Row className={styles.header}>
          <Col xs={12} md={1}>
            <Row middle="xs" center="xs" start="md">
              <Col xs={12}>
                <Link to="/" className={styles.title}>CSC</Link>
              </Col>
            </Row>
          </Col>
          <Col xs={12} md={11}>
            <Row middle="xs" end="xs">
              {auth.user &&
                <Col xs={2}>
                  <Link to="/feed">feed</Link>
                </Col>
              }
              <Col xs={2}>
                <Link to="/communities">communities</Link>
              </Col>
              <Col xs={6}>
                <SearchBar />
              </Col>
              {auth.user && (
                <Col xs={1}>
                  <UploadDropdown />
                </Col>
              )}
              {auth.user && (
                <Col xs={1}>
                  <LoginDropdown
                    user={auth.user}
                    logout={logout}
                  />
                </Col>
              )}
              {!auth.user && (
                <Col xs={2}><Link to={`/login${redirectUrl}`}>login</Link></Col>
              )}
              {!auth.user && (
                <Col xs={2}>
                  <Link
                    className={classNames(
                      'button button--success',
                      styles.signup,
                    )}
                    to="/signup"
                  >
                    signup
                  </Link>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
        <MobileHeader
          auth={auth}
          isMobileMenuOpen={isMobileMenuOpen}
          isUploadMenuOpen={isUploadMenuOpen}
          toggleMobileNavbar={this.onClickMobileNavbarHandler}
          toggleUploadMenu={this.onClickUploadMenuHandler}
        />
      </header>
    )
  }
}

Header.propTypes = propTypes


export default Header
