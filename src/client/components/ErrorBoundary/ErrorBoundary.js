import { Component } from 'react'
import PropTypes from 'prop-types'


const propTypes = {
  children: PropTypes.node.isRequired,
}


class ErrorBoundary extends Component {
  // eslint-disable-next-line class-methods-use-this
  componentDidCatch(info /* , err */) {
    if (__DEV__) {
      // eslint-disable-next-line no-console
      console.info(info)
    }
  }

  render() {
    return this.props.children
  }
}

ErrorBoundary.propTypes = propTypes


export default ErrorBoundary
