import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import styles from './styles.scss'


const propTypes = {
  user: PropTypes.object.isRequired,
  large: PropTypes.bool,
}

const defaultProps = {
  large: false,
}

const UserAvatar = ({ user, large }) => (
  <img
    className={classNames(styles.avatar, { [styles.large]: large })}
    src={user.imageUrl || '/uploads/default-avatar.png'}
    alt={`avatar for ${user.username}`}
  />
)

UserAvatar.propTypes = propTypes
UserAvatar.defaultProps = defaultProps


export default UserAvatar
