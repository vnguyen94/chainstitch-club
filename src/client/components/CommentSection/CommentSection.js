import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import classNames from 'classnames'
import { Row, Col } from 'react-flexbox-grid/lib'

import { EditorText, UserLink, UserAvatar } from '../'
import Comment from '../Comment/Comment'

import styles from './styles.scss'


const propTypes = {
  comments: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  user: PropTypes.object,
}

const CommentSection = ({ user, comments, onSubmit }) => (
  <Fragment>
    <Row className={styles.comments}>
      <Col xs={12}>
        <h3 className={styles.header}>
          Comments ({comments.length})
        </h3>
      </Col>
      <Col xs={12}>
        {comments.map((comment) => (
          <Comment key={comment.id} comment={comment} />
        ))}
      </Col>
    </Row>
    <Row
      className={classNames(
        styles.containerPadding,
        styles.section,
      )}
    >
      {user ? (
        <Fragment>
          <Col xs={2}>
            <UserLink user={user}>
              <UserAvatar user={user} />
            </UserLink>
          </Col>
          <Col xs={9}>
            <EditorText onSubmit={onSubmit} />
          </Col>
        </Fragment>
      ) : (
        <Fragment>
          <Col xs={12}>log in</Col>
        </Fragment>
      )}
    </Row>
  </Fragment>
)

CommentSection.propTypes = propTypes


export default CommentSection

