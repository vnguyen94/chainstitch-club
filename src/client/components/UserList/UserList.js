import PropTypes from 'prop-types'
import React from 'react'
import { Row } from 'react-flexbox-grid/lib'

import UserLink from '../UserLink'


const propTypes = {
  users: PropTypes.array.isRequired,
}

const UserList = ({ users }) => (
  <Row>
    {users && users.map((user) => (
      <UserLink
        key={user.id}
        user={user}
      />
    ))}
  </Row>
)

UserList.propTypes = propTypes


export default UserList
