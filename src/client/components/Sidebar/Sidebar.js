import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Link } from 'react-router'

import styles from './styles.scss'


const propTypes = {
  elements: PropTypes.array.isRequired,
  activePage: PropTypes.string.isRequired,
}

const Sidebar = ({ elements, activePage }) => (
  <ul className={styles.sidebar}>
    {elements.map((element) => (
      <li
        key={element.caption}
        className={classNames({
          [styles.active]: activePage === element.active,
        })}
      >
        <Link to={element.link}>{element.caption}</Link>
      </li>
    ))}
  </ul>
)

Sidebar.propTypes = propTypes


export default Sidebar
