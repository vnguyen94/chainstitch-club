import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'

import { FollowButton } from '../buttons'
import UserLink from '../UserLink'
import UserAvatar from '../UserAvatar'

import styles from './styles.scss'


const propTypes = {
  user: PropTypes.object.isRequired,
}

const UserCard = ({ user }) => (
  <Row className={styles.card}>
    <Col xs={3}>
      <UserLink user={user}>
        <UserAvatar user={user} large />
      </UserLink>
    </Col>
    <Col xs={9} className={styles.subheader}>
      <Row start="xs">
        <Col xs={12}>
          <UserLink user={user} />
        </Col>
        <Col xs={12}>
          121 creations
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <FollowButton thin />
        </Col>
      </Row>
    </Col>
  </Row>
)

UserCard.propTypes = propTypes


export default UserCard
