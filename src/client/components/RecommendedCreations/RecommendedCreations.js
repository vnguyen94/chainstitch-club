import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'

import CreationListMinimal from '../CreationListMinimal'

import styles from './styles.scss'


const propTypes = {
  creations: PropTypes.array,
}

const defaultProps = {
  creations: [
    {
      createdAt: '2017-09-14T04:40:40.050Z',
      entityId: 14,
      fades: {
        creationId: 8,
        description: 'testes',
        id: 1,
        imageUrl: '/uploads/default-creation.png',
        title: 'test',
      },
      id: 5,
      type: 'fades',
      updatedAt: '2017-09-14T04:40:40.050Z',
      userId: 1,
      users: {
        id: 3,
        username: 'badboy365',
      },
    },
    {
      createdAt: '2017-09-14T04:40:40.050Z',
      entityId: 14,
      fades: {
        creationId: 8,
        description: 'testes',
        id: 1,
        imageUrl: '/uploads/default-creation.png',
        title: 'test',
      },
      id: 6,
      type: 'fades',
      updatedAt: '2017-09-14T04:40:40.050Z',
      userId: 1,
      users: {
        id: 3,
        username: 'badboy365',
      },
    },
    {
      createdAt: '2017-09-14T04:40:40.050Z',
      entityId: 14,
      fades: {
        creationId: 8,
        description: 'testes',
        id: 1,
        imageUrl: '/uploads/default-creation.png',
        title: 'test',
      },
      id: 7,
      type: 'fades',
      updatedAt: '2017-09-14T04:40:40.050Z',
      userId: 1,
      users: {
        id: 3,
        username: 'badboy365',
      },
    },
  ],
}

const RecommendedCreations = ({ creations }) => (
  <Row className={styles.container} center="xs">
    <Col xs={12} md={8}>
      <CreationListMinimal creations={creations} />
    </Col>
  </Row>
)

RecommendedCreations.propTypes = propTypes
RecommendedCreations.defaultProps = defaultProps


export default RecommendedCreations
