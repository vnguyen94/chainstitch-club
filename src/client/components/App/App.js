import PropTypes from 'prop-types'
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'

import config from '../../config'
import { Header, Footer, ErrorBoundary } from '../'
import { getStateAuthSelector } from '../../shared/selectors'
import {
  NAME as NAME_LOGIN,
  actions as actionsLogin,
} from '../../pages/Login'

import styles from './styles.scss'

const mapStateToProps = (state) => ({
  auth: getStateAuthSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    [NAME_LOGIN]: bindActionCreators(actionsLogin, dispatch),
  },
})

const propTypes = {
  auth: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
  children: PropTypes.object,
}

const defaultProps = {
  children: {},
}

const App = ({ children, auth, actions, router }) => {
  const { pathname } = router.location
  const onClickLogoutHandler = () => {
    actions[NAME_LOGIN].logout(router.push)
  }

  return (
    <div className={styles.app}>
      <ErrorBoundary>
        <Helmet {...config.app} />
        <Header
          auth={auth}
          logout={onClickLogoutHandler}
          pathname={pathname}
        />
        <main className={styles.main}>
          <div className={styles.childComponent}>{children}</div>
        </main>
        <Footer />
      </ErrorBoundary>
    </div>
  )
}

App.propTypes = propTypes
App.defaultProps = defaultProps


export { App }
export default connect(mapStateToProps, mapDispatchToProps)(App)
