/* eslint-disable new-cap */

import { connectedRouterRedirect } from 'redux-auth-wrapper/history3/redirect'
import { get } from 'lodash'

function checkAuthenticationRole(allowedRoles) {
  return (state) => {
    const user = get(state, ['auth', 'user'])

    if (!user) {
      return false
    }

    return allowedRoles.includes(user.role)
  }
}

const UserIsAuthenticated = connectedRouterRedirect({
  redirectPath: '/login',
  authenticatedSelector: checkAuthenticationRole(['user', 'admin']),
  wrapperDisplayName: 'UserIsAuthenticated',
})

const AdminIsAuthenticated = connectedRouterRedirect({
  redirectPath: '/login',
  authenticatedSelector: checkAuthenticationRole(['admin']),
  wrapperDisplayName: 'AdminIsAuthenticated',
})


export default {
  user: UserIsAuthenticated((props) => props.children),
  admin: AdminIsAuthenticated((props) => props.children),
}
