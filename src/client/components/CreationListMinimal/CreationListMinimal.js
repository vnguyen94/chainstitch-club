import PropTypes from 'prop-types'
import React from 'react'
import classNames from 'classnames'
import { Row, Col } from 'react-flexbox-grid/lib'

import CreationCardMinimal from '../CreationCardMinimal'

import styles from './styles.scss'


const propTypes = {
  creations: PropTypes.array.isRequired,
  rowProps: PropTypes.object,
  colStyles: PropTypes.string,
  colProps: PropTypes.object,
  creationCardProps: PropTypes.object,
}

const defaultProps = {
  rowProps: {
  },
  colStyles: '',
  colProps: {
    xs: 12,
    md: 4,
  },
}

const CreationListMinimal = ({
  creations,
  rowProps,
  colStyles,
  colProps,
}) => (
  <Row className={styles.container} {...rowProps}>
    {creations && creations.map((creation) => (
      <Col
        key={creation.id}
        className={classNames(styles.card, colStyles)}
        {...colProps}
      >
        <CreationCardMinimal creation={creation} />
      </Col>
    ))}
  </Row>
)

CreationListMinimal.propTypes = propTypes
CreationListMinimal.defaultProps = defaultProps


export default CreationListMinimal
