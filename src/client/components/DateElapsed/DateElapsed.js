import PropTypes from 'prop-types'
import React from 'react'
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now'

import styles from './styles.scss'


const propTypes = {
  time: PropTypes.string.isRequired,
  isPosting: PropTypes.bool,
}

const defaultProps = {
  isPosting: false,
}

const DateElapsed = ({ time, isPosting }) => (
  <time className={styles.time} title={time}>
    {isPosting
      ? 'posting...'
      : `${distanceInWordsToNow(time)} ago`
    }
  </time>
)

DateElapsed.propTypes = propTypes
DateElapsed.defaultProps = defaultProps


export default DateElapsed
