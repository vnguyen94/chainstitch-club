import React from 'react'
import PropTypes from 'prop-types'
import { PhotoSwipeGallery } from 'react-photoswipe'

import Thumbnail from './Thumbnail'


const propTypes = {
  options: PropTypes.object,
  thumbnailContent: PropTypes.func,
}

const defaultProps = {
  options: {
    shareEl: false,
    tapToToggleControls: false,
  },
  thumbnailContent: Thumbnail,
}

const ImageGallery = ({ options, thumbnailContent, ...rest }) => (
  <PhotoSwipeGallery
    options={options}
    thumbnailContent={thumbnailContent}
    {...rest}
  />
)

ImageGallery.propTypes = propTypes
ImageGallery.defaultProps = defaultProps


export default ImageGallery
