import React from 'react'


const Thumbnail = (image) => (
  <img
    src={image.thumbnail}
    alt={image.title}
    width={image.w}
    height={image.h}
  />
)


export default Thumbnail
