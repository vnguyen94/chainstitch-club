import PropTypes from 'prop-types'
import React from 'react'
import Dropzone from 'react-dropzone-component'


const propTypes = {
  url: PropTypes.string.isRequired,
  headers: PropTypes.object,
  className: PropTypes.string,
  onSuccessHandler: PropTypes.func,
}

const noop = () => {}

const UploadDropzone = ({
  url,
  className,
  headers = {},
  onSuccessHandler = noop,
}) => (
  <Dropzone
    className={className}
    config={{
      postUrl: url,
    }}
    djsConfig={{
      maxFilesize: 2, // in MB
      paramName: 'image',
      maxFiles: 30,
      acceptedFiles: 'image/jpeg,image/jpg,image/png,image/gif',
      dictDefaultMessage: 'drop files or click to upload',
      params: headers,
    }}
    eventHandlers={{
      success: onSuccessHandler,
    }}
  />
)

UploadDropzone.propTypes = propTypes


export default UploadDropzone
