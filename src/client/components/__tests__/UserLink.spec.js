import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import UserLink from '../UserLink'

describe('<UserLink />', () => {
  const props = {
    user: {
      id: 1,
      firstName: 'john',
      lastName: 'doe',
      username: 'johndoe94',
    },
  }

  it('renders without exploding', () => {
    const wrapper = shallow(
      <UserLink {...props}>
        testContent
      </UserLink>,
    )

    expect(wrapper.length).toEqual(1)
  })

  it('snapshot tests', () => {
    const wrapper = renderer.create(
      <UserLink {...props}>
        testContent
      </UserLink>,
    ).toJSON()

    expect(wrapper).toMatchSnapshot()
  })
})
