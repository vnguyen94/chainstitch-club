import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import CreationLink from '../CreationLink'

describe('<CreationLink />', () => {
  const props = {
    creation: {
      id: 1,
      type: 'fit',
      fit: {
        title: 'testTitle',
      },
      users: {
        username: 'testUsername',
      },
    },
  }

  it('renders without exploding', () => {
    const wrapper = shallow(
      <CreationLink {...props}>
        testContent
      </CreationLink>,
    )

    expect(wrapper.length).toEqual(1)
  })

  it('snapshot tests', () => {
    const wrapper = renderer.create(
      <CreationLink {...props}>
        testContent
      </CreationLink>,
    ).toJSON()

    expect(wrapper).toMatchSnapshot()
  })
})
