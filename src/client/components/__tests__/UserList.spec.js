import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import UserList from '../UserList'

describe('<UserList />', () => {
  const props = {
    users: [
      {
        id: 1,
        firstName: 'john',
        lastName: 'doe',
        username: 'johndoe94',
      },
    ],
  }

  it('renders without exploding', () => {
    const wrapper = shallow(
      <UserList {...props}>
        testContent
      </UserList>,
    )

    expect(wrapper.length).toEqual(1)
  })

  it('snapshot tests', () => {
    const wrapper = renderer.create(
      <UserList {...props}>
        testContent
      </UserList>,
    ).toJSON()

    expect(wrapper).toMatchSnapshot()
  })
})
