import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Dropdown, {
  DropdownTrigger,
  DropdownContent,
} from 'react-simple-dropdown'

import DropdownCaret from '../DropdownCaret'

import styles from './styles.scss'


const propTypes = {
  triggerContent: PropTypes.node.isRequired,
  items: PropTypes.array.isRequired,
  componentRender: PropTypes.func.isRequired,
  onChangeHandler: PropTypes.func.isRequired,
}

class DropdownInput extends Component {
  onClickTriggerHandler = () => {
    this.dropdownInput.focus()
  }

  bindInputRef = (input) => {
    this.dropdownInput = input
  }

  render() {
    const {
      onChangeHandler,
      triggerContent,
      items,
      componentRender,
    } = this.props

    return (
      <div className="dropdown">
        <Dropdown onShow={this.onClickTriggerHandler}>
          <DropdownTrigger className="dropdown--trigger">
            {triggerContent}
          </DropdownTrigger>
          <DropdownContent className="dropdown--content">
            <div className="dropdown--content__list">
              <div className={styles.input}>
                <input
                  ref={this.bindInputRef}
                  onChange={onChangeHandler}
                  type="text"
                />
              </div>
              <ul className={styles.list}>
                {items.map(componentRender)}
              </ul>
            </div>
            <DropdownCaret />
          </DropdownContent>
        </Dropdown>
      </div>
    )
  }
}

DropdownInput.propTypes = propTypes


export default DropdownInput
