import React from 'react'
import { Link } from 'react-router'
import { Row, Col } from 'react-flexbox-grid/lib/index'
import FacebookIcon from 'react-icons/lib/fa/facebook'
import InstagramIcon from 'react-icons/lib/fa/instagram'
import TwitterIcon from 'react-icons/lib/fa/twitter'
import ExternalLinkIcon from 'react-icons/lib/fa/external-link'

import styles from './styles.scss'


const Footer = () => (
  <footer className={styles.footer}>
    <Row className={styles.wrapper}>
      <Col xs={12} md={1}>
        <Link to="/" className={styles.title}>CSC</Link>
      </Col>
      <Col xs={12} mdOffset={1} md={2}>
        <ul className={styles.verticalRow}>
          <li><Link to="/about">about</Link></li>
          <li><Link to="/help">help</Link></li>
        </ul>
      </Col>
      <Col xs={12} md={2}>
        <ul className={styles.verticalRow}>
          <li><Link to="/api">api <ExternalLinkIcon /></Link></li>
          <li><Link to="/sitemap">sitemap</Link></li>
        </ul>
      </Col>
      <Col xs={12} mdOffset={2} md={4} className={styles.rightSection}>
        <Row center="xs">
          <ul className={styles.socialMediaIcons}>
            <li>
              <a
                href="https://facebook.com/chainstitchclub"
                target="_blank"
                rel="noopener noreferrer"
              >
                <FacebookIcon />
              </a>
            </li>
            <li>
              <a
                href="https://instagram.com/chainstitchclub"
                target="_blank"
                rel="noopener noreferrer"
              >
                <InstagramIcon />
              </a>
            </li>
            <li>
              <a
                href="https://twitter.com/chainstitchclub"
                target="_blank"
                rel="noopener noreferrer"
              >
                <TwitterIcon />
              </a>
            </li>
            <li>
              <span>van@chainstitch.club</span>
            </li>
          </ul>
        </Row>
        <Row>
          <Col xs={12} className={styles.copyright}>
            &copy; 2017 chainstitch club
          </Col>
        </Row>
      </Col>
    </Row>
  </footer>
)


export default Footer
