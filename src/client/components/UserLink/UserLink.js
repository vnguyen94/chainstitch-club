import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'
import { kebabCase } from 'lodash'


const propTypes = {
  user: PropTypes.object.isRequired,
  textContent: PropTypes.string,
  children: PropTypes.node,
}

const UserLink = ({ user, children, textContent, ...rest }) => {
  const name = user.firstName && user.lastName
    ? kebabCase(`${user.firstName} ${user.lastName}`)
    : kebabCase(user.username)
  const to = {
    pathname: `/users/${user.id}`,
    query: { name },
  }

  if (children) {
    return (
      <Link to={to} {...rest}>
        {children}
      </Link>
    )
  }

  const text = textContent || name

  return (
    <Link to={to} {...rest}>
      {text}
    </Link>
  )
}

UserLink.propTypes = propTypes


export default UserLink
