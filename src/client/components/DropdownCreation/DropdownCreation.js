import React from 'react'


const DropdownCreation = (creation) => (
  <li key={creation.id}>
    <button className="dropdown--button">
      {creation.title}
    </button>
  </li>
)


export default DropdownCreation
