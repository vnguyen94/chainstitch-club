import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import classNames from 'classnames'
import HeartOIcon from 'react-icons/lib/fa/heart-o'

import CreationLink from '../CreationLink'
import UserLink from '../UserLink'

import styles from './styles.scss'


const propTypes = {
  creation: PropTypes.object.isRequired,
}

const CreationCardMinimal = ({ creation }) => (
  <Fragment>
    <CreationLink
      className={styles.link}
      creation={creation}
    >
      <img
        className={classNames(styles.image)}

        src={creation[creation.type].imageUrl}
        alt={creation[creation.type].title}
      />
    </CreationLink>
    <UserLink
      className={classNames(styles.hoverContent, styles.cardOwner)}
      user={creation.users}
      textContent={creation.users.username}
    />
    <span
      className={classNames(styles.hoverContent, styles.likeIcon)}
      role="button"
    >
      <HeartOIcon />
    </span>
  </Fragment>
)

CreationCardMinimal.propTypes = propTypes


export default CreationCardMinimal
