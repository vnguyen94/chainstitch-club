import React from 'react'

import BaseButton from '../ButtonBase'


const FollowButton = ({ /* , userId, creationId, */ ...rest }) => (
  <BaseButton className="button--success" {...rest}>
    follow
  </BaseButton>
)


export default FollowButton
