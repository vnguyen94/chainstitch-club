import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'


const baseClasses = 'button'

const propTypes = {
  className: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  full: PropTypes.bool,
  thin: PropTypes.bool,
}

const defaultProps = {
  full: false,
  thin: false,
}

const ButtonBase = ({ full, thin, className, children, ...rest }) => (
  <button
    className={classNames(baseClasses, {
      'button--full': full,
      'button--thin': thin,
    }, className)}
    {...rest}
  >
    {children}
  </button>
)

ButtonBase.propTypes = propTypes
ButtonBase.defaultProps = defaultProps


export default ButtonBase
