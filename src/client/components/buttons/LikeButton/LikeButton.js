import React from 'react'
// import PropTypes from 'prop-types'

import BaseButton from '../ButtonBase'


const propTypes = {
  // userId: PropTypes.number.isRequired,
  // creationId: PropTypes.number.isRequired,
}

const LikeButton = ({ /* , userId, creationId, */ ...rest }) => (
  <BaseButton className="button--success" {...rest}>
    like
  </BaseButton>
)

LikeButton.propTypes = propTypes


export default LikeButton
