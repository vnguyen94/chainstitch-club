import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import DateElapsed from '../../../DateElapsed'
import UserAvatar from '../../../UserAvatar'
import UserLink from '../../../UserLink'

import styles from './styles.scss'


const propTypes = {
  creation: PropTypes.object.isRequired,
}

const User = ({ creation }) => {
  const user = creation.users

  return (
    <div className={styles.component}>
      <div className={styles.avatar}>
        <UserLink user={user}>
          <UserAvatar user={user} large />
        </UserLink>
      </div>
      <div className={styles.text}>
        <Row start="xs">
          <Col xs={12}>
            <UserLink user={user} />
          </Col>
          <Col xs={12}>
            <DateElapsed time={creation.createdAt} />
          </Col>
        </Row>
      </div>
    </div>
  )
}

User.propTypes = propTypes


export default User
