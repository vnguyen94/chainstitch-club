import React from 'react'
import PropTypes from 'prop-types'
import HeartOIcon from 'react-icons/lib/fa/heart-o'

import CreationLink from '../../../CreationLink'

import styles from './styles.scss'


const propTypes = {
  creation: PropTypes.object.isRequired,
}

const Content = ({ creation }) => {
  const typeData = creation[creation.type]

  return (
    <div className={styles.component}>
      <h3 className={styles.header}>
        <CreationLink creation={creation}>
          {typeData.title}
        </CreationLink>
        <div className={styles.userActions}>
          <HeartOIcon />
        </div>
      </h3>
      <p className={styles.description}>
        {typeData.description}
      </p>
    </div>
  )
}

Content.propTypes = propTypes


export default Content
