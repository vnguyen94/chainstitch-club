import PropTypes from 'prop-types'
import React from 'react'
import className from 'classnames'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import CreationLink from '../CreationLink'

import { User, Content } from './components'
import styles from './styles.scss'


const propTypes = {
  creation: PropTypes.object.isRequired,
  hasUserCard: PropTypes.bool,
  creationCardImageStyles: PropTypes.string,
}

const defaultProps = {
  hasUserCard: false,
  creationCardImageStyles: '',
}

const CreationCard = ({
  creation,
  hasUserCard,
  creationCardImageStyles,
}) => {
  const typeData = creation[creation.type]

  return (
    <Row className={styles.card}>
      {hasUserCard && (
        <Col xs={12}>
          <User creation={creation} />
        </Col>
      )}
      <Col xs={12}>
        <CreationLink creation={creation}>
          <img
            className={className(styles.image, creationCardImageStyles)}

            src={typeData.imageUrl}
            alt={typeData.title}
          />
        </CreationLink>
      </Col>
      <Col xs={12}>
        <Content creation={creation} />
      </Col>
    </Row>
  )
}

CreationCard.propTypes = propTypes
CreationCard.defaultProps = defaultProps


export default CreationCard
