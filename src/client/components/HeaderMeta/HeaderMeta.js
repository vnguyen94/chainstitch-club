import PropTypes from 'prop-types'
import React from 'react'
import Helmet from 'react-helmet'

import config from '../../config'


const propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
}

const HeaderMeta = ({ title, description, image }) => (
  <Helmet>
    <title>{title}</title>
    {/* Twitter */}
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content={title} />
    <meta name="twitter:description" content={description} />
    <meta name="twitter:image" content={image} />
    {/* Open Graph */}
    <meta property="og:type" content="website" />
    <meta property="og:title" content={title} />
    <meta property="og:description" content={description} />
    <meta property="og:image" content={image} />
    {/* Google+ */}
    <meta itemProp="name" content={title} />
    <meta itemProp="description" content={description} />
    <meta itemProp="image" content={image} />
  </Helmet>
)

HeaderMeta.propTypes = propTypes
HeaderMeta.defaultProps = config.meta


export default HeaderMeta
