import PropTypes from 'prop-types'
import React, { Component } from 'react'
import Textarea from 'react-textarea-autosize'


const propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

class EditorText extends Component {
  constructor() {
    super()

    this.state = {
      text: '',
      pressedKeys: {},
    }
  }

  onChangeMessageHandler = (e) => {
    this.setState({ text: e.target.value })
  }

  onKeyDownHandler = (e) => {
    const { pressedKeys } = this.state

    pressedKeys[e.keyCode] = true

    if (pressedKeys[13]) {
      if (!pressedKeys[16]) {
        e.preventDefault()
        this.postComment()
      }
    }
  }

  onKeyUpHandler = (e) => {
    const { pressedKeys } = this.state

    pressedKeys[e.keyCode] = false
  }

  postComment = () => {
    const { text } = this.state

    this.props.onSubmit(text)
    this.setState({ text: '' })
  }

  render() {
    return (
      <Textarea
        minRows={2}
        onChange={this.onChangeMessageHandler}
        onKeyDown={this.onKeyDownHandler}
        onKeyUp={this.onKeyUpHandler}
        value={this.state.text}
      />
    )
  }
}

EditorText.propTypes = propTypes


export default EditorText

