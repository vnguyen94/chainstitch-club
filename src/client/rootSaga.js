import { fork, all } from 'redux-saga/effects' // eslint-disable-line

import { sagas as exploreSagas } from './pages/Explore'
import { sagas as feedSagas } from './pages/Feed'
import { sagas as loginSagas } from './pages/Login'
import { sagas as settingsSagas } from './pages/Settings'
import { sagas as signupSagas } from './pages/Signup'
import { sagas as userProfileSagas } from './pages/UserProfile'
import { sagas as uploadCreationSagas } from './pages/UploadCreation'
import { sagas as communitiesDetailSagas } from './pages/CommunitiesDetail'
import { sagas as communitiesListSagas } from './pages/CommunitiesList'
import { sagas as creationsDashboardSagas } from './pages/CreationsDashboard'
import { sagas as creationsDetailSagas } from './pages/CreationsDetail'
import { sagas as creationsListSagas } from './pages/CreationsList'


export default function* rootSaga() {
  yield all([
    fork(exploreSagas.watchAttemptFetchExploreResults),
    fork(feedSagas.watchAttemptFetchGlobalFeed),
    fork(loginSagas.watchAttemptLogin),
    fork(loginSagas.watchLogout),
    fork(signupSagas.watchAttemptSignup),
    fork(userProfileSagas.watchAttemptFetchUser),
    fork(userProfileSagas.watchAttemptFetchUserCreations),
    fork(userProfileSagas.watchAttemptPostComment),
    fork(userProfileSagas.watchAttemptFetchFollowersOrFollowing),
    fork(userProfileSagas.watchAttemptFetchCommentsUserProfile),
    fork(settingsSagas.watchAttemptUpdateUserSettings),
    fork(settingsSagas.watchAttemptUpdateProfileSettings),
    fork(settingsSagas.watchAttemptSettingsDetail),
    fork(settingsSagas.watchAttemptUpdateNotifications),
    fork(settingsSagas.watchAttemptSendEmailResetForm),
    fork(settingsSagas.watchAttemptDeleteAccount),
    fork(communitiesDetailSagas.watchAttemptFetchCommunitiesDetail),
    fork(communitiesListSagas.watchAttemptFetchCommunitiesList),
    fork(uploadCreationSagas.watchSubmitCreationForm),
    fork(creationsDashboardSagas.watchSubmitCreationForm),
    fork(creationsDashboardSagas.watchHandleUploadImage),
    fork(creationsDashboardSagas.watchFetchCreation),
    fork(creationsDashboardSagas.watchFetchImages),
    fork(creationsDashboardSagas.watchFetchCommunitiesForDropdown),
    fork(creationsDetailSagas.watchAttemptFetchCreationsDetail),
    fork(creationsDetailSagas.watchAttemptPostComment),
    fork(creationsDetailSagas.watchAttemptFetchComments),
    fork(creationsDetailSagas.watchAttemptFetchCollections),
    fork(creationsListSagas.watchAttemptFetchCreationsList),
  ])
}
