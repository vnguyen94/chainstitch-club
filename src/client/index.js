import React from 'react'
import { render, unmountComponentAtNode } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import { AppContainer } from 'react-hot-loader'

import { loginSuccess } from './pages/Login/sagas'
import configureStore from './store'
import routes from './routes'
import config from './config'

import './styles/app.scss'

// eslint-disable-next-line no-underscore-dangle
const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)
const app = document.getElementById('app')
const localJWT = localStorage.getItem(config.app.JWT_TOKEN_NAME)

if (localJWT) {
  try {
    const { user, token } = JSON.parse(localJWT)

    store.dispatch(loginSuccess(user, token))
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error('could not parse JWT.')
  }
}

const renderApp = (appRoutes) => (
  render(
    <AppContainer>
      <Provider store={store}>
        <Router history={history} routes={appRoutes} />
      </Provider>
    </AppContainer>,
    app,
  )
)

renderApp(routes)

if (__DEV__ && module.hot) {
  module.hot.accept('./routes', () => {
    unmountComponentAtNode(app)

    // eslint-disable-next-line global-require
    const newRoutes = require('./routes').default
    renderApp(newRoutes)
  })
}
