import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta } from '../../components'

import styles from './styles.scss'


const ErrorNotFound = () => (
  <Row center="xs" className={styles.page}>
    <Col xs={12}>
      <HeaderMeta title="404" />
      <p>404 error not found</p>
    </Col>
  </Row>
)


export default ErrorNotFound
