import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'

import { getStateEntitiesSelector } from '../../shared/selectors'
import {
  FadesSchema,
  FitsSchema,
  CommunitiesSchema,
  ImagesSchema,
} from '../../../schemas'

import { NAME } from './ducks'

const schemas = {
  fade: FadesSchema,
  fit: FitsSchema,
}

export const getStateCreationsDashboardSelector = (state) => state[NAME]
export const getCreationTypeSelector =
  (state, ownProps) => ownProps.route.creationType
export const getCreeationIdSelector =
  (state, ownProps) => ownProps.params.creationId

export const denormalizeSingleCreation = (result, entities, creationType) => (
  denormalize(
    result,
    schemas[creationType].single,
    entities,
  )
)
export const getEditCreationSelector = createSelector(
  [getStateCreationsDashboardSelector, getStateEntitiesSelector,
    getCreationTypeSelector],
  (state, entities, creationType) => {
    if (!state.creationForEdit || !entities.creations) {
      return {}
    }

    const creation = denormalizeSingleCreation(
      state.creationForEdit,
      entities,
      creationType,
    )

    return creation
  },
)

export const getCreationImagesSelector = createSelector(
  [getStateCreationsDashboardSelector, getStateEntitiesSelector],
  (state, entities) => {
    if (!state.images || !entities.images) {
      return []
    }

    const images = denormalize(
      state.images,
      ImagesSchema.list,
      entities,
    )

    return images
  },
)

export const getCommunitiesSelectSelector = createSelector(
  [getStateCreationsDashboardSelector, getStateEntitiesSelector],
  (state, entities) => {
    if (!state.communitiesSelectResults || !entities.communities) {
      return []
    }

    const communities = denormalize(
      state.communitiesSelectResults,
      CommunitiesSchema.communitiesList,
      entities,
    ).map((community) => ({
      value: community.creation_id,
      label: community.title,
    }))

    return communities
  },
)

const CreationNavigationLists = {
  fade: [
    {
      urlFragment: '',
      caption: 'general',
      active: 'general',
    },
    {
      urlFragment: 'communities',
      caption: 'communities',
      active: 'communities',
    },
    {
      urlFragment: 'timeline',
      caption: 'timeline',
      active: 'timeline',
    },
    {
      urlFragment: 'privacy',
      caption: 'privacy',
      active: 'privacy',
    },
  ],
  fit: [
    {
      urlFragment: '',
      caption: 'general',
      active: 'general',
    },
    {
      urlFragment: 'communities',
      caption: 'communities',
      active: 'communities',
    },
    {
      urlFragment: 'privacy',
      caption: 'privacy',
      active: 'privacy',
    },
  ],
}

export const getNavigationListSelector = createSelector(
  [getCreeationIdSelector, getCreationTypeSelector],
  (creationId, creationType) => (
    CreationNavigationLists[creationType].map((link) => ({
      ...link,
      link: `/${creationType}s/${creationId}/edit/${link.urlFragment}`,
    }))
  ),
)

export const getInitialFormValuesSelector = createSelector(
  [getEditCreationSelector],
  (creation) => creation[creation.type] || {},
)

export const getErrSelector = createSelector(
  [getStateCreationsDashboardSelector],
  (state) => state.err,
)
