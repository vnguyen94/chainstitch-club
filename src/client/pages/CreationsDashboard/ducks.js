import { createConstants, createReducer } from '../../../utils'


export const NAME = 'CreationsDashboard'

export const types = createConstants(NAME, [
  'COMMUNITY_FORM',
  'FADE_GENERAL_FORM',
  'FIT_GENERAL_FORM',
  'PRIVACY_FORM',
  'FADE_TIMELINE_FORM',
  'CREATION_FORM_REQUEST',
  'CREATION_FORM_SUCCESS',
  'CREATION_FORM_FAILURE',
  'HANDLE_UPLOAD_IMAGE',
  'UPLOAD_IMAGE_SUCCESS',
  'FETCH_CREATION_REQUEST',
  'FETCH_CREATION_SUCCESS',
  'FETCH_CREATION_FAILURE',
  'FETCH_IMAGES_REQUEST',
  'FETCH_IMAGES_SUCCESS',
  'FETCH_IMAGES_FAILURE',
  'FETCH_COMMUNITIES_FOR_DROPDOWN_REQUEST',
  'FETCH_COMMUNITIES_FOR_DROPDOWN_SUCCESS',
  'FETCH_COMMUNITIES_FOR_DROPDOWN_FAILURE',
  'SELECT_COMMUNITY_FROM_SELECT',
])

export const initialState = {
  isSubmitting: false,
  isFetchingCreation: false,
  isFetchingImages: false,
  isFetchingCommunities: false,
  creationForEdit: null,
  images: [],
  communitiesSelectResults: [],
  communitiesSelectSelectedValue: null,
  err: null,
}

export const actions = {
  submitForm: (payload, creationType, authHeaders, navigateToAndReplace) => ({
    type: types.CREATION_FORM_REQUEST,
    payload,
    meta: { authHeaders, creationType, navigateToAndReplace },
  }),
  handleUploadImage: (file, response) => ({
    type: types.HANDLE_UPLOAD_IMAGE,
    payload: response,
  }),
  fetchCreation: (meta) => ({
    type: types.FETCH_CREATION_REQUEST,
    meta,
  }),
  fetchImages: (meta) => ({
    type: types.FETCH_IMAGES_REQUEST,
    meta,
  }),
  fetchCommunitiesForDropdown: (query) => ({
    type: types.FETCH_COMMUNITIES_FOR_DROPDOWN_REQUEST,
    payload: query,
  }),
  selectCommunityFromSelect: (value) => ({
    type: types.SELECT_COMMUNITY_FROM_SELECT,
    payload: value,
  }),
}

export default createReducer(initialState, {
  [types.CREATION_FORM_REQUEST]: (state) => ({
    ...state,
    isSubmitting: true,
    err: null,
  }),
  [types.CREATION_FORM_SUCCESS]: (state) => ({
    ...state,
    isSubmitting: false,
    err: null,
  }),
  [types.CREATION_FORM_FAILURE]: (state, { payload }) => ({
    ...state,
    isSubmitting: false,
    err: payload,
  }),
  [types.UPLOAD_IMAGE_SUCCESS]: (state, { payload }) => ({
    ...state,
    images: state.images.concat(payload.image),
  }),
  [types.FETCH_CREATION_REQUEST]: (state) => ({
    ...state,
    isFetchingCreation: true,
    creationForEdit: null,
    err: null,
  }),
  [types.FETCH_CREATION_SUCCESS]: (state, { payload }) => ({
    ...state,
    isFetchingCreation: false,
    creationForEdit: payload.creation,
    err: null,
  }),
  [types.FETCH_CREATION_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetchingCreation: false,
    err: payload,
  }),
  [types.FETCH_IMAGES_REQUEST]: (state) => ({
    ...state,
    isFetchingImages: true,
    creationForEdit: null,
    err: null,
  }),
  [types.FETCH_IMAGES_SUCCESS]: (state, { payload }) => ({
    ...state,
    isFetchingImages: false,
    images: payload.images,
    err: null,
  }),
  [types.FETCH_IMAGES_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetchingImages: false,
    err: payload,
  }),
  [types.FETCH_COMMUNITIES_FOR_DROPDOWN_REQUEST]: (state) => ({
    ...state,
    isFetchingCommunities: true,
    err: null,
  }),
  [types.FETCH_COMMUNITIES_FOR_DROPDOWN_SUCCESS]: (state, { payload }) => ({
    ...state,
    isFetchingCommunities: false,
    communitiesSelectResults: payload.communities,
    err: null,
  }),
  [types.FETCH_COMMUNITIES_FOR_DROPDOWN_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetchingCommunities: false,
    err: payload,
  }),
  [types.SELECT_COMMUNITY_FROM_SELECT]: (state, { payload }) => ({
    ...state,
    communitiesSelectSelectedValue: payload,
  }),
})
