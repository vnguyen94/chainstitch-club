import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'
import Joi from 'joi'

import { validateClientForm } from '../../../../utils'
import { UploadDropzone } from '../../../components'
import { CreationForms } from '../../../forms'
import { types } from '../ducks'

import CreationImages from './CreationImages'


const formSchema = Joi.object().keys({
  title: Joi.string().required().label('title'),
  description: Joi.string().required().label('description'),
})

const propTypes = {
  onSubmit: PropTypes.func.isRequired,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
  images: PropTypes.array.isRequired,
  handleUploadImage: PropTypes.func.isRequired,
  creationId: PropTypes.string.isRequired,
  creationType: PropTypes.string.isRequired,
  authHeaders: PropTypes.object.isRequired,
}

const FadeGeneralForm = ({
  handleSubmit,
  onSubmit,
  err,
  valid,
  creationId,
  creationType,
  images,
  handleUploadImage,
  authHeaders,
}) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    <UploadDropzone
      url={`/api/${creationType}s/${creationId}/images`}
      headers={authHeaders}
      onSuccessHandler={handleUploadImage}
    />
    <CreationImages images={images} />
    <CreationForms.FadeSection />
    {err && <p>{err}</p>}
    <button
      className="button button--success"
      disabled={!valid}
      type="submit"
    >
      create
    </button>
  </Form>
)

FadeGeneralForm.propTypes = propTypes


export default reduxForm({
  form: types.FADE_GENERAL_FORM,
  fields: ['title', 'description'],
  validate: validateClientForm(formSchema),
  enableReinitialize: true,
})(FadeGeneralForm)
