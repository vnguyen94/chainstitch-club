import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'
import Joi from 'joi'

import { validateClientForm } from '../../../../utils'
import { CreationForms } from '../../../forms'
import { types } from '../ducks'


const formSchema = Joi.object().keys({
  title: Joi.string().required().label('title'),
  description: Joi.string().required().label('description'),
})

const propTypes = {
  onSubmit: PropTypes.func.isRequired,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
}

const FitGeneralForm = ({ handleSubmit, onSubmit, err, valid }) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    <CreationForms.FitSection />
    {err && <p>{err}</p>}
    <button
      className="button button--success"
      disabled={!valid}
      type="submit"
    >
      create
    </button>
  </Form>
)

FitGeneralForm.propTypes = propTypes


export default reduxForm({
  form: types.FIT_GENERAL_FORM,
  fields: ['title', 'description'],
  validate: validateClientForm(formSchema),
  enableReinitialize: true,
})(FitGeneralForm)
