import CommunityForm from './CommunityForm'
import FadeTimelineForm from './FadeTimelineForm'
import FadeGeneralForm from './FadeGeneralForm'
import FitGeneralForm from './FitGeneralForm'
import PrivacyForm from './PrivacyForm'


export {
  CommunityForm,
  FadeTimelineForm,
  FadeGeneralForm,
  FitGeneralForm,
  PrivacyForm,
}
