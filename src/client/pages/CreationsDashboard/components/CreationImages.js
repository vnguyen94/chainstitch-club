import PropTypes from 'prop-types'
import React from 'react'

import styles from './styles.scss'


const propTypes = {
  images: PropTypes.array.isRequired,
}

const CreationImages = ({ images }) => (
  <ul className={styles.imageList}>
    {images.length && images.map((image) => (
      <li key={image.id}>
        <img
          src={image.url}
          alt="fadeImage"
          className={styles.image}
        />
      </li>
    ))}
  </ul>
)

CreationImages.propTypes = propTypes


export default CreationImages
