import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'
// import Joi from 'joi'

// import { validateClientForm } from '../../../../utils'
// import { CreationForms } from '../../../forms'
import { types } from '../ducks'


// const formSchema = Joi.object().keys({
//   title: Joi.string().required().label('title'),
//   body: Joi.string().required().label('body'),
// })

const propTypes = {
  // onChangeEditor: PropTypes.func.isRequired,
  // editorValue: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
}

const PrivacyForm = ({
  handleSubmit,
  onSubmit,
  err,
  valid,
  // onChangeEditor,
  // editorValue,
}) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    privacy form
    {err && <p>{err}</p>}
    <button
      className="button button--success"
      disabled={!valid}
      type="submit"
    >
      save
    </button>
  </Form>
)

PrivacyForm.propTypes = propTypes


export default reduxForm({
  form: types.PRIVACY_FORM,
  fields: [],
  // validate: validateClientForm(formSchema),
  enableReinitialize: true,
})(PrivacyForm)
