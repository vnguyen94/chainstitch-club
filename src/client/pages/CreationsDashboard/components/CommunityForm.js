import React from 'react'
import { Form, reduxForm } from 'redux-form'
import PropTypes from 'prop-types'
import Joi from 'joi'

import { validateClientForm } from '../../../../utils'
import { SelectAsyncField } from '../../../forms/components'
import { types } from '../ducks'


const formSchema = Joi.object().keys({
  date: Joi.date().required().label('title'),
})


const propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onChangeSelect: PropTypes.func.isRequired,
  onInputChangeSelect: PropTypes.func.isRequired,
  communities: PropTypes.array.isRequired,
  isFetchingCommunities: PropTypes.bool.isRequired,
  selectedValue: PropTypes.number,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
}

const CommunityForm = ({
  handleSubmit,
  onSubmit,
  err,
  valid,

  onChangeSelect,
  onInputChangeSelect,
  communities,
  selectedValue,
  isFetchingCommunities,
}) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    <SelectAsyncField
      name="communities"
      elements={communities}
      config={{
        isLoading: isFetchingCommunities,
        onChange: onChangeSelect,
        onInputChange: onInputChangeSelect,
        value: selectedValue,
      }}
    />
    {err && <p>{err}</p>}
    <button
      className="button button--success"
      disabled={!valid}
      type="submit"
    >
      save
    </button>
  </Form>
)

CommunityForm.propTypes = propTypes


export default reduxForm({
  form: types.COMMUNITY_FORM,
  fields: ['communities'],
  validate: validateClientForm(formSchema),
  enableReinitialize: true,
})(CommunityForm)
