import React from 'react'
import { Form, reduxForm } from 'redux-form'
import PropTypes from 'prop-types'
import Joi from 'joi'

import { validateClientForm } from '../../../../utils'
import { DateField } from '../../../forms/components'
import { types } from '../ducks'


const formSchema = Joi.object().keys({
  date: Joi.date().required().label('title'),
})


const propTypes = {
  onSubmit: PropTypes.func.isRequired,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
}

const FadeTimelineForm = ({
  handleSubmit,
  onSubmit,
  err,
  valid,
}) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    <DateField
      name="date"
      caption="date"
    />
    {err && <p>{err}</p>}
    <button
      className="button button--success"
      disabled={!valid}
      type="submit"
    >
      save
    </button>
  </Form>
)

FadeTimelineForm.propTypes = propTypes


export default reduxForm({
  form: types.FADE_TIMELINE_FORM,
  fields: ['date'],
  validate: validateClientForm(formSchema),
  enableReinitialize: true,
})(FadeTimelineForm)
