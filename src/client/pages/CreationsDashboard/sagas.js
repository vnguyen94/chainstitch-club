import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'
import { insertEntity } from '../../shared/entities'

import { types } from './ducks'
import { denormalizeSingleCreation } from './selectors'


function submitCreationSuccess() {
  return {
    type: types.CREATION_FORM_SUCCESS,
  }
}

function submitCreationFailure(err) {
  return {
    type: types.CREATION_FORM_FAILURE,
    payload: err,
    error: true,
  }
}

function* submitCreationRequest({ payload, meta }) {
  try {
    const apiRoute = `/${meta.creationType}s`

    const request = yield call(
      API.post,
      apiRoute,
      payload,
      meta.authHeaders,
    )
    const { redirectUrl } = request.data.data

    yield put(submitCreationSuccess())
    meta.navigateTo(redirectUrl)
  } catch (err) {
    yield put(submitCreationFailure(err.response.data.message))
  }
}

function* watchSubmitCreationForm() {
  yield takeLatest(types.CREATION_FORM_REQUEST, submitCreationRequest)
}

function uploadImageSuccess(image) {
  return {
    type: types.UPLOAD_IMAGE_SUCCESS,
    payload: {
      image,
    },
  }
}

function* handleUploadImage({ payload }) {
  const { result, entities } = payload.results

  yield all([
    put(insertEntity(entities)),
    put(uploadImageSuccess(result)),
  ])
}

function* watchHandleUploadImage() {
  yield takeLatest(types.HANDLE_UPLOAD_IMAGE, handleUploadImage)
}

function fetchCreationSuccess(creation) {
  return {
    type: types.FETCH_CREATION_SUCCESS,
    payload: {
      creation,
    },
  }
}

function fetchCreationFailure(err) {
  return {
    type: types.FETCH_CREATION_FAILURE,
    payload: err,
    error: true,
  }
}

function* fetchCreationRequest({ meta }) {
  try {
    const pluralCreation = `${meta.creationType}s`
    const apiRoute = `/${pluralCreation}/${meta.creationId}`
    const request = yield call(API.get, apiRoute)
    const { result, entities } = request.data.data
    const creation = denormalizeSingleCreation(
      result,
      entities,
      meta.creationType,
    )

    // non-owners should not be able to edit
    if (
      creation.users.id !== meta.currentUser.id ||
      creation.type !== pluralCreation
    ) {
      meta.navigateToAndReplace('/403')
      return
    }

    yield all([
      put(insertEntity(entities)),
      put(fetchCreationSuccess(result)),
    ])
  } catch (err) {
    yield put(fetchCreationFailure(err.response.data.message))
  }
}

function* watchFetchCreation() {
  yield takeLatest(types.FETCH_CREATION_REQUEST, fetchCreationRequest)
}

function fetchImagesSuccess(images) {
  return {
    type: types.FETCH_IMAGES_SUCCESS,
    payload: {
      images,
    },
  }
}

function fetchImagesFailure(err) {
  return {
    type: types.FETCH_IMAGES_FAILURE,
    payload: err,
    error: true,
  }
}

function* fetchImagesRequest({ meta }) {
  try {
    const pluralCreation = `${meta.creationType}s`
    const apiRoute = `/${pluralCreation}/${meta.creationId}/images`
    const request = yield call(API.get, apiRoute)
    const { result, entities } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchImagesSuccess(result)),
    ])
  } catch (err) {
    yield put(fetchImagesFailure(err.response.data.message))
  }
}

function* watchFetchImages() {
  yield takeLatest(types.FETCH_IMAGES_REQUEST, fetchImagesRequest)
}

function fetchCommunitiesForDropdownSuccess(communities) {
  return {
    type: types.FETCH_COMMUNITIES_FOR_DROPDOWN_SUCCESS,
    payload: {
      communities,
    },
  }
}

function fetchCommunitiesForDropdownFailure(err) {
  return {
    type: types.FETCH_COMMUNITIES_FOR_DROPDOWN_FAILURE,
    payload: err,
    error: true,
  }
}

function* fetchCommunitiesForDropdownRequest({ payload }) {
  try {
    const apiRoute = `/communities/search?query=${payload}`
    const request = yield call(API.get, apiRoute)
    const { result, entities } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCommunitiesForDropdownSuccess(result)),
    ])
  } catch (err) {
    yield put(fetchCommunitiesForDropdownFailure(err.response.data.message))
  }
}

function* watchFetchCommunitiesForDropdown() {
  yield takeLatest(
    types.FETCH_COMMUNITIES_FOR_DROPDOWN_REQUEST,
    fetchCommunitiesForDropdownRequest,
  )
}


export default {
  watchSubmitCreationForm,
  watchHandleUploadImage,
  watchFetchCreation,
  watchFetchImages,
  watchFetchCommunitiesForDropdown,
}
