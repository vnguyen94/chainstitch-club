import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'
import { Link } from 'react-router'
import CaretLeftIcon from 'react-icons/lib/fa/caret-left'

import { HeaderMeta, Sidebar } from '../../components'
import {
  getAuthHeadersSelector,
  getCurrentUserSelector,
} from '../../shared/selectors'

import {
  CommunityForm,
  FadeGeneralForm,
  FadeTimelineForm,
  FitGeneralForm,
  PrivacyForm,
} from './components'
import {
  getStateCreationsDashboardSelector,
  getNavigationListSelector,
  getErrSelector,
  getCreationImagesSelector,
  getCommunitiesSelectSelector,
  getInitialFormValuesSelector,
} from './selectors'
import { actions as CreationsDashboardActions } from './ducks'
import styles from './styles.scss'

const CreationForms = {
  fade: {
    general: FadeGeneralForm,
    communities: CommunityForm,
    timeline: FadeTimelineForm,
    privacy: PrivacyForm,
  },
  fit: {
    general: FitGeneralForm,
    communities: CommunityForm,
    privacy: PrivacyForm,
  },
}

const propTypes = {
  creationsDashboard: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
  authHeaders: PropTypes.object.isRequired,
  images: PropTypes.array.isRequired,
  communitiesSelectResults: PropTypes.array.isRequired,
  navigationList: PropTypes.array.isRequired,
  initialFormValues: PropTypes.object.isRequired,
  currentUser: PropTypes.object.isRequired,
}

const mapStateToProps = (state, props) => ({
  creationsDashboard: getStateCreationsDashboardSelector(state),
  communitiesSelectResults: getCommunitiesSelectSelector(state),
  err: getErrSelector(state),
  currentUser: getCurrentUserSelector(state),
  images: getCreationImagesSelector(state),
  authHeaders: getAuthHeadersSelector(state),
  navigationList: getNavigationListSelector(state, props),
  initialFormValues: getInitialFormValuesSelector(state, props),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    creationsDashboard: bindActionCreators(CreationsDashboardActions, dispatch),
  },
})

class CreationsDashboard extends Component {
  componentDidMount() {
    const {
      route: { creationType, activePage },
      params: { creationId },
      actions,
      currentUser,
      router,
    } = this.props

    actions.creationsDashboard.fetchCreation({
      creationType,
      creationId,
      currentUser,
      navigateToAndReplace: router.replace,
    })
    if (activePage === 'general') {
      actions.creationsDashboard.fetchImages({
        creationId,
        creationType,
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      route: { creationType, activePage },
      params: { creationId },
      actions,
    } = this.props

    if (
      activePage !== nextProps.route.activePage &&
      nextProps.route.activePage === 'general'
    ) {
      actions.creationsDashboard.fetchImages({
        creationId,
        creationType,
      })
    }
  }

  onChangeCommunitiesSelectHandler = (community) => {
    const { actions } = this.props
    const newValue = community ? community.value : null

    actions.creationsDashboard.selectCommunityFromSelect(newValue)
  }

  onInputChangeCommunitiesSelectHandler = (query) => {
    const { actions } = this.props

    if (!query) {
      return
    }

    actions.creationsDashboard.fetchCommunitiesForDropdown(query)
  }

  onSubmitHandler = (payload) => {
    const {
      route: { creationType },
      actions,
      router,
      authHeaders,
    } = this.props

    actions.creationsDashboard.submitForm(
      payload,
      creationType,
      authHeaders,
      router.push,
    )
  }

  render() {
    const {
      route: { creationType, activePage },
      params: { creationId },
      creationsDashboard: {
        isFetchingCommunities,
        communitiesSelectSelectedValue,
      },
      communitiesSelectResults,
      navigationList,
      initialFormValues,
      images,
      actions,
      authHeaders,
    } = this.props
    const CreationForm = CreationForms[creationType][activePage]
    const formProps = {
      onSubmit: this.onSubmitHandler,
      initialValues: initialFormValues,
      handleUploadImage: actions.creationsDashboard.handleUploadImage,
      creationType,
      creationId,
      authHeaders,
      images,
    }

    if (activePage === 'communities') {
      formProps.onChangeSelect = this.onChangeCommunitiesSelectHandler
      formProps.onInputChangeSelect = this.onInputChangeCommunitiesSelectHandler
      formProps.isFetchingCommunities = isFetchingCommunities
      formProps.communities = communitiesSelectResults
      formProps.selectedValue = communitiesSelectSelectedValue
    }

    return (
      <Fragment>
        <HeaderMeta title={`upload ${creationType}`} />
        <Row center="xs" className={styles.page}>
          <Col xs={12}>
            <Row>
              <Col xs={12} md={4}>
                <Row className={styles.backLink}>
                  <Link to={`/${creationType}s/${creationId}`}>
                    <CaretLeftIcon />
                    <span> back</span>
                  </Link>
                </Row>
                <Row>
                  <Sidebar
                    activePage={activePage}
                    elements={navigationList}
                  />
                </Row>
              </Col>
              <Col xs={12} md={8}>
                <CreationForm {...formProps} />
              </Col>
            </Row>
          </Col>
        </Row>
      </Fragment>
    )
  }
}

CreationsDashboard.propTypes = propTypes


export { CreationsDashboard }
export default connect(mapStateToProps, mapDispatchToProps)(CreationsDashboard)
