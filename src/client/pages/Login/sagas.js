import { put, call, takeLatest } from 'redux-saga/effects' //eslint-disable-line

import { API } from '../../../utils'
import config from '../../config'

import { types } from './ducks'


export function loginSuccess(user, token) {
  return {
    type: types.LOGIN_SUCCESS,
    payload: {
      user,
      token,
    },
  }
}

function loginFailure(err) {
  return {
    type: types.LOGIN_FAILURE,
    payload: err,
    error: true,
  }
}

function* loginRequest({ payload, meta }) {
  try {
    const request = yield call(API.post, '/auth/login', payload)
    const { user, token } = request.data.data
    const redirectUrl = meta.query.redirect || '/'

    yield put(loginSuccess(user, token))

    if (meta.rememberMe) {
      localStorage.setItem(
        config.app.JWT_TOKEN_NAME,
        JSON.stringify(request.data.data),
      )
    }

    meta.navigateTo(redirectUrl)
  } catch (err) {
    yield put(loginFailure(err.response.data.message))
  }
}

function* watchAttemptLogin() {
  yield takeLatest(types.LOGIN_REQUEST, loginRequest)
}

function logoutRequest({ meta }) {
  localStorage.removeItem(config.app.JWT_TOKEN_NAME)
  meta.navigateTo('/')
}

function* watchLogout() {
  yield takeLatest(types.LOGOUT, logoutRequest)
}


export default {
  watchAttemptLogin,
  watchLogout,
}
