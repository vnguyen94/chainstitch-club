import PropTypes from 'prop-types'
import React, { Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'
import { Link } from 'react-router'

import { getStateAuthSelector } from '../../shared/selectors'
import { HeaderMeta } from '../../components'

import { getErrSelector } from './selectors'
import { actions } from './ducks'
import { LoginForm } from './components'
import styles from './styles.scss'

const propTypes = {
  actions: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  err: PropTypes.string,
}

const mapStateToProps = (state) => ({
  auth: getStateAuthSelector(state),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    login: bindActionCreators(actions, dispatch),
  },
})

const Login = (props) => {
  const { router, location } = props
  const onLoginHandler = ({ rememberMe, ...payload }) => {
    props.actions.login.login(
      payload,
      {
        navigateTo: router.push,
        query: location.query,
        rememberMe,
      },
    )
  }

  return (
    <Fragment>
      <HeaderMeta
        title="login"
        description="login to chainstitch club and start sharing denim."
      />
      <Row center="xs">
        <Col xs={12} className={styles.page}>
          <div className={styles.formContainer}>
            <div className={styles.subHeader}>
              <h3>log in</h3>
              <h4>a place for denim and clothes</h4>
              <h4><Link to="/signup">or sign up here</Link></h4>
            </div>
            <LoginForm
              err={props.err}
              login={onLoginHandler}
            />
          </div>
        </Col>
      </Row>
    </Fragment>
  )
}

Login.propTypes = propTypes


export { Login }
export default connect(mapStateToProps, mapDispatchToProps)(Login)
