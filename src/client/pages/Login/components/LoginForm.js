import PropTypes from 'prop-types'
import React from 'react'
import className from 'classnames'
import { Form, reduxForm } from 'redux-form'

import { UserForms } from '../../../forms'
import { CheckboxField } from '../../../forms/components'
import { types } from '../ducks'

import styles from './styles.scss'


const LoginForm = ({ handleSubmit, login, err, valid }) => (
  <Form onSubmit={handleSubmit(login)}>
    <UserForms.UserSection />
    <CheckboxField
      name="rememberMe"
      caption="remember me"
    />
    {err && <p>{err}</p>}
    <button
      disabled={!valid}
      type="submit"
      className={className('button', 'button--success', styles.button)}
    >
      login
    </button>
  </Form>
)

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
}


export default reduxForm({
  form: types.LOGIN_FORM,
  fields: ['emailOrUsername', 'password', 'rememberMe'],
})(LoginForm)
