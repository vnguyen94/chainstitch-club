import { createConstants, createReducer } from '../../../utils'


export const NAME = 'auth'

export const types = createConstants(NAME, [
  'LOGIN_REQUEST',
  'LOGIN_SUCCESS',
  'LOGIN_FAILURE',
  'LOGOUT',
  'LOGIN_FORM',
])

export const initialState = {
  user: null,
  token: null,
  isFetching: false,
  err: null,
}

export const actions = {
  login: (payload, meta) => ({
    type: types.LOGIN_REQUEST,
    payload,
    meta,
  }),
  logout: (navigateTo) => ({
    type: types.LOGOUT,
    meta: { navigateTo },
  }),
}

export default createReducer(initialState, {
  [types.LOGIN_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.LOGIN_SUCCESS]: (state, { payload }) => ({
    ...state,
    user: payload.user,
    token: payload.token,
    isFetching: false,
    err: null,
  }),
  [types.LOGIN_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
  [types.LOGOUT]: (state) => ({
    ...state,
    user: null,
    token: null,
  }),
})
