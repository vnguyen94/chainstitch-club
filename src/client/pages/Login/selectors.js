import { createSelector } from 'reselect'

import { NAME } from './ducks'


export const getStateLoginSelector = (state) => state[NAME]

export const getErrSelector = createSelector(
  [getStateLoginSelector],
  (loginState) => loginState.err,
)
