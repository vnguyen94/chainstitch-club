import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta, Sidebar } from '../../components'

import {
  AccountHelp,
  GeneralHelp,
  ModerationHelp,
  PostingHelp,
} from './components'
import styles from './styles.scss'


const HelpComponents = {
  account: AccountHelp,
  general: GeneralHelp,
  moderation: ModerationHelp,
  posting: PostingHelp,
}

const sidebarElements = [
  {
    caption: 'general',
    link: '/help',
    active: 'general',
  },
  {
    caption: 'account',
    link: '/help/account',
    active: 'account',
  },
  {
    caption: 'posting',
    link: '/help/posting',
    active: 'posting',
  },
  {
    caption: 'rules and moderation',
    link: '/help/moderation',
    active: 'moderation',
  },
]

const propTypes = {
  route: PropTypes.object.isRequired,
}

const Help = (props) => {
  const {
    route: { activePage },
  } = props
  const HelpComponent = HelpComponents[activePage]

  return (
    <Row center="xs" className={styles.page}>
      <Col xs={12}>
        <HeaderMeta title="settings" />
        <Row>
          <Col xs={12} md={4}>
            <Sidebar
              activePage={activePage}
              elements={sidebarElements}
            />
          </Col>
          <Col xs={12} md={8}>
            <HelpComponent />
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

Help.propTypes = propTypes


export default Help
