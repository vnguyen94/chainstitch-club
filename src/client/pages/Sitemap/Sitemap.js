import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'
import { Link } from 'react-router'

import { HeaderMeta } from '../../components'

import styles from './styles.scss'


const Sitemap = () => (
  <Row className={styles.page} center="xs" start="md">
    <Col xs={12}>
      <HeaderMeta title="sitemap" />
      <ul>
        <li>
          explore
          <ul>
            <li>
              <Link to="/">popular</Link>
            </li>
            <li>
              <Link to="/featured">featured</Link>
            </li>
            <li>
              <Link to="/new">new</Link>
            </li>
          </ul>
        </li>
        <li>
          <Link to="/search">search</Link>
        </li>
        <li>
          <Link to="/about">about</Link>
        </li>
        <li>
          help
          <ul>
            <li>
              <Link to="/help">general</Link>
            </li>
            <li>
              <Link to="/help/account">account</Link>
            </li>
            <li>
              <Link to="/help/posting">posting</Link>
            </li>
            <li>
              <Link to="/help/moderation">rules and moderation</Link>
            </li>
          </ul>
        </li>
        <li>
          <Link to="/api-docs">api</Link>
        </li>
      </ul>
    </Col>
  </Row>
)


export default Sitemap
