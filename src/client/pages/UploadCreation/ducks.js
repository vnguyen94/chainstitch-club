import { createConstants, createReducer } from '../../../utils'


export const NAME = 'UploadCreation'

export const types = createConstants(NAME, [
  'FADE_FORM',
  'FIT_FORM',
  'CREATION_FORM_REQUEST',
  'CREATION_FORM_SUCCESS',
  'CREATION_FORM_FAILURE',
])

export const initialState = {
  creationForEdit: null,
  err: null,
}

export const actions = {
  submitForm: (payload, creationType, authHeaders, navigateTo) => ({
    type: types.CREATION_FORM_REQUEST,
    payload,
    meta: { authHeaders, creationType, navigateTo },
  }),
}

export default createReducer(initialState, {
  [types.CREATION_FORM_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.CREATION_FORM_SUCCESS]: (state) => ({
    ...state,
    isFetching: false,
    err: null,
  }),
  [types.CREATION_FORM_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
