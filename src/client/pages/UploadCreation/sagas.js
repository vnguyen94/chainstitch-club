import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'

import { types } from './ducks'


function submitCreationSuccess() {
  return {
    type: types.CREATION_FORM_SUCCESS,
  }
}

function submitCreationFailure(err) {
  return {
    type: types.CREATION_FORM_FAILURE,
    payload: err,
    error: true,
  }
}

function* submitCreationRequest({ payload, meta }) {
  try {
    const apiRoute = `/${meta.creationType}s`

    const request = yield call(
      API.post,
      apiRoute,
      payload,
      meta.authHeaders,
    )
    const { redirectUrl } = request.data.data

    yield put(submitCreationSuccess())
    meta.navigateTo(`${redirectUrl}/edit`)
  } catch (err) {
    yield put(submitCreationFailure(err.response.data.message))
  }
}

function* watchSubmitCreationForm() {
  yield takeLatest(types.CREATION_FORM_REQUEST, submitCreationRequest)
}


export default {
  watchSubmitCreationForm,
}
