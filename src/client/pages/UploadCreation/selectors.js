import { createSelector } from 'reselect'

import { NAME } from './ducks'


export const getStateUserCreationsSelector = (state) => state[NAME]
export const getCreationTypeSelector =
  (state, ownProps) => ownProps.route.creationType

export const getErrSelector = createSelector(
  [getStateUserCreationsSelector],
  (state) => state.err,
)
