import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta } from '../../components'
import {
  getAuthHeadersSelector,
  getCurrentUserSelector,
} from '../../shared/selectors'

import { FadeForm, FitForm } from './components'
import {
  getErrSelector,
} from './selectors'
import { actions as UploadCreationActions } from './ducks'
import styles from './styles.scss'

const CreationForms = {
  fade: FadeForm,
  fit: FitForm,
}

const propTypes = {
  actions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
  authHeaders: PropTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  err: getErrSelector(state),
  currentUser: getCurrentUserSelector(state),
  authHeaders: getAuthHeadersSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    uploadCreation: bindActionCreators(UploadCreationActions, dispatch),
  },
})

class UploadCreation extends Component {
  onSubmitHandler = (payload) => {
    const {
      route: { creationType },
      actions,
      router,
      authHeaders,
    } = this.props

    actions.uploadCreation.submitForm(
      payload,
      creationType,
      authHeaders,
      router.push,
    )
  }

  render() {
    const {
      route: { creationType },
    } = this.props
    const CreationForm = CreationForms[creationType]
    const formProps = {
      onSubmit: this.onSubmitHandler,
    }
    let title
    let subheader

    if (creationType === 'fade') {
      title = 'create fade'
      subheader = 'denim jeans, waxed jackets, leather boots -- whatever fades'
    } else if (creationType === 'fit') {
      title = 'create fit'
      subheader = 'share your outfit with the global community'
    } else {
      throw new Error('invalid creation type.')
    }

    return (
      <Fragment>
        <HeaderMeta title={title} />
        <Row center="xs">
          <Col xs={12} className={styles.page}>
            <div className={styles.formContainer}>
              <div className={styles.subHeader}>
                <h3>{title}</h3>
                <h4>{subheader}</h4>
              </div>
              <CreationForm {...formProps} />
            </div>
          </Col>
        </Row>
      </Fragment>
    )
  }
}

UploadCreation.propTypes = propTypes


export { UploadCreation }
export default connect(mapStateToProps, mapDispatchToProps)(UploadCreation)
