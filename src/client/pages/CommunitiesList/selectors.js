import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'

import { getStateEntitiesSelector } from '../../shared/selectors'
import { CommunitiesSchema } from '../../../schemas'

import { NAME } from './ducks'


export const getStateCommunitiesListSelector = (state) => state[NAME]

export const getCommunitiesListSelector = createSelector(
  [getStateCommunitiesListSelector, getStateEntitiesSelector],
  (state, entities) => (
    state.communities.length && entities.communities
      ? (
        denormalize(
          state.communities,
          CommunitiesSchema.list,
          entities,
        )
      ) : []
  ),
)

export const getPaginationSelector = createSelector(
  [getStateCommunitiesListSelector],
  (state) => state.pagination,
)

export const getErrSelector = createSelector(
  [getStateCommunitiesListSelector],
  (state) => state.err,
)
