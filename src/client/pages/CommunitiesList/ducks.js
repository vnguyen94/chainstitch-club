import { createConstants, createReducer } from '../../../utils'


export const NAME = 'CommunitiesList'

export const types = createConstants(NAME, [
  'FETCH_COMMUNITIES_LIST_REQUEST',
  'FETCH_COMMUNITIES_LIST_SUCCESS',
  'FETCH_COMMUNITIES_LIST_FAILURE',
])

export const initialState = {
  communities: [],
  pagination: {},
  isFetching: false,
  err: null,
}

export const actions = {
  fetchCommunitiesList: (activePage) => ({
    type: types.FETCH_COMMUNITIES_LIST_REQUEST,
    payload: { activePage },
  }),
}

export default createReducer(initialState, {
  [types.FETCH_COMMUNITIES_LIST_REQUEST]: (state) => ({
    ...state,
    communities: initialState.communities,
    pagination: initialState.pagination,
    isFetching: true,
    err: null,
  }),
  [types.FETCH_COMMUNITIES_LIST_SUCCESS]: (state, { payload }) => ({
    ...state,
    communities: payload.communities,
    pagination: payload.pagination,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_COMMUNITIES_LIST_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
