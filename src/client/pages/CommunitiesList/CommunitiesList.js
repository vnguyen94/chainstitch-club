import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import {
  CommunityCard,
  Navbar,
  HeaderMeta,
} from '../../components'

import {
  getCommunitiesListSelector,
  getErrSelector,
  getPaginationSelector,
} from './selectors'
import { actions as CommunitiesListActions } from './ducks'
import styles from './styles.scss'


const navbarOptions = [
  { name: 'popular', url: '/communities' },
  { name: 'styles', url: '/communities/styles' },
  { name: 'brands', url: '/communities/brands' },
  { name: 'regions', url: '/communities/regions' },
]

const propTypes = {
  actions: PropTypes.object.isRequired,
  communities: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

const mapStateToProps = (state, props) => ({
  communities: getCommunitiesListSelector(state, props),
  pagination: getPaginationSelector(state),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    CommunitiesList: bindActionCreators(CommunitiesListActions, dispatch),
  },
})

class CommunitiesList extends Component {
  componentDidMount() {
    const {
      route: { activePage },
      actions,
    } = this.props

    actions.CommunitiesList.fetchCommunitiesList(activePage)
  }

  componentWillReceiveProps(nextProps) {
    const {
      route: { activePage },
      actions,
    } = this.props

    if (activePage !== nextProps.route.activePage) {
      actions.CommunitiesList.fetchCommunitiesList(
        nextProps.route.activePage,
      )
    }
  }

  render() {
    const {
      route,
      location,
      communities,
      pagination,
    } = this.props

    return (
      <Fragment>
        <HeaderMeta
          title="communities"
          description="explore communities on chainstitch club."
        />
        <Navbar
          location={location}
          route={route}
          options={navbarOptions}
        />
        <div className={styles.page}>
          <Row>
            {communities.map((community) => (
              <Col
                className={styles.community}
                key={community.id}
                xs={12}
                md={4}
              >
                <CommunityCard
                  community={community.communities}
                />
              </Col>
            ))}
          </Row>
          {pagination && JSON.stringify(pagination)}
        </div>
      </Fragment>
    )
  }
}

CommunitiesList.propTypes = propTypes


export { CommunitiesList }
export default connect(mapStateToProps, mapDispatchToProps)(CommunitiesList)
