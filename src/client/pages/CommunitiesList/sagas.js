import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'
import { insertEntity } from '../../shared/entities'

import { types } from './ducks'


function fetchCommunitiesListSuccess(communities, pagination) {
  return {
    type: types.FETCH_COMMUNITIES_LIST_SUCCESS,
    payload: {
      communities,
      pagination,
    },
  }
}

function fetchCommunitiesListFailure(err) {
  return {
    type: types.FETCH_COMMUNITIES_LIST_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchCommunitiesList({ payload }) {
  try {
    const { activePage } = payload
    const request = yield call(API.get, `/communities?filter=${activePage}`)
    const {
      result,
      entities,
      pagination,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCommunitiesListSuccess(result, pagination)),
    ])
  } catch (err) {
    yield put(fetchCommunitiesListFailure(err.response.data.message))
  }
}

function* watchAttemptFetchCommunitiesList() {
  yield takeLatest(
    types.FETCH_COMMUNITIES_LIST_REQUEST,
    requestFetchCommunitiesList,
  )
}


export default {
  watchAttemptFetchCommunitiesList,
}
