import { createConstants, createReducer } from '../../../utils'


export const NAME = 'explore'

export const types = createConstants(NAME, [
  'FETCH_EXPLORE_REQUEST',
  'FETCH_EXPLORE_SUCCESS',
  'FETCH_EXPLORE_FAILURE',
])

export const initialState = {
  result: [],
  entities: {},
  isFetching: false,
  err: null,
}

export const actions = {
  fetchExploreResults: (filter, sort) => ({
    type: types.FETCH_EXPLORE_REQUEST,
    payload: {
      filter,
      sort,
    },
  }),
}

export default createReducer(initialState, {
  [types.FETCH_EXPLORE_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    result: [],
    entities: {},
    err: null,
  }),
  [types.FETCH_EXPLORE_SUCCESS]: (state, { payload }) => ({
    ...state,
    result: payload.result,
    entities: payload.entities,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_EXPLORE_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
