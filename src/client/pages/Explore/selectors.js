import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'
import { isEmpty } from 'lodash'

import { CreationsSchema } from '../../../schemas'

import { NAME } from './ducks'


export const getStateExploreSelector = (state) => state[NAME]

export const getExploreCreationsSelector = createSelector(
  [getStateExploreSelector],
  (state) => {
    if (!state.result.length || isEmpty(state.entities)) {
      return []
    }

    return denormalize(
      state.result,
      CreationsSchema.listWithUser,
      state.entities,
    )
      .map((result) => ({
        ...result,
        [result.type]: state.entities[result.type][result.id],
      }))
  },
)

export const getErrSelector = createSelector(
  [getStateExploreSelector],
  (loginState) => loginState.err,
)
