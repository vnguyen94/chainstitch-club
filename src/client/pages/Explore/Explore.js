import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import {
  CreationList,
  HeaderMeta,
  Navbar,
} from '../../components'

import { actions as ExploreActions } from './ducks'
import {
  getStateExploreSelector,
  getExploreCreationsSelector,
} from './selectors'
import styles from './styles.scss'


const navbarOptions = [
  { name: 'popular', url: '/', shouldDisableFilters: true },
  { name: 'featured', url: '/featured' },
  { name: 'new', url: '/new' },
]

const navbarFilters = [
  {
    name: 'all',
    query: '',
    isActive: (sort) => !sort,
  },
  {
    name: 'fits',
    query: '?sort=fits',
    isActive: (sort) => sort === 'fits',
  },
  {
    name: 'fades',
    query: '?sort=fades',
    isActive: (sort) => sort === 'fades',
  },
  {
    name: 'collections',
    query: '?sort=collections',
    isActive: (sort) => sort === 'collections',
  },
]

const propTypes = {
  actions: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  explore: PropTypes.object.isRequired,
  creations: PropTypes.array.isRequired,
}

const mapStateToProps = (state) => ({
  creations: getExploreCreationsSelector(state),
  explore: getStateExploreSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    Explore: bindActionCreators(ExploreActions, dispatch),
  },
})

class Explore extends Component {
  componentDidMount() {
    const { actions, location, route } = this.props

    actions.Explore.fetchExploreResults(route.activePage, location.query.sort)
  }

  componentWillReceiveProps(nextProps) {
    const { actions, location, route } = this.props

    if (route.activePage !== nextProps.route.activePage) {
      actions.Explore.fetchExploreResults(
        nextProps.route.activePage,
        nextProps.location.query.sort,
      )
    } else if (location.query.sort !== nextProps.location.query.sort) {
      actions.Explore.fetchExploreResults(
        nextProps.route.activePage,
        nextProps.location.query.sort,
      )
    }
  }

  render() {
    const {
      explore,
      location,
      route,
      creations,
    } = this.props

    return (
      <Fragment>
        <HeaderMeta title="explore" />
        <Navbar
          location={location}
          route={route}
          options={navbarOptions}
          filters={navbarFilters}
        />
        <Row center="xs" className={styles.page}>
          <Col xs={12} md={6} mdOffset={3}>
            {explore.isFetching && (
              <span>loading...</span>
            )}
            {!explore.isFetching && (
              <CreationList
                creations={creations}
                creationCardProps={{
                  hasUserCard: true,
                  creationCardImageStyles: styles.creationCardImage,
                }}
              />
            )}
          </Col>
        </Row>
      </Fragment>
    )
  }
}

Explore.propTypes = propTypes


export { Explore }
export default connect(mapStateToProps, mapDispatchToProps)(Explore)
