import { put, all, call, takeLatest } from 'redux-saga/effects' //eslint-disable-line

import { API } from '../../../utils'


import { types } from './ducks'


export function fetchExploreResultsSuccess(payload) {
  return {
    type: types.FETCH_EXPLORE_SUCCESS,
    payload,
  }
}

function fetchExploreResultsFailure(err) {
  return {
    type: types.FETCH_EXPLORE_FAILURE,
    payload: err,
    error: true,
  }
}

function* fetchExploreResults({ payload }) {
  try {
    const { filter, sort } = payload
    const sortString = sort
      ? `?sort=${sort}`
      : ''

    const request = yield call(API.get, `/explore/${filter}${sortString}`)

    yield put(fetchExploreResultsSuccess(request.data.data))
  } catch (err) {
    yield put(fetchExploreResultsFailure(err.response.data.message))
  }
}

function* watchAttemptFetchExploreResults() {
  yield takeLatest(types.FETCH_EXPLORE_REQUEST, fetchExploreResults)
}


export default {
  watchAttemptFetchExploreResults,
}
