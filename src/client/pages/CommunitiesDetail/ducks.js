import { createConstants, createReducer } from '../../../utils'


export const NAME = 'CommunitiesDetail'

export const types = createConstants(NAME, [
  'FETCH_COMMUNITIES_DETAIL_REQUEST',
  'FETCH_COMMUNITIES_DETAIL_SUCCESS',
  'FETCH_COMMUNITIES_DETAIL_FAILURE',
])

export const initialState = {
  community: null,
  isFetching: false,
  err: null,
}

export const actions = {
  fetchCommunities: (communityId) => ({
    type: types.FETCH_COMMUNITIES_DETAIL_REQUEST,
    payload: { communityId },
  }),
}

export default createReducer(initialState, {
  [types.FETCH_COMMUNITIES_DETAIL_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.FETCH_COMMUNITIES_DETAIL_SUCCESS]: (state, { payload }) => ({
    ...state,
    community: payload.community,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_COMMUNITIES_DETAIL_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
