import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import styles from './styles.scss'


const propTypes = {
  community: PropTypes.object.isRequired,
}

const Details = ({ community }) => (
  <Row>
    <Col xs={12}>
      <img
        src={community.imageUrl}
        alt={community.title}
        className={styles.profileImage}
      />
      <h2>{community.title}</h2>
      <h4>{community.type}</h4>
      <h4>{community.description}</h4>
    </Col>
  </Row>
)

Details.propTypes = propTypes


export default Details
