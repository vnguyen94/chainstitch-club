import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta, CreationListMinimal } from '../../components'

import { Detail } from './components'
import {
  getCommunitiesDetailSelector,
  getErrSelector,
} from './selectors'
import { actions as CommunitiesDetailActions } from './ducks'
import styles from './styles.scss'


const propTypes = {
  actions: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  community: PropTypes.object,
}

const mapStateToProps = (state, ownProps) => ({
  community: getCommunitiesDetailSelector(state, ownProps),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    CommunitiesDetail: bindActionCreators(CommunitiesDetailActions, dispatch),
  },
})

class CommunitiesDetail extends Component {
  componentDidMount() {
    const {
      params: { communityId },
      actions,
    } = this.props

    actions.CommunitiesDetail.fetchCommunities(communityId)
  }

  render() {
    const { community } = this.props
    let content

    if (community) {
      content = (
        <Fragment>
          <HeaderMeta
            title={community.title}
            description={community.description}
            image={community.image}
          />
          <Detail community={community} />
          <Row center="xs" className={styles.page}>
            <Col xs={12} md={6} mdOffset={3}>
              <CreationListMinimal creations={community.creations} />
            </Col>
          </Row>
        </Fragment>
      )
    }

    return (
      <Row center="xs" className={styles.page}>
        <Col xs={12}>
          {content}
        </Col>
      </Row>
    )
  }
}

CommunitiesDetail.propTypes = propTypes


export { CommunitiesDetail }
export default connect(mapStateToProps, mapDispatchToProps)(CommunitiesDetail)
