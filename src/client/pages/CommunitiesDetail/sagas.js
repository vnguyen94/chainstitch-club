import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'
import { insertEntity } from '../../shared/entities'

import { types } from './ducks'


function fetchCommunitiesDetailSuccess(community) {
  return {
    type: types.FETCH_COMMUNITIES_DETAIL_SUCCESS,
    payload: {
      community,
    },
  }
}

function fetchCommunitiesDetailFailure(err) {
  return {
    type: types.FETCH_COMMUNITIES_DETAIL_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchCommunitiesDetail({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/communities/${payload.communityId}?related=creations`,
    )
    const {
      result,
      entities,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCommunitiesDetailSuccess(result)),
    ])
  } catch (err) {
    yield put(fetchCommunitiesDetailFailure(err.response.data.message))
  }
}

function* watchAttemptFetchCommunitiesDetail() {
  yield takeLatest(
    types.FETCH_COMMUNITIES_DETAIL_REQUEST,
    requestFetchCommunitiesDetail,
  )
}


export default {
  watchAttemptFetchCommunitiesDetail,
}
