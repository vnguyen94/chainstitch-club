import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'

import { getStateEntitiesSelector } from '../../shared/selectors'
import { CommunitiesSchema } from '../../../schemas'

import { NAME } from './ducks'


export const getStateUserCommunitiesSelector = (state) => state[NAME]

export const getCommunitiesDetailSelector = createSelector(
  [getStateUserCommunitiesSelector, getStateEntitiesSelector],
  (state, entities) => (
    state.community && entities.communities
      ? (
        denormalize(
          state.community,
          CommunitiesSchema.single,
          entities,
        )
      ) : null
  ),
)

export const getErrSelector = createSelector(
  [getStateUserCommunitiesSelector],
  (state) => state.err,
)
