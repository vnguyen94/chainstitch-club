import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta } from '../../components'

import { SearchForm } from './components'
import styles from './styles.scss'


const Search = () => (
  <Row center="xs" className={styles.page}>
    <Col xs={12}>
      <HeaderMeta
        title="search"
        description="search for fits and fades on chainstitch club."
      />
      <SearchForm />
    </Col>
  </Row>
)


export default Search
