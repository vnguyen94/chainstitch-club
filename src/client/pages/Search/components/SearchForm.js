import React from 'react'
import { Field, Form, reduxForm } from 'redux-form'

import { SEARCH_FORM } from '../constants'


const SearchForm = () => (
  <Form>
    <Field name="search" component="input" type="text" />
    <button
      className="button button--success"
      type="submit"
    >
      search
    </button>
  </Form>
)


export default reduxForm({ form: SEARCH_FORM })(SearchForm)
