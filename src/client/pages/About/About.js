import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta } from '../../components'

import styles from './styles.scss'


/* eslint-disable max-len */
const About = () => (
  <Row className={styles.page} center="xs">
    <Col xs={12}>
      <HeaderMeta
        title="about"
        description="made by and for denim enthusiasts"
      />
      <div className={styles.heroImage}>
        <div className={styles.heroTextContainer}>
          <h1>a place to share denim and clothes</h1>
          <p>
            CSC expands the raw denim owner and the clothing
            enthusiast networks to a global scale using a
            data-driven approach.
          </p>
        </div>
      </div>
      <section>
        <Row middle="md">
          <Col xs={12} md={6}>
            <h1>a place to share denim and clothes</h1>
            <p>
              CSC expands the raw denim owner and the clothing
              enthusiast networks to a global scale using a
              data-driven approach.
            </p>
            <button className="button button--success button--thin">
              explore
            </button>
          </Col>
          <Col xs={12} md={6}>
            <img
              src="https://46fjj12eeht73lsxezcqpvlt-wpengine.netdna-ssl.com/wp-content/uploads/2016/05/Fade-Friday-Pure-Blue-Japan-X-011-2-Years-7-Months-4-Soaks-fit.jpg"
              alt="some dude"
            />
          </Col>
        </Row>
      </section>
      <section>
        <Row middle="md">
          <Col xs={12} md={6}>
            <img
              src="https://www.denimhunters.com/wp-content/uploads/2016/10/Denimglossary-chainstitch.jpg"
              alt="some dude"
            />
          </Col>
          <Col xs={12} md={6}>
            <h1>a place to share denim and clothes</h1>
            <p>
              CSC expands the raw denim owner and the clothing
              enthusiast networks to a global scale using a
              data-driven approach.
            </p>
            <button className="button button--success button--thin">
              explore
            </button>
          </Col>
        </Row>
      </section>
      <section>
        <Row middle="md">
          <Col xs={12} md={6}>
            <h1>a place to share denim and clothes</h1>
            <p>
              CSC expands the raw denim owner and the clothing
              enthusiast networks to a global scale using a
              data-driven approach.
            </p>
            <button className="button button--success button--thin">
              explore
            </button>
          </Col>
          <Col xs={12} md={6}>
            <img
              src="https://46fjj12eeht73lsxezcqpvlt-wpengine.netdna-ssl.com/wp-content/uploads/2011/04/slubby-denim.jpg"
              alt="some dude"
            />
          </Col>
        </Row>
      </section>
    </Col>
  </Row>
)
/* eslint-enable max-len */


export default About
