import { createSelector } from 'reselect'

import { NAME } from './ducks'


export const getStateSettingsSelector = (state) => state[NAME]
export const getErrSelector = createSelector(
  [getStateSettingsSelector],
  (state) => state.err,
)

export const getSettingsSelector = createSelector(
  [getStateSettingsSelector],
  (state) => state.settings,
)
