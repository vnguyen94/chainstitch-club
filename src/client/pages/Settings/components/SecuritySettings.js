import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'

import { types } from '../ducks'

import { SettingsTab, SettingsSection } from './'


const EmailResetForm = reduxForm({
  form: types.EMAIL_RESET_FORM,
})(({ handleSubmit, onSubmit, valid }) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    <h3>change password or email</h3>
    <button
      className="button button--success button--thin"
      disabled={!valid}
      type="submit"
    >
      send reset email
    </button>
  </Form>
))

const propTypes = {
  actions: PropTypes.object.isRequired,
}

const SecuritySettings = ({ actions }) => (
  <SettingsTab>
    <SettingsSection>
      <EmailResetForm
        onSubmit={actions.Settings.sendEmailResetForm}
      />
    </SettingsSection>
  </SettingsTab>
)

SecuritySettings.propTypes = propTypes


export default SecuritySettings
