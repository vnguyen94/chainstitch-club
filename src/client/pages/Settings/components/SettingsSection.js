import PropTypes from 'prop-types'
import React from 'react'

import styles from './styles.scss'


const propTypes = {
  children: PropTypes.element.isRequired,
}

const SettingsSection = ({ children }) => (
  <ul className={styles.settingsSection}>
    {children}
  </ul>
)

SettingsSection.propTypes = propTypes


export default SettingsSection
