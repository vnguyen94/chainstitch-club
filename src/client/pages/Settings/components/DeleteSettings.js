import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Form, reduxForm } from 'redux-form'
import classNames from 'classnames'

import { types } from '../ducks'

import styles from './styles.scss'

import { SettingsTab, SettingsSection } from './'


const propTypesDeleteButtons = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
}

class DeleteButtons extends Component {
  state = {
    confirmDelete: false,
  }

  onClickToggleConfirmDeleteHandler = (e) => {
    e.preventDefault()

    this.setState({ confirmDelete: !this.state.confirmDelete })
  }

  render() {
    const { handleSubmit, onSubmit } = this.props
    const { confirmDelete } = this.state

    return (
      <Form onSubmit={handleSubmit(onSubmit)}>
        <h3>delete account</h3>
        <button
          className={classNames('button button--success button--thin', {
            hide: confirmDelete,
          })}
          onClick={this.onClickToggleConfirmDeleteHandler}
        >
          delete account
        </button>
        <button
          className={classNames('button button--clear button--thin', {
            [styles.cancelDelete]: true,
            hide: !confirmDelete,
          })}
          onClick={this.onClickToggleConfirmDeleteHandler}
        >
          cancel
        </button>
        <button
          className={classNames('button button--success button--thin', {
            hide: !confirmDelete,
          })}
          type="submit"
        >
          confirm delete
        </button>
      </Form>
    )
  }
}

DeleteButtons.propTypes = propTypesDeleteButtons

const propTypesDeleteSettings = {
  actions: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
}

const DeleteSection = reduxForm({
  form: types.DELETE_FORM,
})(DeleteButtons)

const DeleteSettings = ({ actions, router }) => {
  const onSubmitHandler = () => {
    actions.Settings.deleteAccount(() => {
      actions.Login.logout(router.push)
    })
  }

  return (
    <SettingsTab>
      <SettingsSection>
        <DeleteSection onSubmit={onSubmitHandler} />
      </SettingsSection>
    </SettingsTab>
  )
}

DeleteSettings.propTypes = propTypesDeleteSettings


export default DeleteSettings
