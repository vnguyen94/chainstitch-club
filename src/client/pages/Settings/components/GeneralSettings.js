import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'
import Joi from 'joi'

import { validateClientForm } from '../../../../utils'
import { UserForms } from '../../../forms'
import { types } from '../ducks'

import { SettingsTab, SettingsSection } from './'


const usernameFormSchema = Joi.object().keys({
  username: Joi.string().required().label('username'),
})

const UsernameSection = reduxForm({
  form: types.USERNAME_FORM,
  fields: ['username'],
  validate: validateClientForm(usernameFormSchema),
  enableReinitialize: true,
})(({ handleSubmit, onSubmit, err, valid }) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    <h3>change username</h3>
    <UserForms.UsernameSection />
    {err && <p>{err}</p>}
    <button
      className="button button--success button--thin"
      disabled={!valid}
      type="submit"
    >
      update
    </button>
  </Form>
))

const userProfileFormSchema = Joi.object().keys({
  firstName: Joi.string().allow('').label('first name'),
  lastName: Joi.string().allow('').label('last name'),
  location: Joi.string().allow('').label('location'),
  description: Joi.string().allow('').label('description'),
})

const UserProfileSection = reduxForm({
  form: types.USER_PROFILE_FORM,
  fields: ['firstName', 'lastName', 'location', 'description'],
  validate: validateClientForm(userProfileFormSchema),
  enableReinitialize: true,
})(({ handleSubmit, onSubmit, err, valid }) => (
  <Form onSubmit={handleSubmit(onSubmit)}>
    <h3>update profile</h3>
    <UserForms.UserProfileSection />
    {err && <p>{err}</p>}
    <button
      className="button button--success button--thin"
      disabled={!valid}
      type="submit"
    >
      update
    </button>
  </Form>
))

const propTypes = {
  settings: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
}

const GeneralSettings = ({ actions, settings }) => (
  <SettingsTab>
    <SettingsSection>
      <UsernameSection
        initialValues={{ username: settings.user.username }}
        onSubmit={actions.Settings.updateUserSettings}
      />
    </SettingsSection>
    <SettingsSection>
      <UserProfileSection
        initialValues={{
          firstName: settings.profile.firstName,
          lastName: settings.profile.lastName,
          location: settings.profile.location,
          description: settings.profile.description,
        }}
        onSubmit={actions.Settings.updateProfileSettings}
      />
    </SettingsSection>
  </SettingsTab>
)

GeneralSettings.propTypes = propTypes


export default GeneralSettings
