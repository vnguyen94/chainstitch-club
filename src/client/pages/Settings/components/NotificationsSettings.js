import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'
import Joi from 'joi'

import { CheckboxField } from '../../../forms/components'
import { validateClientForm } from '../../../../utils'
import { types } from '../ducks'

import { SettingsTab, SettingsSection } from './'


const notificationFormSchema = Joi.object().keys({
  notifMessages: Joi.any().valid(true, false, '').label('messages'),
  notifFollowers: Joi.any().valid(true, false, '').label('followers'),
  notifLikers: Joi.any().valid(true, false, '').label('likers'),
  notifFeed: Joi.any().valid(true, false, '').label('feedUpdates'),
})

const NotificationSection = reduxForm({
  form: types.NOTIFICATION_FORM,
  fields: ['notifMessages', 'notifFollowers', 'notifLikers', 'notifFeed'],
  validate: validateClientForm(notificationFormSchema),
  enableReinitialize: true,
})(({ handleSubmit, onSubmit, err, valid }) => (
  <Form
    onSubmit={handleSubmit((e) => onSubmit({
      notifMessages: !!e.notifMessages,
      notifFollowers: !!e.notifFollowers,
      notifLikers: !!e.notifLikers,
      notifFeed: !!e.notifFeed,
    }))}
  >
    <h3>toggle notifications</h3>
    <CheckboxField
      name="notifMessages"
      caption="messages"
    />
    <CheckboxField
      name="notifFollowers"
      caption="followers"
    />
    <CheckboxField
      name="notifLikers"
      caption="likers"
    />
    <CheckboxField
      name="notifFeed"
      caption="feed updates"
    />
    {err && <p>{err}</p>}
    <button
      className="button button--success button--thin"
      disabled={!valid}
      type="submit"
    >
      update notifications
    </button>
  </Form>
))

const propTypes = {
  settings: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
}


// eslint-disable-next-line no-unused-vars
const NotificationsSettings = ({ settings, actions }) => (
  <SettingsTab>
    <SettingsSection>
      <NotificationSection
        onSubmit={actions.Settings.updateNotifications}
        initialValues={{
          notifMessages: settings.notifications.messages,
          notifFollowers: settings.notifications.followers,
          notifLikers: settings.notifications.likers,
          notifFeed: settings.notifications.feed,
        }}
      />
    </SettingsSection>
  </SettingsTab>
)

NotificationsSettings.propTypes = propTypes


export default NotificationsSettings
