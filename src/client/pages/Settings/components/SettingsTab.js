import PropTypes from 'prop-types'
import React from 'react'

import styles from './styles.scss'


const propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]).isRequired,
}

const SettingsTab = ({ children }) => (
  <li className={styles.settingsTab}>
    {children}
  </li>
)

SettingsTab.propTypes = propTypes


export default SettingsTab
