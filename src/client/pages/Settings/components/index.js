import GeneralSettings from './GeneralSettings'
import SecuritySettings from './SecuritySettings'
import NotificationsSettings from './NotificationsSettings'
import DeleteSettings from './DeleteSettings'
import SettingsTab from './SettingsTab'
import SettingsSection from './SettingsSection'


export {
  GeneralSettings,
  SecuritySettings,
  NotificationsSettings,
  DeleteSettings,
  SettingsTab,
  SettingsSection,
}
