import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta, Sidebar } from '../../components'
import { getCurrentUserSelector } from '../../shared/selectors'
import { actions as LoginActions } from '../Login/ducks'

import {
  GeneralSettings,
  SecuritySettings,
  NotificationsSettings,
  DeleteSettings,
} from './components'
import { getSettingsSelector, getErrSelector } from './selectors'
import { actions as SettingsActions } from './ducks'
import styles from './styles.scss'

const SettingsComponents = {
  general: GeneralSettings,
  security: SecuritySettings,
  notifications: NotificationsSettings,
  delete: DeleteSettings,
}

const sidebarElements = [
  {
    caption: 'general',
    link: '/settings/general',
    active: 'general',
  },
  {
    caption: 'security and login',
    link: '/settings/security',
    active: 'security',
  },
  {
    caption: 'notifications',
    link: '/settings/notifications',
    active: 'notifications',
  },
  {
    caption: 'delete',
    link: '/settings/delete',
    active: 'delete',
  },
]

const propTypes = {
  user: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  settings: PropTypes.object,
}

const mapStateToProps = (state) => ({
  settings: getSettingsSelector(state),
  user: getCurrentUserSelector(state),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    Settings: bindActionCreators(SettingsActions, dispatch),
    Login: bindActionCreators(LoginActions, dispatch),
  },
})

class Settings extends Component {
  componentDidMount() {
    const { actions, user } = this.props

    actions.Settings.fetchSettings(user.id)
  }

  render() {
    const {
      router,
      settings,
      actions,
      route: { activePage },
    } = this.props
    const SettingsComponent = SettingsComponents[activePage]

    return (
      <Row center="xs" className={styles.page}>
        <Col xs={12}>
          <HeaderMeta title="settings" />
          <Row>
            <Col xs={12} md={4}>
              <Sidebar
                activePage={activePage}
                elements={sidebarElements}
              />
            </Col>
            <Col xs={12} md={8}>
              {settings &&
                <SettingsComponent
                  router={router}
                  settings={settings}
                  actions={actions}
                />
              }
            </Col>
          </Row>
        </Col>
      </Row>
    )
  }
}

Settings.propTypes = propTypes


export { Settings }
export default connect(mapStateToProps, mapDispatchToProps)(Settings)
