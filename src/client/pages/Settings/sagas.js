import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'

import { types } from './ducks'


function updateUserSettingsSuccess(settings) {
  return {
    type: types.UPDATE_USER_SETTINGS_SUCCESS,
    payload: settings,
  }
}

function updateUserSettingsFailure(err) {
  return {
    type: types.UPDATE_USER_SETTINGS_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestUpdateUserSettings({ payload }) {
  try {
    yield call(API.patch, '/users', payload)
    yield put(updateUserSettingsSuccess(payload))
  } catch (err) {
    yield put(updateUserSettingsFailure(err.response.data.message))
  }
}

function* watchAttemptUpdateUserSettings() {
  yield takeLatest(
    types.UPDATE_USER_SETTINGS_REQUEST,
    requestUpdateUserSettings,
  )
}

function updateProfileSettingsSuccess(settings) {
  return {
    type: types.UPDATE_PROFILE_SETTINGS_SUCCESS,
    payload: settings,
  }
}

function updateProfileSettingsFailure(err) {
  return {
    type: types.UPDATE_PROFILE_SETTINGS_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestUpdateProfileSettings({ payload }) {
  try {
    yield call(API.patch, '/users', payload)
    yield put(updateProfileSettingsSuccess(payload))
  } catch (err) {
    yield put(updateProfileSettingsFailure(err.response.data.message))
  }
}

function* watchAttemptUpdateProfileSettings() {
  yield takeLatest(
    types.UPDATE_PROFILE_SETTINGS_REQUEST,
    requestUpdateProfileSettings,
  )
}

function updateNotificationsSuccess(notifications) {
  return {
    type: types.UPDATE_NOTIFICATIONS_SUCCESS,
    payload: {
      notifications,
    },
  }
}

function updateNotificationsFailure(err) {
  return {
    type: types.UPDATE_NOTIFICATIONS_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestUpdateNotifications({ payload }) {
  try {
    yield call(API.patch, '/users', payload)
    yield put(updateNotificationsSuccess(payload))
  } catch (err) {
    yield put(updateNotificationsFailure(err.response.data.message))
  }
}

function* watchAttemptUpdateNotifications() {
  yield takeLatest(
    types.UPDATE_NOTIFICATIONS_REQUEST,
    requestUpdateNotifications,
  )
}

function sendEmailResetFormSuccess() {
  return {
    type: types.SEND_EMAIL_RESET_SUCCESS,
  }
}

function sendEmailResetFormFailure(err) {
  return {
    type: types.SEND_EMAIL_RESET_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestSendEmailResetForm() {
  try {
    yield call(API.post, '/auth/security')
    yield put(sendEmailResetFormSuccess())
  } catch (err) {
    yield put(sendEmailResetFormFailure(err.response.data.message))
  }
}

function* watchAttemptSendEmailResetForm() {
  yield takeLatest(
    types.SEND_EMAIL_RESET_REQUEST,
    requestSendEmailResetForm,
  )
}

function deleteAccountSuccess() {
  return {
    type: types.DELETE_ACCOUNT_SUCCESS,
  }
}

function deleteAccountFailure(err) {
  return {
    type: types.DELETE_ACCOUNT_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestDeleteAccount({ meta }) {
  try {
    yield call(API.delete, '/users')
    yield put(deleteAccountSuccess())
    meta.successCallback()
  } catch (err) {
    yield put(deleteAccountFailure(err.response.data.message))
  }
}

function* watchAttemptDeleteAccount() {
  yield takeLatest(
    types.DELETE_ACCOUNT_REQUEST,
    requestDeleteAccount,
  )
}

function fetchSettingsSuccess(settings) {
  return {
    type: types.FETCH_SETTINGS_SUCCESS,
    payload: {
      settings,
    },
  }
}

function fetchSettingsFailure(err) {
  return {
    type: types.FETCH_SETTINGS_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestSettingsDetail({ payload }) {
  try {
    const request = yield call(API.get, `/users/${payload.userId}/profile`)
    const settings = request.data.data

    yield put(fetchSettingsSuccess(settings))
  } catch (err) {
    yield put(fetchSettingsFailure(err.response.data.message))
  }
}

function* watchAttemptSettingsDetail() {
  yield takeLatest(
    types.FETCH_SETTINGS_REQUEST,
    requestSettingsDetail,
  )
}


export default {
  watchAttemptUpdateUserSettings,
  watchAttemptUpdateProfileSettings,
  watchAttemptUpdateNotifications,
  watchAttemptSendEmailResetForm,
  watchAttemptDeleteAccount,
  watchAttemptSettingsDetail,
}
