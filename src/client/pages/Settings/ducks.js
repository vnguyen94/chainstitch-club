import { createConstants, createReducer } from '../../../utils'


export const NAME = 'Settings'

export const types = createConstants(NAME, [
  // actions
  'FETCH_SETTINGS_REQUEST',
  'FETCH_SETTINGS_SUCCESS',
  'FETCH_SETTINGS_FAILURE',
  'UPDATE_USER_SETTINGS_REQUEST',
  'UPDATE_USER_SETTINGS_SUCCESS',
  'UPDATE_USER_SETTINGS_FAILURE',
  'UPDATE_PROFILE_SETTINGS_REQUEST',
  'UPDATE_PROFILE_SETTINGS_SUCCESS',
  'UPDATE_PROFILE_SETTINGS_FAILURE',
  'UPDATE_NOTIFICATIONS_REQUEST',
  'UPDATE_NOTIFICATIONS_SUCCESS',
  'UPDATE_NOTIFICATIONS_FAILURE',
  'UPDATE_PROFILE_SETTINGS_FAILURE',
  'SEND_EMAIL_RESET_REQUEST',
  'SEND_EMAIL_RESET_SUCCESS',
  'SEND_EMAIL_RESET_FAILURE',
  'DELETE_ACCOUNT_REQUEST',
  'DELETE_ACCOUNT_SUCCESS',
  'DELETE_ACCOUNT_FAILURE',
  // forms
  'USERNAME_FORM',
  'USER_PROFILE_FORM',
  'NOTIFICATION_FORM',
  'EMAIL_RESET_FORM',
  'DELETE_FORM',
])

export const initialState = {
  settings: null,
  isFetching: false,
  err: null,
}

export const actions = {
  fetchSettings: (userId) => ({
    type: types.FETCH_SETTINGS_REQUEST,
    payload: { userId },
  }),
  updateUserSettings: (payload) => ({
    type: types.UPDATE_USER_SETTINGS_REQUEST,
    payload,
  }),
  updateProfileSettings: (payload) => ({
    type: types.UPDATE_PROFILE_SETTINGS_REQUEST,
    payload,
  }),
  updateNotifications: (payload) => ({
    type: types.UPDATE_NOTIFICATIONS_REQUEST,
    payload,
  }),
  sendEmailResetForm: () => ({
    type: types.SEND_EMAIL_RESET_REQUEST,
  }),
  deleteAccount: (successCallback) => ({
    type: types.DELETE_ACCOUNT_REQUEST,
    meta: { successCallback },
  }),
}

export default createReducer(initialState, {
  [types.FETCH_SETTINGS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.FETCH_SETTINGS_SUCCESS]: (state, { payload }) => ({
    ...state,
    settings: payload.settings,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_SETTINGS_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
  [types.UPDATE_USER_SETTINGS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.UPDATE_USER_SETTINGS_SUCCESS]: (state, { payload }) => ({
    ...state,
    settings: {
      ...state.settings,
      user: {
        ...state.settings.user,
        ...payload,
      },
    },
    isFetching: false,
    err: null,
  }),
  [types.UPDATE_USER_SETTINGS_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
  [types.UPDATE_PROFILE_SETTINGS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.UPDATE_PROFILE_SETTINGS_SUCCESS]: (state, { payload }) => ({
    ...state,
    settings: {
      ...state.settings,
      profile: {
        ...state.settings.profile,
        ...payload,
      },
    },
    isFetching: false,
    err: null,
  }),
  [types.UPDATE_PROFILE_SETTINGS_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
  [types.UPDATE_NOTIFICATIONS_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.UPDATE_NOTIFICATIONS_SUCCESS]: (state, { payload }) => ({
    ...state,
    settings: {
      ...state.settings,
      notifications: {
        ...state.settings.notifications,
        messages: payload.notifications.notifMessages,
        followers: payload.notifications.notifFollowers,
        likers: payload.notifications.notifLikers,
        feed: payload.notifications.notifFeed,
      },
    },
    isFetching: false,
    err: null,
  }),
  [types.UPDATE_NOTIFICATIONS_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
  [types.DELETE_ACCOUNT_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.DELETE_ACCOUNT_SUCCESS]: (state) => ({
    ...state,
    isFetching: false,
  }),
  [types.DELETE_ACCOUNT_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
