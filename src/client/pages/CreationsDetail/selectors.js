import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'

import { getStateEntitiesSelector } from '../../shared/selectors'
import { normalizeDimensions } from '../../shared/clientUtils'
import {
  FadesSchema,
  FitsSchema,
  CommentsSchema,
} from '../../../schemas'

import { NAME } from './ducks'

const schemas = {
  fades: FadesSchema,
  fits: FitsSchema,
}


export const getStateUserCreationsSelector = (state) => state[NAME]
export const getCreationTypeSelector =
  (state, ownProps) => ownProps.route.creationType

export const getCreationsDetailSelector = createSelector(
  [getStateUserCreationsSelector, getStateEntitiesSelector,
    getCreationTypeSelector],
  (state, entities, creationType) => (
    state.creation && entities[creationType]
      ? (
        denormalize(
          state.creation,
          schemas[creationType].single,
          entities,
        )
      ) : null
  ),
)

export const getCreationsDetailImagesSelector = createSelector(
  [getCreationsDetailSelector],
  (creation) => {
    if (!creation || !creation.images) {
      return []
    }

    return creation.images.map((image) => {
      const normalized = normalizeDimensions(image.width, image.height)

      return {
        src: image.url,
        thumbnail: image.url,
        title: image.createdAt,
        w: normalized.width,
        h: normalized.height,
      }
    })
  },
)

export const getCreationsDetailCommentsSelector = createSelector(
  [getStateUserCreationsSelector, getStateEntitiesSelector],
  (state, entities) => {
    if (!state.comments.length || !entities.comments) {
      return []
    }

    const normalized = (
      denormalize(
        state.comments,
        CommentsSchema.list,
        entities,
      )
    )

    return [...normalized, ...state.commentsPosting]
  },
)

export const getErrSelector = createSelector(
  [getStateUserCreationsSelector],
  (state) => state.err,
)
