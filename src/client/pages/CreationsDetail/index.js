import reducer, {
  NAME,
  types,
  initialState,
  actions,
} from './ducks'
import sagas from './sagas'
import selectors from './selectors'


export {
  reducer,
  NAME,
  types,
  initialState,
  actions,
  sagas,
  selectors,
}
