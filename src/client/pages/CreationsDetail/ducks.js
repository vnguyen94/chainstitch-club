import { remove } from 'lodash'

import { createConstants, createReducer } from '../../../utils'


export const NAME = 'CreationsDetail'

export const types = createConstants(NAME, [
  'FETCH_CREATIONS_DETAIL_REQUEST',
  'FETCH_CREATIONS_DETAIL_SUCCESS',
  'FETCH_CREATIONS_DETAIL_FAILURE',
  'FETCH_COMMENTS_REQUEST',
  'FETCH_COMMENTS_SUCCESS',
  'FETCH_COMMENTS_FAILURE',
  'FETCH_COLLECTIONS_REQUEST',
  'FETCH_COLLECTIONS_SUCCESS',
  'FETCH_COLLECTIONS_FAILURE',
  'POST_COMMENT_REQUEST',
  'POST_COMMENT_SUCCESS',
  'POST_COMMENT_FAILURE',
])

export const initialState = {
  creation: null,
  comments: [],
  commentsPosting: [],
  collections: {
    jeans: [],
    lists: [],
    communities: [],
    pagination: null,
    isFetching: false,
    err: null,
  },
  isFetching: false,
  err: null,
}

export const actions = {
  fetchCreations: (payload) => ({
    type: types.FETCH_CREATIONS_DETAIL_REQUEST,
    payload,
  }),
  fetchComments: (payload) => ({
    type: types.FETCH_COMMENTS_REQUEST,
    payload,
  }),
  fetchCollections: (userId, collectionType) => ({
    type: types.FETCH_COLLECTIONS_REQUEST,
    payload: {
      userId,
      type: collectionType,
    },
  }),
  postComment: (comment, meta) => ({
    type: types.POST_COMMENT_REQUEST,
    payload: {
      comment,
    },
    meta,
  }),
}

const matchesPlaceholderId = (c) => (placeholder) => c.id === placeholder

export default createReducer(initialState, {
  [types.FETCH_CREATIONS_DETAIL_REQUEST]: (state) => ({
    ...state,
    ...initialState,
  }),
  [types.FETCH_CREATIONS_DETAIL_SUCCESS]: (state, { payload }) => ({
    ...state,
    creation: payload.creation,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_CREATIONS_DETAIL_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
  [types.FETCH_COMMENTS_REQUEST]: (state) => ({
    ...state,
    isFetchingComments: true,
    err: null,
  }),
  [types.FETCH_COMMENTS_SUCCESS]: (state, { payload }) => ({
    ...state,
    comments: payload.comments,
    isFetchingComments: false,
    err: null,
  }),
  [types.FETCH_COMMENTS_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetchingComments: false,
    err: payload,
  }),
  [types.FETCH_COLLECTIONS_REQUEST]: (state) => ({
    ...state,
    collections: initialState.collections,
  }),
  [types.FETCH_COLLECTIONS_SUCCESS]: (state, { payload }) => ({
    ...state,
    collections: {
      ...state.collections,
      [payload.type]: payload.result,
      pagination: payload.pagination,
      isFetching: false,
      err: null,
    },
  }),
  [types.FETCH_COLLECTIONS_FAILURE]: (state, { payload }) => ({
    ...state,
    collections: {
      ...state.collections,
      isFetching: false,
      err: payload,
    },
  }),
  [types.POST_COMMENT_REQUEST]: (state, { payload, meta }) => ({
    ...state,
    isCommenting: true,
    commentsPosting: [
      ...state.commentsPosting,
      {
        id: meta.placeholderId,
        body: payload.comment,
        creations: {
          users: meta.user,
        },
        isPosting: true,
      },
    ],
    err: null,
  }),
  [types.POST_COMMENT_SUCCESS]: (state, { payload, meta }) => ({
    ...state,
    isCommenting: false,
    comments: [...state.comments, payload],
    commentsPosting: remove(
      state.commentsPosting,
      matchesPlaceholderId(meta.placeholderId),
    ),
    err: null,
  }),
  [types.POST_COMMENT_FAILURE]: (state, { payload, meta }) => ({
    ...state,
    isCommenting: false,
    commentsPosting: remove(
      state.commentsPosting,
      matchesPlaceholderId(meta.placeholderId),
    ),
    err: payload,
  }),
})
