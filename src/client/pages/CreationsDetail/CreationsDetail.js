import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import { getRandomIdPlaceholder } from '../../../utils'
import {
  HeaderMeta,
  CommentSection,
  RecommendedCreations,
} from '../../components'
import { getCurrentUserSelector } from '../../shared/selectors'

import {
  OwnerDashboard,
  CreationOwner,
  Detail,
  Fade,
  Fit,
} from './components'
import {
  getCreationsDetailSelector,
  getCreationsDetailImagesSelector,
  getCreationsDetailCommentsSelector,
  getCreationTypeSelector,
  getErrSelector,
} from './selectors'
import { actions as CreationsDetailActions } from './ducks'
import styles from './styles.scss'

const creationContent = {
  fits: Fit,
  fades: Fade,
}

const propTypes = {
  actions: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  images: PropTypes.array.isRequired,
  comments: PropTypes.array.isRequired,
  creationType: PropTypes.string.isRequired,
  user: PropTypes.object,
  creation: PropTypes.object,
}

const mapStateToProps = (state, ownProps) => ({
  user: getCurrentUserSelector(state),
  creationType: getCreationTypeSelector(state, ownProps),
  creation: getCreationsDetailSelector(state, ownProps),
  images: getCreationsDetailImagesSelector(state, ownProps),
  comments: getCreationsDetailCommentsSelector(state, ownProps),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    CreationsDetail: bindActionCreators(CreationsDetailActions, dispatch),
  },
})

class CreationsDetail extends Component {
  componentDidMount() {
    const {
      params: { creationId },
      actions,
      creationType,
    } = this.props

    actions.CreationsDetail.fetchCreations({ creationId, creationType })
    actions.CreationsDetail.fetchComments({ creationId, creationType })
  }

  onSubmitCommentHandler = (comment) => {
    const {
      params: { creationId },
      actions,
      creationType,
      user,
    } = this.props
    const placeholderId = getRandomIdPlaceholder()

    actions.CreationsDetail.postComment(
      comment,
      {
        creationId,
        creationType,
        placeholderId,
        user,
      },
    )
  }

  onChangeFetchJeansHandler = () => {
    const { actions, user } = this.props


    actions.CreationsDetail.fetchCollections(user.id, 'jeans')
  }

  // onChangeFetchUserCollectionsHandler = () => {
  //   const { actions, user } = this.props


  //   actions.CreationsDetail.fetchCollections(user.id, 'collections')
  // }

  render() {
    const { creation, creationType, comments, user, images } = this.props
    const isOwner = !!user && creation && creation.userId === user.id

    if (!creation || !creation[creationType]) {
      return (
        <div>loading...</div>
      )
    }

    const Creation = creationContent[creationType]
    const creationObject = creation[creationType]

    const creationProps = {
      specific: creationObject,
      // fetchUserCollections: this.onChangeFetchUserCollectionsHandler,
      creation,
      isOwner,
    }

    if (creationType === 'fades') {
      creationProps.fetchJeans = this.onChangeFetchJeansHandler
    }

    return (
      <Fragment>
        <HeaderMeta
          key="1"
          title={creationObject.title}
          description={creationObject.description}
          image={creationObject.imageUrl}
        />
        <Row center="xs" className={styles.page}>
          <Col xs={12} md={9}>
            <Detail images={images} />
          </Col>
          <Col xs={12} md={3} className={styles.rightContent}>
            <CreationOwner creation={creation} />
            <Creation {...creationProps} />
            {isOwner &&
              <OwnerDashboard creation={creation} />
            }
            <div>
              <CommentSection
                user={user}
                comments={comments}
                onSubmit={this.onSubmitCommentHandler}
              />
            </div>
          </Col>
        </Row>
        <RecommendedCreations />
      </Fragment>
    )
  }
}

CreationsDetail.propTypes = propTypes


export { CreationsDetail }
export default connect(mapStateToProps, mapDispatchToProps)(CreationsDetail)
