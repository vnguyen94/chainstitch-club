import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'
import { insertEntity } from '../../shared/entities'

import { types } from './ducks'


function fetchCreationsDetailSuccess(creation) {
  return {
    type: types.FETCH_CREATIONS_DETAIL_SUCCESS,
    payload: {
      creation,
    },
  }
}

function fetchCreationsDetailFailure(err) {
  return {
    type: types.FETCH_CREATIONS_DETAIL_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchCreationsDetail({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/${payload.creationType}/${payload.creationId}`,
    )
    const {
      result,
      entities,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCreationsDetailSuccess(result)),
    ])
  } catch (err) {
    yield put(fetchCreationsDetailFailure(err.response.data.message))
  }
}

function* watchAttemptFetchCreationsDetail() {
  yield takeLatest(
    types.FETCH_CREATIONS_DETAIL_REQUEST,
    requestFetchCreationsDetail,
  )
}

function fetchCommentsSuccess(comments) {
  return {
    type: types.FETCH_COMMENTS_SUCCESS,
    payload: {
      comments,
    },
  }
}

function fetchCommentsFailure(err) {
  return {
    type: types.FETCH_COMMENTS_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchComments({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/${payload.creationType}/${payload.creationId}/comments`,
    )
    const {
      result,
      entities,
      pagination,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCommentsSuccess(result, pagination)),
    ])
  } catch (err) {
    yield put(fetchCommentsFailure(err.response.data.message))
  }
}

function* watchAttemptFetchComments() {
  yield takeLatest(
    types.FETCH_COMMENTS_REQUEST,
    requestFetchComments,
  )
}

function fetchCollectionsSuccess({ result, pagination, type }) {
  return {
    type: types.FETCH_COLLECTIONS_SUCCESS,
    payload: {
      result,
      pagination,
      type,
    },
  }
}

function fetchCollectionsFailure(err) {
  return {
    type: types.FETCH_COLLECTIONS_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchCollections({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/users/${payload.userId}/${payload.type}`,
    )
    const {
      result,
      entities,
      pagination,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCollectionsSuccess({ result, pagination, type: payload.type })),
    ])
  } catch (err) {
    yield put(fetchCollectionsFailure(err.response.data.message))
  }
}

function* watchAttemptFetchCollections() {
  yield takeLatest(
    types.FETCH_COLLECTIONS_REQUEST,
    requestFetchCollections,
  )
}

function postCommentSuccess(comment, { placeholderId }) {
  return {
    type: types.POST_COMMENT_SUCCESS,
    payload: comment.id,
    meta: {
      placeholderId,
    },
  }
}

function postCommentFailure(err, { placeholderId }) {
  return {
    type: types.POST_COMMENT_FAILURE,
    payload: err,
    meta: {
      placeholderId,
    },
    error: true,
  }
}

function* requestPostComment({ payload, meta }) {
  try {
    const request = yield call(
      API.post,
      `/${meta.creationType}/${meta.creationId}/comments`,
      payload,
    )
    const { results, entities } = request.data.data
    const comment = entities.comments[results]

    yield all([
      put(insertEntity(entities)),
      put(postCommentSuccess(comment, meta)),
    ])
  } catch (err) {
    yield put(postCommentFailure(err.response.data.message, meta))
  }
}

function* watchAttemptPostComment() {
  yield takeLatest(types.POST_COMMENT_REQUEST, requestPostComment)
}


export default {
  watchAttemptFetchCreationsDetail,
  watchAttemptFetchCollections,
  watchAttemptFetchComments,
  watchAttemptPostComment,
}
