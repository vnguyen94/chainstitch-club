import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import { LikeButton } from '../../../components'

import styles from './styles.scss'


const propTypes = {
  // creation: PropTypes.object.isRequired,
  specific: PropTypes.object.isRequired,
  // fetchJeans: PropTypes.func.isRequired,
  // fetchUserCollections: PropTypes.func.isRequired,
  // isOwner: PropTypes.bool.isRequired,
}

const Fade = ({
  /* creation, */
  specific,
  // fetchJeans,
  // fetchUserCollections,
  // isOwner,
}) => (
  <Row start="xs" className={styles.creation}>
    <Col xs={12}>
      <h2>{specific.title}</h2>
    </Col>
    <Col xs={12}>
      <LikeButton thin />
    </Col>
    {specific.description && (
      <Col xs={12}>
        <p>{specific.description}</p>
      </Col>
    )}
  </Row>
)

Fade.propTypes = propTypes


export default Fade
