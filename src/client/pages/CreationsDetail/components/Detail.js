import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import { ImageGallery } from '../../../components'

import styles from './styles.scss'


const propTypes = {
  images: PropTypes.array.isRequired,
}

const Details = ({ images }) => (
  <Row className={styles.component} center="xs">
    <Col xs={12} md={10}>
      <ImageGallery
        items={images}
      />
    </Col>
  </Row>
)

Details.propTypes = propTypes


export default Details
