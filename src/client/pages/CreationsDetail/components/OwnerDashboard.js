import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import styles from './styles.scss'


const propTypes = {
  creation: PropTypes.object.isRequired,
}

const OwnerDashboard = ({ creation }) => (
  <Row className={styles.creation} start="md">
    <Col xs={12} md={6}>
      <Link
        to={`/${creation.type}/${creation.id}/edit`}
        className="button button--success button--small"
      >
        edit
      </Link>
    </Col>
  </Row>
)

OwnerDashboard.propTypes = propTypes


export default OwnerDashboard
