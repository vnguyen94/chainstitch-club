import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib/index'

import { UserCard } from '../../../components'


const propTypes = {
  creation: PropTypes.object.isRequired,
}

const CreationOwner = ({ creation }) => (
  <Row>
    <Col xs={12}>
      <UserCard user={creation.users} />
    </Col>
  </Row>
)

CreationOwner.propTypes = propTypes


export default CreationOwner
