import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import { getCurrentUserSelector } from '../../shared/selectors'
import { HeaderMeta, RecommendedCreations } from '../../components'

import { actions as FeedActions } from './ducks'
import { getFeedSelector } from './selectors'
import { FeedRow, FollowRecommendations } from './components'
import styles from './styles.scss'


const propTypes = {
  actions: PropTypes.object.isRequired,
  // user: PropTypes.object.isRequired,
  feed: PropTypes.array.isRequired,
}

const mapStateToProps = (state) => ({
  user: getCurrentUserSelector(state),
  feed: getFeedSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    Feed: bindActionCreators(FeedActions, dispatch),
  },
})

class Feed extends Component {
  componentDidMount() {
    const { actions } = this.props

    actions.Feed.fetchGlobalFeed()
  }

  render() {
    const { feed } = this.props

    return ([
      <Row key="1" center="xs" className={styles.page}>
        <Col xs={12} md={7} mdOffset={1}>
          <HeaderMeta title="feed" />
          {feed.length && feed.map((row) => (
            <FeedRow key={row.id} row={row} />
          ))}
        </Col>
        <Col xs={12} md={3}>
          <FollowRecommendations />
        </Col>
      </Row>,
      <RecommendedCreations key="2" />,
    ])
  }
}

Feed.propTypes = propTypes


export { Feed }
export default connect(mapStateToProps, mapDispatchToProps)(Feed)
