import { createConstants, createReducer } from '../../../utils'


export const NAME = 'feed'

export const types = createConstants(NAME, [
  'FETCH_FEED_REQUEST',
  'FETCH_FEED_SUCCESS',
  'FETCH_FEED_FAILURE',
])

export const initialState = {
  feed: [],
  isFetching: false,
  err: null,
}

export const actions = {
  fetchGlobalFeed: () => ({
    type: types.FETCH_FEED_REQUEST,
  }),
}

export default createReducer(initialState, {
  [types.FETCH_FEED_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.FETCH_FEED_SUCCESS]: (state, { payload }) => ({
    ...state,
    feed: payload.feed,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_FEED_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
