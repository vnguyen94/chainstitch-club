import React from 'react'

import styles from './styles.scss'
import { UserRecommendation } from './components'


const dummyUsers = [
  {
    id: 1,
    imageUrl: '/uploads/file_1505084669083.png',
    username: 'vnguyen94',
    createdAt: '2017-09-10T22:58:05.226Z',
    featuredImages: [
      {
        id: 1,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 2,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 3,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 4,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
    ],
  },
  {
    id: 2,
    imageUrl: '/uploads/file_1505084669083.png',
    username: 'badboy365',
    createdAt: '2017-09-10T22:58:05.226Z',
    featuredImages: [
      {
        id: 1,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 2,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 3,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 4,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
    ],
  },
  {
    id: 3,
    imageUrl: '/uploads/file_1505084669083.png',
    username: 'yomama',
    createdAt: '2017-09-10T22:58:05.226Z',
    featuredImages: [
      {
        id: 1,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 2,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 3,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
      {
        id: 4,
        url: '/uploads/default-creation.png',
        height: 64,
        width: 64,
      },
    ],
  },
]

const FollowRecommendations = () => (
  <div className={styles.component}>
    <h3>recommended users</h3>
    <div className={styles.recommendations}>
      {dummyUsers.map((user) => (
        <UserRecommendation key={user.id} user={user} />
      ))}
    </div>
  </div>
)


export default FollowRecommendations
