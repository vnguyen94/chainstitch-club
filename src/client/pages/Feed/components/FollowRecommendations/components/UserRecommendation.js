import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'

import { UserAvatar, UserLink } from '../../../../../components'

import styles from './styles.scss'


const propTypes = {
  user: PropTypes.object.isRequired,
}

const UserRecommendation = ({ user }) => (
  <Row>
    <Col xs={3}>
      <UserLink user={user}>
        <UserAvatar user={user} large />
      </UserLink>
    </Col>
    <Col xs={9} className={styles.subheader}>
      <Row>
        <Col xs={12}>
          <UserLink user={user} />
        </Col>
        <Col xs={12}>
          121 creations
        </Col>
      </Row>
      <button className="button button--success button--thin">follow</button>
    </Col>
    <Col className={styles.featuredImages} center="xs" xs={12}>
      {user.featuredImages.map((image) => (
        <img
          key={image.id}
          src={image.url}
          alt="user fit"
        />
      ))}
    </Col>
  </Row>
)

UserRecommendation.propTypes = propTypes


export default UserRecommendation
