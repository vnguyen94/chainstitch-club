import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import { DateElapsed, UserAvatar, UserLink } from '../../../../components'

import {
  FeedActor,
  FeedVerb,
  FeedObject,
  FeedTarget,
} from './components'
import styles from './styles.scss'


const propTypes = {
  row: PropTypes.object.isRequired,
}

const FeedRow = ({ row }) => (
  <Row className={styles.row}>
    <Col xs={1}>
      <Row>
        <UserLink user={row.actor}>
          <UserAvatar user={row.actor} large />
        </UserLink>
      </Row>
    </Col>
    <Col xs={11}>
      <Row>
        <FeedActor actor={row.actor} />
        <FeedVerb verb={row.verb} />
        <FeedObject verb={row.verb} object={row.object} />
        {row.target &&
          <FeedTarget target={row.target} />
        }
        <span>.</span>
      </Row>
      <Row>
        <DateElapsed time={row.createdAt} />
      </Row>
    </Col>
  </Row>
)

FeedRow.propTypes = propTypes


export default FeedRow
