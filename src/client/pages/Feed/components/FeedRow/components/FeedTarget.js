import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'

import { UserLink } from '../../../../../components'


const propTypes = {
  target: PropTypes.object.isRequired,
}

const FeedTarget = ({ target }) => {
  let prefix = ''
  let suffix = ''
  let link

  if (target.type === 'fades') {
    prefix = 'on the fade '
    link = (
      <Link to={`/fades/${target.id}`}>
        sample
      </Link>
    )
  }
  if (target.type === 'users_profiles') {
    prefix = 'on '
    link = <UserLink user={target} />
    suffix = '\'s profile'
  }

  return (
    <span>&nbsp;{prefix}{link}{suffix}</span>
  )
}

FeedTarget.propTypes = propTypes


export default FeedTarget
