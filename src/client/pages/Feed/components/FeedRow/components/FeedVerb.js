import PropTypes from 'prop-types'
import React from 'react'
import { last } from 'lodash'


const propTypes = {
  verb: PropTypes.string.isRequired,
}

const FeedVerb = ({ verb }) => {
  let pastTenseVerb = last(verb) === 'e'
    ? `${verb}d`
    : `${verb}ed`

  if (verb === 'signup') {
    pastTenseVerb = 'signed up'
  }

  return (
    <span>&nbsp;{pastTenseVerb}</span>
  )
}


FeedVerb.propTypes = propTypes


export default FeedVerb
