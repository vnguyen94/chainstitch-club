import FeedActor from './FeedActor'
import FeedVerb from './FeedVerb'
import FeedObject from './FeedObject'
import FeedTarget from './FeedTarget'


export {
  FeedActor,
  FeedVerb,
  FeedObject,
  FeedTarget,
}
