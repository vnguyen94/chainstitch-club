import PropTypes from 'prop-types'
import React from 'react'

import { UserLink } from '../../../../../components'


const propTypes = {
  actor: PropTypes.object.isRequired,
}

const FeedActor = ({ actor }) => (
  <UserLink user={actor} />
)

FeedActor.propTypes = propTypes


export default FeedActor
