import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'

import { UserLink } from '../../../../../components'

const objectTypes = {
  comments: 'comment',
  users: 'user',
  communities: 'community',
  fits: 'fit',
  fades: 'fade',
}

function toSingular(type) {
  const singular = objectTypes[type]

  if (!singular) {
    throw new Error('object type does not have singular version.')
  }

  return singular
}

const propTypes = {
  verb: PropTypes.string.isRequired,
  object: PropTypes.object.isRequired,
}

const FeedObject = ({ verb, object }) => {
  // take care in `FeedTarget`
  if (object.type === 'comments') {
    return null
  }
  if (verb === 'signup') {
    return null
  }

  let link

  if (object.type === 'users') {
    link = <UserLink user={object} />
  } else {
    const to = {
      pathname: `${object.type}/${object.id}`,
      query: { title: object.title },
    }
    link = (
      <Link to={to}>{object.title}</Link>
    )
  }

  const singularObject = toSingular(object.type)

  return (
    <span>&nbsp;the&nbsp;{singularObject}&nbsp;{link}</span>
  )
}

FeedObject.propTypes = propTypes


export default FeedObject
