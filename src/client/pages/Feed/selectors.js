import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'
import { get } from 'lodash'

import { FeedsSchema } from '../../../schemas'
import { getStateEntitiesSelector } from '../../shared/selectors'

import { NAME } from './ducks'


export const getStateFeedSelector = (state) => state[NAME]

function assignEntityObject(entities, object) {
  const { id, type } = object
  const entityObject = get(entities, [type, id])

  if (entityObject) {
    // keep specific types from creation
    return {
      ...entityObject,
      id: object.id,
      type: object.type,
      objectType: entityObject.type,
      objectId: entityObject.id,
    }
  }

  return object
}

export const getFeedSelector = createSelector(
  [getStateFeedSelector, getStateEntitiesSelector],
  (state, entities) => {
    if (!state.feed.length || !entities.feeds) {
      return []
    }

    const feed = denormalize(
      state.feed,
      FeedsSchema.list,
      entities,
    )

    return feed.map((f) => {
      f.object = assignEntityObject(entities, f.object)

      if (f.target) {
        f.target = assignEntityObject(entities, f.target)
      }

      return f
    })
  },
)

export const getErrSelector = createSelector(
  [getStateFeedSelector],
  (loginState) => loginState.err,
)
