import { put, all, call, takeLatest } from 'redux-saga/effects' //eslint-disable-line

import { API } from '../../../utils'
import { insertEntity } from '../../shared/entities'


import { types } from './ducks'


export function fetchGlobalFeedSuccess(feed) {
  return {
    type: types.FETCH_FEED_SUCCESS,
    payload: {
      feed,
    },
  }
}

function fetchGlobalFeedFailure(err) {
  return {
    type: types.FETCH_FEED_FAILURE,
    payload: err,
    error: true,
  }
}

function* fetchGlobalFeed() {
  try {
    const request = yield call(API.get, '/feeds?direction=desc')
    const {
      result,
      entities,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchGlobalFeedSuccess(result)),
    ])
  } catch (err) {
    yield put(fetchGlobalFeedFailure(err.response.data.message))
  }
}

function* watchAttemptFetchGlobalFeed() {
  yield takeLatest(types.FETCH_FEED_REQUEST, fetchGlobalFeed)
}


export default {
  watchAttemptFetchGlobalFeed,
}
