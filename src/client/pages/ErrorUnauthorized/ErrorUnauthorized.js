import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import { HeaderMeta } from '../../components'

import styles from './styles.scss'


const ErrorUnauthorized = () => (
  <Row center="xs" className={styles.page}>
    <Col xs={12}>
      <HeaderMeta title="403" />
      <p>403 unauthorized</p>
    </Col>
  </Row>
)


export default ErrorUnauthorized
