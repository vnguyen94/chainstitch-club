import { createConstants, createReducer } from '../../../utils'


export const NAME = 'signup'

export const types = createConstants(NAME, [
  'SIGNUP_REQUEST',
  'SIGNUP_FAILURE',
  'SIGNUP_FORM',
  'PERSONAL_INFORMATION_FORM',
])

export const initialState = {
  user: null,
  token: null,
  isFetching: false,
  err: null,
}

export const actions = {
  signup: ({ confirmPassword, ...payload }, callback) => ({
    type: types.SIGNUP_REQUEST,
    payload,
    meta: { callback },
  }),
}

export default createReducer(initialState, {
  [types.SIGNUP_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.SIGNUP_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
