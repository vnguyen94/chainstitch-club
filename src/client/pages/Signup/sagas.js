import { put, call, takeLatest } from 'redux-saga/effects' //eslint-disable-line

import { API } from '../../../utils'
import { loginSuccess } from '../Login/sagas'

import { types } from './ducks'


function signupFailure(err) {
  return {
    type: types.SIGNUP_FAILURE,
    payload: err,
    error: true,
  }
}

function* signupRequest({ payload, meta }) {
  try {
    const request = yield call(API.post, '/users', payload)
    const { user, token } = request.data.data

    yield put(loginSuccess(user, token))
    meta.callback()
  } catch (err) {
    yield put(signupFailure(err.response.data.message))
  }
}

function* watchAttemptSignup() {
  yield takeLatest(types.SIGNUP_REQUEST, signupRequest)
}


export default {
  watchAttemptSignup,
}
