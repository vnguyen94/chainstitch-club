import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import { CreationCard } from '../../../components'

import styles from './styles.scss'


const FollowTopPage = ({ skipStep }) => {
  // eslint-disable-next-line max-len
  const creations = JSON.parse('[{"id":1,"userId":1,"entityId":14,"type":"fades","createdAt":"2017-09-14T04:40:40.050Z","updatedAt":"2017-09-14T04:40:40.050Z","users":{"id":2,"username":"vnguyen94","imageUrl":"/uploads/file_1505084669083.png"},"fades":{"id":3,"creationId":8,"title":"test","description":"testes","imageUrl":"/uploads/default-creation.png"}}, {"id":4,"userId":1,"entityId":14,"type":"fades","createdAt":"2017-09-14T04:40:40.050Z","updatedAt":"2017-09-14T04:40:40.050Z","users":{"id":5,"username":"vnguyen94","imageUrl":"/uploads/file_1505084669083.png"},"fades":{"id":6,"creationId":8,"title":"test","description":"testes","imageUrl":"/uploads/default-creation.png"}}, {"id":7,"userId":1,"entityId":14,"type":"fades","createdAt":"2017-09-14T04:40:40.050Z","updatedAt":"2017-09-14T04:40:40.050Z","users":{"id":8,"username":"vnguyen94","imageUrl":"/uploads/file_1505084669083.png"},"fades":{"id":9,"creationId":8,"title":"test","description":"testes","imageUrl":"/uploads/default-creation.png"}}, {"id":10,"userId":1,"entityId":14,"type":"fades","createdAt":"2017-09-14T04:40:40.050Z","updatedAt":"2017-09-14T04:40:40.050Z","users":{"id":11,"username":"vnguyen94","imageUrl":"/uploads/file_1505084669083.png"},"fades":{"id":12,"creationId":8,"title":"test","description":"testes","imageUrl":"/uploads/default-creation.png"}}]')

  return ([
    <section key={1} className={styles.subHeader}>
      <h3>build it and they will come</h3>
      <h4>
        jumpstart your follows by checking out these
        currently popular users and communities
      </h4>
    </section>,
    <Row key={2}>
      <Col xs={12}>
        <h4>users</h4>
      </Col>
      <Col xs={12}>
        <Row>
          {creations && creations.map((creation) => (
            <Col key={creation.id} xs={12} sm={6} md={4}>
              <CreationCard creation={creation} />
            </Col>
          ))}
        </Row>
      </Col>
    </Row>,
    <Row key={3}>
      <Col xs={12}>
        <h4>communities</h4>
      </Col>
      <Col xs={12}>
        <Row>
          {creations && creations.map((creation) => (
            <Col key={creation.id} xs={12} sm={6} md={4}>
              <CreationCard creation={creation} />
            </Col>
          ))}
        </Row>
      </Col>
    </Row>,
    <Row key={4}>
      <Col xs={6}>
        <button
          onClick={skipStep}
          className="button button--clear"
        >
          skip
        </button>
      </Col>
      <Col xs={6}>
        <button
          onClick={skipStep}
          className="button button--success"
        >
          next
        </button>
      </Col>
    </Row>,
  ])
}

FollowTopPage.propTypes = {
  skipStep: PropTypes.func.isRequired,
}


export default FollowTopPage
