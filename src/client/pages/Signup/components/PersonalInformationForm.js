import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'
import { Row, Col } from 'react-flexbox-grid/lib'

import { UserForms } from '../../../forms'
import { UploadDropzone } from '../../../components'
import { types } from '../ducks'

import styles from './styles.scss'


const PersonalInformationForm = ({
  handleSubmit,
  submitPersonalInfo,
  skipStep,
  err,
}) => ([
  <section key={1} className={styles.subHeader}>
    <h3>tell us about yourself</h3>
    <h4>
      this information can be toggled to be hidden.
    </h4>
  </section>,
  <Form key={2} onSubmit={handleSubmit(submitPersonalInfo)}>
    <Row>
      <Col xs={12} md={5}>
        <UploadDropzone
          className={styles.profileImageUpload}
          url="/api/creationType/creationId/images"
          onSuccessHandler={() => {}}
        />
      </Col>
      <Col xs={12} md={7}>
        <UserForms.UserProfileSection />
      </Col>
    </Row>
    {err && <p>{err}</p>}
    <Row>
      <Col xs={6}>
        <button
          onClick={skipStep}
          className="button button--clear"
        >
          skip
        </button>
      </Col>
      <Col xs={6}>
        <button
          type="submit"
          className="button button--success"
        >
          next
        </button>
      </Col>
    </Row>
  </Form>,
])

PersonalInformationForm.propTypes = {
  submitPersonalInfo: PropTypes.func.isRequired,
  skipStep: PropTypes.func.isRequired,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
}


export default reduxForm({
  form: types.PERSONAL_INFORMATION_FORM,
  fields: [
    'username',
    'firstName',
    'lastName',
    'email',
    'password',
    'confirmPassword',
  ],
})(PersonalInformationForm)
