import PropTypes from 'prop-types'
import React from 'react'
import { Form, reduxForm } from 'redux-form'
import { Link } from 'react-router'
import { Row, Col } from 'react-flexbox-grid/lib'

import { UserForms } from '../../../forms'
import { types } from '../ducks'

import styles from './styles.scss'


const SignupForm = ({ handleSubmit, signup, err, valid }) => ([
  <section key={1} className={styles.subHeader}>
    <h3>sign up to share</h3>
    <h4>a place for denim and clothes</h4>
    <h4><Link to="/login">or log in here</Link></h4>
  </section>,
  <Form key={2} onSubmit={handleSubmit(signup)}>
    <UserForms.FullProfileSection />
    {err && <p>{err}</p>}
    <Row>
      <Col xs={12}>
        <button
          disabled={!valid}
          type="submit"
          className="button button--success"
        >
          sign up
        </button>
      </Col>
    </Row>
  </Form>,
])

SignupForm.propTypes = {
  signup: PropTypes.func.isRequired,
  err: PropTypes.string,
  handleSubmit: PropTypes.func,
  valid: PropTypes.bool,
}


export default reduxForm({
  form: types.SIGNUP_FORM,
  fields: [
    'username',
    'firstName',
    'lastName',
    'email',
    'password',
    'confirmPassword',
  ],
})(SignupForm)
