import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import styles from './styles.scss'


const ClosingPage = ({ navigateOut }) => ([
  <section key={1} className={styles.subHeader}>
    <h3>account successfully created</h3>
    <h4>
      don&apos;t forget remember to verify your email.
      for now, check out some of our top creations
    </h4>
  </section>,
  <Row>
    <Col xs={12}>
      <button
        onClick={navigateOut}
        className="button button--success"
      >
        go explore
      </button>
    </Col>
  </Row>,
])

ClosingPage.propTypes = {
  router: PropTypes.func.isRequired,
}


export default ClosingPage
