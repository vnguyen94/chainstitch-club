import ClosingPage from './ClosingPage'
import FollowTopPage from './FollowTopPage'
import PersonalInformationForm from './PersonalInformationForm'
import SignupForm from './SignupForm'


export {
  ClosingPage,
  FollowTopPage,
  PersonalInformationForm,
  SignupForm,
}
