import { createSelector } from 'reselect'

import { NAME } from './ducks'


export const getStateSignupSelector = (state) => state[NAME]

export const getErrSelector = createSelector(
  [getStateSignupSelector],
  (signupState) => signupState.err,
)
