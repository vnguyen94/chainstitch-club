import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import { getStateAuthSelector } from '../../shared/selectors'
import { HeaderMeta } from '../../components'

import { getErrSelector } from './selectors'
import { actions as signupActions } from './ducks'
import {
  ClosingPage,
  FollowTopPage,
  SignupForm,
  PersonalInformationForm,
} from './components'
import styles from './styles.scss'

const propTypes = {
  actions: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
  err: PropTypes.object,
}

const mapStateToProps = (state) => ({
  auth: getStateAuthSelector(state),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    signup: bindActionCreators(signupActions, dispatch),
  },
})

const PAGES = {
  CREATE_ACCOUNT: 1,
  OPTIONAL_INFORMATION: 2,
  FOLLOW: 3,
  CLOSING: 4,
}

class Signup extends Component {
  constructor() {
    super()

    this.state = {
      page: PAGES.CREATE_ACCOUNT,
    }
  }

  onSubmitSignupHandler = (payload) => {
    const { actions } = this.props

    actions.signup.signup(payload, () => {
      this.setState({ page: PAGES.OPTIONAL_INFORMATION })
    })
  }

  onSubmitPersonalInfoHandler = (payload) => {
    const { actions } = this.props

    actions.signup.signup(payload, () => {
      this.setState({ page: PAGES.FOLLOW })
    })
  }

  onClickSkipStepHandler = () => {
    const { page } = this.state

    this.setState({ page: page + 1 })
  }

  onClickNavigateOutHandler = () => {
    const { router } = this.props

    router.push('/explore')
  }

  renderPageComponent = () => {
    const { err } = this.props
    const { page } = this.state

    if (page === PAGES.CREATE_ACCOUNT) {
      return (
        <SignupForm
          signup={this.onSubmitSignupHandler}
          err={err}
        />
      )
    } else if (page === PAGES.OPTIONAL_INFORMATION) {
      return (
        <PersonalInformationForm
          submitPersonalInfo={this.onSubmitPersonalInfoHandler}
          skipStep={this.onClickSkipStepHandler}
          err={err}
        />
      )
    } else if (page === PAGES.FOLLOW) {
      return (
        <FollowTopPage
          skipStep={this.onClickSkipStepHandler}
          err={err}
        />
      )
    } else if (page === PAGES.CLOSING) {
      return (
        <ClosingPage
          navigateOut={this.onClickNavigateOutHandler}
          err={err}
        />
      )
    }

    throw new Error('invalid page index.')
  }

  render() {
    return (
      <Fragment>
        <HeaderMeta
          title="signup"
          description="signup to chainstitch club and start sharing denim."
        />
        <Row center="xs" className={styles.page}>
          <Col xs={12} className={styles.page}>
            <div className={styles.formContainer}>
              {this.renderPageComponent()}
            </div>
          </Col>
        </Row>
      </Fragment>
    )
  }
}

Signup.propTypes = propTypes


export { Signup }
export default connect(mapStateToProps, mapDispatchToProps)(Signup)
