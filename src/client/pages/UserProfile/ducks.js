import { createConstants, createReducer } from '../../../utils'


export const NAME = 'UserProfile'

export const types = createConstants(NAME, [
  'FETCH_USER_REQUEST',
  'FETCH_USER_SUCCESS',
  'FETCH_USER_FAILURE',
  'FETCH_USER_CREATIONS_REQUEST',
  'FETCH_USER_CREATIONS_SUCCESS',
  'FETCH_USER_CREATIONS_FAILURE',
  'FETCH_FOLLOWERS_OR_FOLLOWING_REQUEST',
  'FETCH_FOLLOWERS_OR_FOLLOWING_SUCCESS',
  'FETCH_FOLLOWERS_OR_FOLLOWING_FAILURE',
  'FETCH_COMMENTS_USER_PROFILE_REQUEST',
  'FETCH_COMMENTS_USER_PROFILE_SUCCESS',
  'FETCH_COMMENTS_USER_PROFILE_FAILURE',
  'POST_COMMENT_REQUEST',
  'POST_COMMENT_SUCCESS',
  'POST_COMMENT_FAILURE',
])

export const initialState = {
  user: null,
  creations: {
    result: [],
    normalized: [],
    entities: {},
    pagination: null,
    isFetching: false,
    err: null,
  },
  users: {
    followers: [],
    following: [],
    entities: {},
    pagination: null,
    isFetching: false,
    err: null,
  },
  comments: [],
  isFetching: false,
  isCommenting: false,
  err: null,
}

export const actions = {
  fetchUser: (payload) => ({
    type: types.FETCH_USER_REQUEST,
    payload,
  }),
  fetchCreations: (payload) => ({
    type: types.FETCH_USER_CREATIONS_REQUEST,
    payload,
  }),
  fetchCommentsUserProfile: (payload) => ({
    type: types.FETCH_COMMENTS_USER_PROFILE_REQUEST,
    payload,
  }),
  fetchFollowersOrFollowing: (payload) => ({
    type: types.FETCH_FOLLOWERS_OR_FOLLOWING_REQUEST,
    payload,
  }),
  postComment: (comment, userId) => ({
    type: types.POST_COMMENT_REQUEST,
    payload: {
      comment,
    },
    meta: {
      userId,
    },
  }),
}

export default createReducer(initialState, {
  [types.FETCH_USER_CREATIONS_REQUEST]: (state) => ({
    ...state,
    creations: initialState.creations,
  }),
  [types.FETCH_USER_CREATIONS_SUCCESS]: (state, { payload }) => ({
    ...state,
    creations: {
      ...state.creations,
      result: payload.result,
      entities: payload.entities,
      pagination: payload.pagination,
      isFetching: false,
      err: null,
    },
  }),
  [types.FETCH_USER_CREATIONS_FAILURE]: (state, { payload }) => ({
    ...state,
    creations: {
      ...state.creations,
      isFetching: false,
      err: payload,
    },
  }),
  [types.FETCH_USER_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    user: null,
    err: null,
  }),
  [types.FETCH_USER_SUCCESS]: (state, { payload }) => ({
    ...state,
    user: payload.user,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_USER_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
  [types.FETCH_FOLLOWERS_OR_FOLLOWING_REQUEST]: (state) => ({
    ...state,
    users: initialState.users,
  }),
  [types.FETCH_FOLLOWERS_OR_FOLLOWING_SUCCESS]: (state, { payload, meta }) => ({
    ...state,
    users: {
      ...state.users,
      [meta.type]: payload.result,
      entities: payload.entities,
      pagination: payload.pagination,
      isFetching: false,
      err: null,
    },
  }),
  [types.FETCH_FOLLOWERS_OR_FOLLOWING_FAILURE]: (state, { payload }) => ({
    ...state,
    users: {
      ...state.users,
      isFetching: false,
      err: payload,
    },
  }),
  [types.FETCH_COMMENTS_USER_PROFILE_REQUEST]: (state) => ({
    ...state,
    isFetchingComments: true,
    err: null,
  }),
  [types.FETCH_COMMENTS_USER_PROFILE_SUCCESS]: (state, { payload }) => ({
    ...state,
    comments: payload.comments,
    isFetchingComments: false,
    err: null,
  }),
  [types.FETCH_COMMENTS_USER_PROFILE_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetchingComments: false,
    err: payload,
  }),
  [types.POST_COMMENT_REQUEST]: (state) => ({
    ...state,
    isCommenting: true,
    err: null,
  }),
  [types.POST_COMMENT_SUCCESS]: (state) => ({
    ...state,
    isCommenting: false,
    err: null,
  }),
  [types.POST_COMMENT_FAILURE]: (state, { payload }) => ({
    ...state,
    isCommenting: false,
    err: payload,
  }),
})
