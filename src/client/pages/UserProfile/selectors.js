import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'

import {
  getCurrentUserSelector,
  getStateEntitiesSelector,
} from '../../shared/selectors'
import {
  CreationsSchema,
  CommentsSchema,
  FadesSchema,
  FitsSchema,
  UsersSchema,
} from '../../../schemas'

import { NAME } from './ducks'


export const getStateProfileSelector = (state) => state[NAME]

export const getUserProfileSelector = createSelector(
  [getStateProfileSelector],
  (state) => state.user,
)

export const getStatePropsSelector = (state, props) => props

export const getUsersSelector = createSelector(
  [getStateProfileSelector, getStatePropsSelector],
  (state, props) => {
    const {
      route: { activePage },
    } = props

    if (!['followers', 'following'].includes(activePage)) {
      return []
    }
    if (!state.users[activePage].length || !state.users.entities.users) {
      return []
    }

    return denormalize(
      state.users[activePage],
      UsersSchema.list,
      state.users.entities,
    )
  },
)

export const getCreationsSelector = createSelector(
  [getStateProfileSelector, getStatePropsSelector],
  (state, props) => {
    const {
      route: { activePage },
    } = props
    if (
      activePage !== 'creations' ||
      !state.creations.result.length ||
      !state.creations.entities.creations
    ) {
      return state.creations
    }

    state.creations.normalized = denormalize(
      state.creations.result,
      CreationsSchema.listWithUser,
      state.creations.entities,
    )

    return state.creations
  },
)

export const getIsFetchingSelector = createSelector(
  [getStateProfileSelector],
  (state) => state.isFetching,
)

export const getIsProfileOwnerSelector = createSelector(
  [getUserProfileSelector, getCurrentUserSelector],
  (profileUser, currentUser) => !!(
    profileUser &&
    currentUser &&
    profileUser.user.id === currentUser.id
  ),
)

export const getUserTopCreationsSelector = createSelector(
  [getStateProfileSelector],
  (state) => {
    if (!state.user || !state.user.creations) {
      return {}
    }

    let { fits, fades } = state.user.creations
    fits = denormalize(fits.result, FitsSchema.list, fits.entities)
    fades = denormalize(fades.result, FadesSchema.list, fades.entities)

    return { fits, fades }
  },
)

export const getCommentsUserProfileSelector = createSelector(
  [getStateProfileSelector, getStateEntitiesSelector],
  (state, entities) => (
    state.comments.length && entities.comments
      ? (
        denormalize(
          state.comments,
          CommentsSchema.list,
          entities,
        )
      ) : []
  ),
)

export const getErrSelector = createSelector(
  [getStateProfileSelector],
  (state) => state.err,
)
