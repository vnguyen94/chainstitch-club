import PropTypes from 'prop-types'
import React, { Component, Fragment } from 'react'
import classNames from 'classnames'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import {
  HeaderMeta,
  RecommendedCreations,
} from '../../components'
import { getCurrentUserSelector } from '../../shared/selectors'

import {
  About,
  Creations,
  Follows,
  Likes,
  HeaderPhoto,
  ProfileCard,
  ProfileNavbar,
  Overview,
} from './components'
import {
  getUserProfileSelector,
  getCreationsSelector,
  getIsFetchingSelector,
  getIsProfileOwnerSelector,
  getUserTopCreationsSelector,
  getUsersSelector,
  getCommentsUserProfileSelector,
  getErrSelector,
} from './selectors'
import { actions as ProfileActions } from './ducks'
import styles from './styles.scss'


const propTypes = {
  actions: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  topCreations: PropTypes.object.isRequired,
  users: PropTypes.array.isRequired,
  creations: PropTypes.object.isRequired,
  comments: PropTypes.array.isRequired,
  // isFetching: PropTypes.bool.isRequired,
  isProfileOwner: PropTypes.bool.isRequired,
  currentUser: PropTypes.object,
  profileUser: PropTypes.object,
  // err: PropTypes.string,
}

const mapStateToProps = (state, ownProps) => ({
  currentUser: getCurrentUserSelector(state),
  isProfileOwner: getIsProfileOwnerSelector(state),
  profileUser: getUserProfileSelector(state),
  topCreations: getUserTopCreationsSelector(state),
  creations: getCreationsSelector(state, ownProps),
  users: getUsersSelector(state, ownProps),
  isFetching: getIsFetchingSelector(state),
  comments: getCommentsUserProfileSelector(state),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    UserProfile: bindActionCreators(ProfileActions, dispatch),
  },
})

class UserProfile extends Component {
  componentDidMount() {
    const {
      params: { userId },
      actions,
    } = this.props

    actions.UserProfile.fetchUser({ userId })
    this.fetchDataForActivePage(this.props)
  }

  componentWillReceiveProps(nextProps) {
    const { actions, route, params } = this.props
    const { userId } = nextProps.params

    if (params.userId !== userId) {
      actions.UserProfile.fetchUser({ userId })
      this.fetchDataForActivePage(nextProps)
    } else if (route.activePage !== nextProps.route.activePage) {
      this.fetchDataForActivePage(nextProps)
    } else if (route.creationName !== nextProps.route.creationName) {
      this.fetchDataForActivePage(nextProps)
    }
  }

  onSubmitCommentHandler = (comment) => {
    const {
      params: { userId },
      actions,
    } = this.props

    actions.UserProfile.postComment(comment, userId)
  }

  fetchDataForActivePage = (props) => {
    const {
      params: { userId },
      route: { activePage, creationName },
      actions,
    } = props

    if (activePage === 'overview') {
      actions.UserProfile.fetchCommentsUserProfile({ userId })
    } else if (activePage === 'creations') {
      actions.UserProfile.fetchCreations({ userId, creationName })
    } else if (activePage === 'followers' || activePage === 'following') {
      actions.UserProfile.fetchFollowersOrFollowing({
        type: activePage,
        userId,
      })
    }
  }

  render() {
    const {
      params: { userId },
      route,
      currentUser,
      isProfileOwner,
      profileUser,
      creations,
      topCreations,
      comments,
      users,
    } = this.props
    const { activePage, creationName } = route

    if (!profileUser) {
      return (
        <div>loading...</div>
      )
    }

    const title = profileUser.user.username
    const description = `check out ${title}'s profile on chainstitch club.`
    let content

    if (activePage === 'overview') {
      content = (
        <Overview
          currentUser={currentUser}
          profileUser={profileUser.user}
          topCreations={topCreations}
          comments={comments}
          submitComment={this.onSubmitCommentHandler}
        />
      )
    } else if (activePage === 'creations') {
      if (creations.isFetching) {
        content = (
          <div>loading...</div>
        )
      } else {
        content = (
          <Creations
            creationName={creationName}
            results={creations.normalized}
            pagination={creations.pagination}
            err={creations.err}
          />
        )
      }
    } else if (activePage === 'likes') {
      content = (
        <Likes />
      )
    } else if (activePage === 'about') {
      content = (
        <About />
      )
    } else if (activePage === 'followers' || activePage === 'following') {
      content = (
        <Follows users={users} />
      )
    }

    return (
      <Fragment>
        <HeaderMeta
          title={title}
          description={description}
        />
        <Row center="xs">
          <Col xs={12}>
            <div className={styles.aboveContent}>
              <HeaderPhoto profileUser={profileUser.user} />
              <ProfileNavbar
                userId={userId}
                route={route}
                counts={profileUser.counts}
              />
            </div>
          </Col>
        </Row>
        <Row className={classNames(styles.mainContent)}>
          <Col md={2} mdOffset={2} className={styles.component}>
            <ProfileCard
              isProfileOwner={isProfileOwner}
              profileUser={profileUser.user}
              profile={profileUser.profile}
              counts={profileUser.counts}
              userId={userId}
            />
          </Col>
          <Col md={6} className={styles.contentRight}>
            {content}
          </Col>
        </Row>
        <RecommendedCreations />
      </Fragment>
    )
  }
}

UserProfile.propTypes = propTypes


export { UserProfile }
export default connect(mapStateToProps, mapDispatchToProps)(UserProfile)
