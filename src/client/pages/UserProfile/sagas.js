import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'
import { insertEntity } from '../../shared/entities'

import { types } from './ducks'


function fetchUserSuccess(user) {
  return {
    type: types.FETCH_USER_SUCCESS,
    payload: {
      user,
    },
  }
}

function fetchUserFailure(err) {
  return {
    type: types.FETCH_USER_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchUser({ payload }) {
  try {
    const url = `/users/${payload.userId}/profile?topCreations=true&counts=true`
    const request = yield call(API.get, url)

    yield put(fetchUserSuccess(request.data.data))
  } catch (err) {
    yield put(fetchUserFailure(err.response.data.message))
  }
}

function* watchAttemptFetchUser() {
  yield takeLatest(types.FETCH_USER_REQUEST, requestFetchUser)
}

function fetchUserCreationsSuccess(payload) {
  return {
    type: types.FETCH_USER_CREATIONS_SUCCESS,
    payload,
  }
}

function fetchUserCreationsFailure(err) {
  return {
    type: types.FETCH_USER_CREATIONS_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchUserCreations({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/users/${payload.userId}/${payload.creationName}`,
    )

    yield put(fetchUserCreationsSuccess(request.data.data))
  } catch (err) {
    yield put(fetchUserCreationsFailure(err.response.data.message))
  }
}

function* watchAttemptFetchUserCreations() {
  yield takeLatest(
    types.FETCH_USER_CREATIONS_REQUEST,
    requestFetchUserCreations,
  )
}
function fetchFollowersOrFollowingCreationsSuccess(type, payload) {
  return {
    type: types.FETCH_FOLLOWERS_OR_FOLLOWING_SUCCESS,
    meta: {
      type,
    },
    payload,
  }
}

function fetchFollowersOrFollowingCreationsFailure(err) {
  return {
    type: types.FETCH_FOLLOWERS_OR_FOLLOWING_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchFollowersOrFollowing({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/users/${payload.userId}/${payload.type}`,
    )

    yield put(fetchFollowersOrFollowingCreationsSuccess(
      payload.type,
      request.data.data,
    ))
  } catch (err) {
    yield put(
      fetchFollowersOrFollowingCreationsFailure(err.response.data.message),
    )
  }
}

function* watchAttemptFetchFollowersOrFollowing() {
  yield takeLatest(
    types.FETCH_FOLLOWERS_OR_FOLLOWING_REQUEST,
    requestFetchFollowersOrFollowing,
  )
}

function fetchCommentsUserProfileSuccess(comments) {
  return {
    type: types.FETCH_COMMENTS_USER_PROFILE_SUCCESS,
    payload: {
      comments,
    },
  }
}

function fetchCommentsUserProfileFailure(err) {
  return {
    type: types.FETCH_COMMENTS_USER_PROFILE_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchCommentsUserProfile({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/users/${payload.userId}/profile/comments`,
    )
    const {
      result,
      entities,
      pagination,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCommentsUserProfileSuccess(result, pagination)),
    ])
  } catch (err) {
    yield put(fetchCommentsUserProfileFailure(err.response.data.message))
  }
}

function* watchAttemptFetchCommentsUserProfile() {
  yield takeLatest(
    types.FETCH_COMMENTS_USER_PROFILE_REQUEST,
    requestFetchCommentsUserProfile,
  )
}

function postCommentSuccess() {
  return {
    type: types.POST_COMMENT_SUCCESS,
  }
}

function postCommentFailure(err) {
  return {
    type: types.POST_COMMENT_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestPostComment({ payload, meta }) {
  try {
    yield call(
      API.post,
      `/users/${meta.userId}/profile/comments`,
      payload,
    )

    yield put(postCommentSuccess())
  } catch (err) {
    yield put(postCommentFailure(err.response.data.message))
  }
}

function* watchAttemptPostComment() {
  yield takeLatest(types.POST_COMMENT_REQUEST, requestPostComment)
}


export default {
  watchAttemptFetchUser,
  watchAttemptFetchUserCreations,
  watchAttemptFetchFollowersOrFollowing,
  watchAttemptFetchCommentsUserProfile,
  watchAttemptPostComment,
}
