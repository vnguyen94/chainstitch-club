import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'

import styles from '../../styles.scss'
import { CommentSection } from '../../../../components'

import UserCreationList from './UserCreationList'


const propTypes = {
  profileUser: PropTypes.object.isRequired,
  topCreations: PropTypes.object.isRequired,
  comments: PropTypes.array.isRequired,
  submitComment: PropTypes.func.isRequired,
  currentUser: PropTypes.object,
}

const Overview = ({
  profileUser,
  currentUser,
  topCreations,
  comments,
  submitComment,
}) => {
  let userContent

  if (!topCreations.fits.length && !topCreations.fades.length) {
    userContent = (
      <Col xs={12} className={styles.component}>
        <span>this user does not have any content.</span>
      </Col>
    )
  } else {
    userContent = (
      <Col xs={12} className={styles.component}>
        <UserCreationList
          type="fits"
          userId={profileUser.id}
          creations={topCreations.fits}
        />
        <UserCreationList
          type="fades"
          userId={profileUser.id}
          creations={topCreations.fades}
        />
      </Col>
    )
  }

  return (
    <Row className={styles.verticalComponents}>
      {userContent}
      <Col xs={12} className={styles.component}>
        <CommentSection
          user={currentUser}
          comments={comments}
          onSubmit={submitComment}
        />
      </Col>
    </Row>
  )
}

Overview.propTypes = propTypes


export default Overview
