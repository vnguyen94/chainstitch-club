import PropTypes from 'prop-types'
import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'
import { Link } from 'react-router'

import { CreationListMinimal } from '../../../../components'

import styles from './styles.scss'


const propTypes = {
  type: PropTypes.string.isRequired,
  userId: PropTypes.number.isRequired,
  creations: PropTypes.array.isRequired,
}

const UserCreationList = ({ type, userId, creations }) => {
  if (!creations || !creations.length) {
    return null
  }

  return (
    <Row>
      <Col xs={12}>
        <h3 className={styles.caption}>{type}</h3>
        <span className={styles.seeAll}>
          <Link to={`/users/${userId}/${type}`}>see all</Link>
        </span>
      </Col>
      <Col xs={12}>
        <CreationListMinimal creations={creations} />
      </Col>
    </Row>
  )
}

UserCreationList.propTypes = propTypes


export default UserCreationList
