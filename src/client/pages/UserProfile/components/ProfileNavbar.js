import PropTypes from 'prop-types'
import React from 'react'
import classNames from 'classnames'
import { Link } from 'react-router'
import { Row } from 'react-flexbox-grid/lib/index'

import styles from './styles.scss'


const propTypes = {
  userId: PropTypes.string.isRequired,
  route: PropTypes.object.isRequired,
  counts: PropTypes.object.isRequired,
}

const ProfileNavbar = ({ userId, route, counts }) => (
  <Row className={styles.profileNavbar}>
    <ul className={styles.navbar}>
      <li
        className={classNames({
          [styles.active]: route.activePage === 'overview',
        })}
      >
        <Link to={`/users/${userId}`}>overview</Link>
      </li>
      <li
        className={classNames({
          [styles.active]: (
            route.activePage === 'creations' &&
            route.creationName === 'fits'
          ),
        })}
      >
        <Link to={`/users/${userId}/fits`}>
          <span>fits </span>
          <span><b>{counts.fits}</b></span>
        </Link>
      </li>
      <li
        className={classNames({
          [styles.active]: (
            route.activePage === 'creations' &&
            route.creationName === 'fades'
          ),
        })}
      >
        <Link to={`/users/${userId}/fades`}>
          <span>fades </span>
          <span><b>{counts.fades}</b></span>
        </Link>
      </li>
      <li
        className={classNames({
          [styles.active]: route.activePage === 'likes',
        })}
      >
        <Link to={`/users/${userId}/likes`}>
          <span>likes </span>
          <span><b>{counts.likes}</b></span>
        </Link>
      </li>
      <li
        className={classNames({
          [styles.active]: route.activePage === 'about',
        })}
      >
        <Link to={`/users/${userId}/about`}>about</Link>
      </li>
    </ul>
  </Row>
)

ProfileNavbar.propTypes = propTypes


export default ProfileNavbar
