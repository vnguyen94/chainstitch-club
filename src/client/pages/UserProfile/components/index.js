import About from './About'
import Creations from './Creations'
import Follows from './Follows'
import Likes from './Likes'
import HeaderPhoto from './HeaderPhoto'
import Overview from './Overview'
import ProfileCard from './ProfileCard'
import ProfileNavbar from './ProfileNavbar'


export {
  About,
  Creations,
  Follows,
  Likes,
  HeaderPhoto,
  Overview,
  ProfileCard,
  ProfileNavbar,
}
