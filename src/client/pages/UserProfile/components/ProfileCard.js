import PropTypes from 'prop-types'
import React from 'react'
import { Link } from 'react-router'
import classNames from 'classnames'
import { Row, Col } from 'react-flexbox-grid/lib/index'
import MaleIcon from 'react-icons/lib/fa/mars'
import FemaleIcon from 'react-icons/lib/fa/venus'
import GenderlessIcon from 'react-icons/lib/fa/genderless'

import { UserLink } from '../../../components'

import styles from './styles.scss'


const propTypes = {
  userId: PropTypes.string.isRequired,
  isProfileOwner: PropTypes.bool.isRequired,
  profileUser: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  counts: PropTypes.object.isRequired,
}

const baseButtonClass = 'button button--small'
const buttonClass = classNames(baseButtonClass)
const buttonDisabledClass = classNames(baseButtonClass, 'button--clear')

const ProfileCard = ({
  userId,
  isProfileOwner,
  profileUser,
  profile,
  counts,
}) => (
  <Row className={styles.profileCard}>
    <Col xs={12} className={styles.names}>
      <h2>{profile.firstName} {profile.lastName}</h2>
      <h4 className={styles.username}>
        @<UserLink user={profile}>{profileUser.username}</UserLink>
      </h4>
    </Col>
    {!isProfileOwner && [
      <Col key="1" xs={12} className={styles.divider} />,
      <Col key="2" xs={12}>
        {profileUser.isFollowing
          ? <button className={buttonClass}>following</button>
          : <button className={buttonDisabledClass}>follow</button>
        }
      </Col>,
    ]}
    <Col xs={12} className={styles.divider} />
    <Col xs={12} md={6} center="xs">
      <h4 className={styles.counts}>
        <Link
          className={styles.countLink}
          to={`/users/${userId}/followers`}
        >
          <div className={styles.counter}><b>{counts.followers}</b></div>
          <div>followers</div>
        </Link>
      </h4>
    </Col>
    <Col xs={12} md={6} center="xs">
      <h4 className={styles.counts}>
        <Link
          className={styles.countLink}
          to={`/users/${userId}/following`}
        >
          <div className={styles.counter}><b>{counts.following}</b></div>
          <div>following</div>
        </Link>
      </h4>
    </Col>
    {profile.description && [
      <Col key="1" xs={12} className={styles.divider} />,
      <Col key="2" xs={12}>
        <h4>{profile.description}</h4>
      </Col>,
    ]}
    {profile.gender && [
      <Col key="1" xs={12} className={styles.divider} />,
      <Col key="2" xs={12}>
        <h4>
          {profile.gender === 'male' && (
            <MaleIcon />
          )}
          {profile.gender === 'female' && (
            <FemaleIcon />
          )}
          {!['male', 'female'].includes(profile.gender) && (
            <GenderlessIcon />
          )}
          {profile.gender}
        </h4>
      </Col>,
    ]}
    {profile.location && [
      <Col key="1" xs={12} className={styles.divider} />,
      <Col key="2" xs={12}>
        <h4>
          <i className="fa fa-map-marker" />
          {profile.location}
        </h4>
      </Col>,
    ]}
  </Row>
)

ProfileCard.propTypes = propTypes


export default ProfileCard
