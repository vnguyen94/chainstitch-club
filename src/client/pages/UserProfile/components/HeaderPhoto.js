import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'


import styles from './styles.scss'


const propTypes = {
  profileUser: PropTypes.object.isRequired,
}

// TODO: make not static
// eslint-disable-next-line max-len
const staticHeaderUrl = 'https://pbs.twimg.com/profile_banners/949097670/1506704562/1500x500'

const HeaderPhoto = ({ profileUser }) => (
  <div className={styles.headerPhotoContainer}>
    <div className={styles.headerPhoto}>
      <img
        src={staticHeaderUrl}
        alt={`${profileUser.username}'s header`}
      />
    </div>
    <Link to={`/users/${profileUser.id}`}>
      <div className={styles.imageHolder}>
        <div className={styles.profileImage}>
          <img
            src={profileUser.imageUrl}
            alt={profileUser.username}
          />
        </div>
      </div>
    </Link>
  </div>
)

HeaderPhoto.propTypes = propTypes


export default HeaderPhoto
