import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'

import { CreationListMinimal } from '../../../../components'
import styles from '../../styles.scss'


const propTypes = {
  creationName: PropTypes.string.isRequired,
  results: PropTypes.array.isRequired,
  // pagination: PropTypes.object.isRequired,
  // err: PropTypes.object,
}


const Creations = ({ creationName, results /* , pagination, err */ }) => {
  let content

  if (!results) {
    content = (
      <span>user has no {creationName}.</span>
    )
  } else {
    content = (
      <CreationListMinimal creations={results} />
    )
  }

  return (
    <Row>
      <Col xs={12} className={styles.component}>
        {content}
      </Col>
    </Row>
  )
}

Creations.propTypes = propTypes


export default Creations
