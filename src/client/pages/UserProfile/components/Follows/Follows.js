import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'react-flexbox-grid/lib'

import { UserList } from '../../../../components'
import styles from '../../styles.scss'


const propTypes = {
  users: PropTypes.array.isRequired,
}

const Follows = ({ users }) => (
  <Row>
    <Col xs={12} className={styles.component}>
      {users.length ? (
        <UserList users={users} />
      ) : (
        <h4>This user has no likes.</h4>
      )}
    </Col>
  </Row>
)

Follows.propTypes = propTypes


export default Follows
