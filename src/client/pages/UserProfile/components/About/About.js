import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import styles from '../../styles.scss'


const About = () => (
  <Row>
    <Col xs={12} className={styles.component}>
      About
    </Col>
  </Row>
)


export default About
