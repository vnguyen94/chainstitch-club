import React from 'react'
import { Row, Col } from 'react-flexbox-grid/lib'

import styles from '../../styles.scss'


const Likes = () => (
  <Row>
    <Col xs={12} className={styles.component}>
      <h4>This user has no likes.</h4>
    </Col>
  </Row>
)


export default Likes
