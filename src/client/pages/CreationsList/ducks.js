import { createConstants, createReducer } from '../../../utils'


export const NAME = 'CreationsList'

export const types = createConstants(NAME, [
  'FETCH_CREATIONS_LIST_REQUEST',
  'FETCH_CREATIONS_LIST_SUCCESS',
  'FETCH_CREATIONS_LIST_FAILURE',
])

export const initialState = {
  creations: [],
  pagination: {},
  isFetching: false,
  err: null,
}

export const actions = {
  fetchCreationsList: (payload) => ({
    type: types.FETCH_CREATIONS_LIST_REQUEST,
    payload,
  }),
}

export default createReducer(initialState, {
  [types.FETCH_CREATIONS_LIST_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
    err: null,
  }),
  [types.FETCH_CREATIONS_LIST_SUCCESS]: (state, { payload }) => ({
    ...state,
    creations: payload.creations,
    pagination: payload.pagination,
    isFetching: false,
    err: null,
  }),
  [types.FETCH_CREATIONS_LIST_FAILURE]: (state, { payload }) => ({
    ...state,
    isFetching: false,
    err: payload,
  }),
})
