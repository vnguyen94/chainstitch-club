import { createSelector } from 'reselect'
import { denormalize } from 'normalizr'

import { getStateEntitiesSelector } from '../../shared/selectors'
import { FadesSchema, FitsSchema } from '../../../schemas'

import { NAME } from './ducks'

const schemas = {
  fades: FadesSchema,
  fits: FitsSchema,
}


export const getStateCreationsListSelector = (state) => state[NAME]
export const getCreationTypeSelector =
  (state, ownProps) => ownProps.route.creationType

export const getCreationsListSelector = createSelector(
  [getStateCreationsListSelector, getStateEntitiesSelector,
    getCreationTypeSelector],
  (state, entities, creationType) => (
    state.creations.length && entities.creations
      ? (
        denormalize(
          state.creations,
          schemas[creationType].list,
          entities,
        )
      ) : []
  ),
)

export const getPaginationSelector = createSelector(
  [getStateCreationsListSelector],
  (state) => state.pagination,
)

export const getErrSelector = createSelector(
  [getStateCreationsListSelector],
  (state) => state.err,
)
