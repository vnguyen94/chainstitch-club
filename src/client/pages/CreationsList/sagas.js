import { put, all, call, takeLatest } from 'redux-saga/effects' // eslint-disable-line

import { API } from '../../../utils'
import { insertEntity } from '../../shared/entities'

import { types } from './ducks'


function fetchCreationsListSuccess(creations, pagination) {
  return {
    type: types.FETCH_CREATIONS_LIST_SUCCESS,
    payload: {
      creations,
      pagination,
    },
  }
}

function fetchCreationsListFailure(err) {
  return {
    type: types.FETCH_CREATIONS_LIST_FAILURE,
    payload: err,
    error: true,
  }
}

function* requestFetchCreationsList({ payload }) {
  try {
    const request = yield call(
      API.get,
      `/${payload.creationType}`,
    )
    const {
      result,
      entities,
      pagination,
    } = request.data.data

    yield all([
      put(insertEntity(entities)),
      put(fetchCreationsListSuccess(result, pagination)),
    ])
  } catch (err) {
    yield put(fetchCreationsListFailure(err.response.data.message))
  }
}

function* watchAttemptFetchCreationsList() {
  yield takeLatest(
    types.FETCH_CREATIONS_LIST_REQUEST,
    requestFetchCreationsList,
  )
}


export default {
  watchAttemptFetchCreationsList,
}
