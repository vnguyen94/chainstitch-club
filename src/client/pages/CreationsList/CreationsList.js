import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Row, Col } from 'react-flexbox-grid/lib'

import { CreationCard, HeaderMeta } from '../../components'

import {
  getCreationsListSelector,
  getErrSelector,
  getPaginationSelector,
} from './selectors'
import { actions as CreationsListActions } from './ducks'
import styles from './styles.scss'


const propTypes = {
  actions: PropTypes.object.isRequired,
  route: PropTypes.object.isRequired,
  creations: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
}

const mapStateToProps = (state, props) => ({
  creations: getCreationsListSelector(state, props),
  pagination: getPaginationSelector(state),
  err: getErrSelector(state),
})

const mapDispatchToProps = (dispatch) => ({
  actions: {
    CreationsList: bindActionCreators(CreationsListActions, dispatch),
  },
})

class CreationsList extends Component {
  componentDidMount() {
    const {
      route: { creationType },
      actions,
    } = this.props

    actions.CreationsList.fetchCreationsList({ creationType })
  }

  render() {
    const {
      route: { creationType },
      creations,
      pagination,
    } = this.props

    return (
      <Row center="xs" className={styles.page}>
        <Col xs={12}>
          <HeaderMeta
            title={creationType}
            description={`explore ${creationType}s on chainstitch club.`}
          />
          {creations && creations.map((creation) => (
            <CreationCard key={creation.id} creation={creation} />
          ))}
          {pagination && JSON.stringify(pagination)}
        </Col>
      </Row>
    )
  }
}

CreationsList.propTypes = propTypes


export { CreationsList }
export default connect(mapStateToProps, mapDispatchToProps)(CreationsList)
