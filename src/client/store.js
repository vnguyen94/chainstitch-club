import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly'

import rootReducer from './rootReducer'
import rootSaga from './rootSaga'


export default function configureStore(preloadedState = {}) {
  const sagaMiddleware = createSagaMiddleware()
  const middlewares = [
    applyMiddleware(sagaMiddleware),
  ]

  const store = createStore(
    rootReducer,
    preloadedState,
    composeWithDevTools(...middlewares),
  )
  sagaMiddleware.run(rootSaga)

  if (__DEV__ && module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./rootReducer', () => {
      // eslint-disable-next-line global-require
      const nextRootReducer = require('./rootReducer').default
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
