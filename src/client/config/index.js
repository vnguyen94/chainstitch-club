const featureFlags = require('../../../config/featureFlagsClient')

const baseConfig = __DEV__
  ? require('./dev').default
  : require('./prod').default


const config = Object.assign({}, baseConfig, featureFlags)


export default config
