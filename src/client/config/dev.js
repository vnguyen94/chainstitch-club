export default {
  app: {
    htmlAttributes: { lang: 'en' },
    title: 'chainstitch club',
    titleTemplate: '%s - chainstitch club',
    JWT_TOKEN_NAME: 'csc_jwt',
  },
  meta: {
    title: 'chainstitch club',
    description: 'made by and for denim enthusiasts',
    image: '/uploads/some_image.jpg',
  },
}
