function log_duration {
    echo "$1: $(($2 / 60)) minutes and $(($2 % 60)) seconds elapsed.";
}

seconds_difference=0;

echo 'removing dist...           (1/4)';
rm -rf dist;
mkdir -p dist/src;
seconds_difference=$SECONDS;
duration_remove=$seconds_difference;

echo 'transpiling server code... (2/4)';
STEP_SECONDS=duration_remove;
SECONDS=0;
babel config -d dist/config --source-maps inline -q;
babel src/schemas -d dist/src/schemas --source-maps inline -q;
babel src/server -d dist/src/server --source-maps inline -q;
babel src/utils -d dist/src/utils --source-maps inline -q;
seconds_difference=$(($SECONDS - $seconds_difference));
duration_transpile=$seconds_difference;

echo 'adding public assets...    (3/4)';
STEP_SECONDS=duration_transpile;
SECONDS=0;
mkdir -p dist/public/js;
cp -R public/img dist/public/img;
cp public/favicon.ico dist/public/favicon.ico;
cp public/humans.txt dist/public/humans.txt;
cp public/robots.txt dist/public/robots.txt;
tar -czf dist/public/humans.txt.gz public/humans.txt
tar -czf dist/public/robots.txt.gz public/robots.txt
seconds_difference=$(($SECONDS - $seconds_difference));
duration_assets=$seconds_difference;

echo 'bundling client code...    (4/4)';
STEP_SECONDS=duration_assets;
SECONDS=0;
NODE_ENV=production NODE_PATH=src DEBUG=app:* \
    node -r babel-core/register node_modules/.bin/webpack \
        --progress \
        --config \
        webpack/config/production;
seconds_difference=$(($SECONDS - $seconds_difference));
duration_webpack=$seconds_difference;

log_duration 'remove' $duration_remove;
log_duration 'transpile' $duration_transpile;
log_duration 'assets' $duration_assets;
log_duration 'webpack' $duration_webpack;
